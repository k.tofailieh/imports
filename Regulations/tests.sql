



select plcsys_ke, de_ke, der_plcsys.*
from plcsys
     inner join der_plcsys on plcsys.plcsys_id = der_plcsys.der_src_id
     inner join de d on der_plcsys.der_dst_id = d.de_id
where plcsys_ke like ('H220%')

insert into der_plcsys(der_id, der_src_id, der_dst_id, der_tp)
values (nextval('seq_der_plcsys_id'), 4577, 253, 'ROUTE_OF_EXPOSURE_LOV');

update plcsys
set is_public = 1
where plcsys_id in (100065201, 100065200, 100065202, 100065203);

delete
from der_plcsys
where der_id = 2003224;


select entr_plcsys_plcsys.*
from plcsys pp
     inner join entr_plcsys_plcsys on pp.plcsys_id = entr_plcsys_plcsys.entr_dst_id
     inner join plcsys p on p.plcsys_id = entr_plcsys_plcsys.entr_src_id
where pp.plcsys_ke in ('H232', 'H318') --and entr_tp like '%GHS06'


--where pp.plcsys_id in (100065201,100065200,100065202,100065203)


update der_plcsys
set der_char1 = 'Pyr. Gas',
    der_char2 = 'H232'
where der_id = 2003216
update entr_plcsys_plcsys
set entr_char1 = 'Pyr. Gas',
    entr_char2 = 'H232'
where entr_id = 244622;
update entr_plcsys_plcsys
set entr_tp = entr_tp || '66666'
where entr_id in (234912, 234913)

select *
from entr_plcsys_plcsys
where entr_id = 244616;


select entr_src_id, entr_dst_id, entr_tp, hstm.plcsys_ke, cat.plcsys_ke
from entr_plcsys_plcsys
     inner join plcsys cat on entr_plcsys_plcsys.entr_src_id = cat.plcsys_id
     inner join plcsys hstm on entr_plcsys_plcsys.entr_dst_id = hstm.plcsys_id
where cat.plcsys_ke = 'Chem. Unst. Gas A';


select *
from plcsys
where plcsys_ke = 'H232'

update plcsys
set ns = '*'
where plcsys_co = 'new statement.'


delete from import.import_line where imp_ref = 'new-statements-import-test-2'
delete from import.import_matching where true;