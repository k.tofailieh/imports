/* upaload the import-file into import_line with these specifications:
   value1: path
   value2: tp
   value3: entr_char1
   value4: entr_char2
   value5: entr_tp
   value6: GHS07

   in the queries we will use also:
        value_id1: de_id
        value10, value11: update message for entr_plcsys_plcsys and der_plcsys.
        import_matching(imp_id, entr_table,entr_dst_id,entr_tp,entr_char1,entr_char2): contains entr_plcsys_plcsys relations before creating them in the DB.
*/

/* Check. */
------------------------------------------------------------------------------------------------------------------------
/* Get plcsys_id. */
UPDATE import.import_line
SET internal_id = plcsys_id
FROM plcsys
WHERE upper(trim(value1)) = upper(trim(path)) -- do the matching by path.
  AND imp_ref = :imp_ref;

/* Error for not matched records. */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || E' | Cannot implement GH07 for this entity, it doesn\'t  matched with DB'
WHERE internal_id IS NULL
  AND imp_ref = :imp_ref;
-- comment:put error message for the categories(path) that we need for GHS07 so you need to have 1 in value6

/* Error for GH07 = null */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || E' | Must define GH07 to 0 or 1'
WHERE imp_ref = :imp_ref
  AND coalesce(value6, '') = '';
-- comment: need to provide a statistic if the cases like this exists

/* Insert the relations into import-matching */
INSERT INTO import.import_matching (imp_id, entr_table, entr_dst_id, entr_tp, entr_char1, entr_char2)
SELECT count(*)
FROM (SELECT DISTINCT il.imp_id,
                      'entr_plcsys_plcsys'            AS entr_table,
                      entr_dst_id,
                      upper(trim(value5)) || '_GHS07' AS entr_tp,
                      entr_char1,
                      entr_char2
      FROM import.import_line il
               LEFT JOIN entr_plcsys_plcsys
                         ON entr_src_id = il.internal_id AND upper(trim(entr_char1)) = upper(trim(value3)) AND
                            upper(trim(entr_char2)) = upper(trim(value4)) AND entr_tp LIKE upper(trim(value5)) || '%'
      WHERE imp_ref = :imp_ref
        AND coalesce(imp_err, FALSE) = FALSE
        AND value6 = '1'
        AND entr_id IS NULL) ss;
-- GROUP BY entr_dst_id, value5, entr_char1, entr_char2;
-- for Relations entr_plcsys_plcsys, they appeared with not empty entr_tp in the import file.
--comment: I don't understand the purpose of this script

/* get de_id for GHS07 */
UPDATE import.import_line
SET value_id1 = de_id
FROM de
WHERE coalesce(value5, '') = '' -- for Categories, they appeared with an empty entr_tp in the import file.
--comment:we need to link the de to the categories as well to make them valide for GHS07
  AND coalesce(imp_err, FALSE) = FALSE
  AND de_ke LIKE 'GHS07'
  AND value6 = '1'
  AND imp_ref = :imp_ref;

/* Warning if der_plcsys is exists for some records. */
UPDATE import.import_line
SET imp_warn     = TRUE,
    imp_warn_msg = coalesce(imp_warn_msg, '') ||
                   ' | already implement GHS07(der_plcsys for this record is already exists).'
FROM der_plcsys
WHERE der_src_id = internal_id --comment: the internal_id is the id of the categories only right? not H_STMT,GHS_PICTO,...
  AND der_dst_id = value_id1
  AND der_tp = 'VALID_FOR_REGULATORY'
  AND imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE;

/* Commit. */
------------------------------------------------------------------------------------------------------------------------
BEGIN;
/* insert entr_plcsys_plcsys */
INSERT INTO entr_plcsys_plcsys(entr_id, entr_tp, entr_src_id, entr_dst_id, entr_char1, entr_char2, entr_co)
SELECT nextval('seq_entr_plcsys_plcsys_id'),
       entr_tp,
       import_line.internal_id,
       entr_dst_id,
       entr_char1,
       entr_char2,
       'GH07 implement test'
FROM import.import_line
         INNER JOIN import.import_matching USING (imp_id)
WHERE imp_ref = :imp_ref
  AND coalesce(import_line.imp_err, FALSE) = FALSE
  AND value10 IS NULL -- check if the link hadn't created.
  AND value6 = '1';

/* This update is to avoid creating the link many times. */
UPDATE import.import_line
SET value10 = 'entr_plcsys_plcsys is Already Implemented.'
FROM import.import_matching
WHERE import_matching.imp_id = import_line.imp_id
  AND imp_ref = :imp_ref
  AND coalesce(import_line.imp_err, FALSE) = FALSE
  AND value6 = '1';

/* Link the current plcsys with de(GHS07) */
INSERT INTO der_plcsys(der_id, der_src_id, der_dst_id, der_tp, der_co)
SELECT nextval('seq_der_plcsys_id'), internal_id, value_id1, 'VALID_FOR_REGULATORY', 'GHS07 implement test.'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND coalesce(imp_warn, FALSE) = FALSE
  AND coalesce(value5, '') = ''
  AND value11 IS NULL
  AND value6 = '1';

/* This update is to avoid creating the link many times. */
UPDATE import.import_line
SET value11 = 'der_plcsys is Already Implemented.'
WHERE imp_ref = :imp_ref
  AND coalesce(import_line.imp_err, FALSE) = FALSE
  AND coalesce(imp_warn, FALSE) = FALSE
  AND coalesce(value5, '') = ''
  AND value6 = '1';

COMMIT;
------------------------------------------------------------------------------------------------------------------------
-- Not Implemented Records.
SELECT value1      AS path,
       value2      AS tp,
       value3      AS entr_tp,
       value4      AS entr_char1,
       value5      AS entr_char2,
       value6      AS GHS07,
       imp_err_msg AS error_message
FROM import.import_line
WHERE imp_err = TRUE
  AND imp_ref = :imp_ref;

------------------------------------------------------------------------------------------------------------------------


SELECT *
FROM import.import_line;


SELECT plcsys_id, path, tp, entr_tp, entr_char1, entr_char2
FROM plcsys
         INNER JOIN entr_plcsys_plcsys ON plcsys.plcsys_id = entr_plcsys_plcsys.entr_src_id
WHERE plcsys_id = 128;


SELECT *
FROM entr_plcsys_plcsys
WHERE entr_char1 = 'Aerosol 1'
  AND entr_char2 = 'H222';


--SELECT * from import.import_line where imp_ref = :imp_ref

SELECT count(DISTINCT plcsys_id)
FROM plcsys
         INNER JOIN import.import_line ON upper(trim(plcsys_ke)) = upper(trim(value4))
WHERE imp_ref = :imp_ref
  AND coalesce(value4, '') != '';


SELECT count(*)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = TRUE
  AND value6 = '0'
--AND entr_tp LIKE upper(trim(value5)) || '%'


SELECT *
FROM (SELECT DISTINCT il.imp_id,
                      il.internal_id,
                      'entr_plcsys_plcsys'            AS entr_table,
                      value3,
                      value4,
                      value5,
                      value6,
                      entr_dst_id,
                      upper(trim(value5)) || '_GHS07' AS entr_tp,
                      entr_char1,
                      entr_char2
      FROM import.import_line il
               LEFT JOIN entr_plcsys_plcsys
                         ON entr_src_id = il.internal_id AND upper(trim(entr_char1)) = upper(trim(value3)) AND
                            upper(trim(entr_char2)) = upper(trim(value4)) AND entr_tp LIKE upper(trim(value5)) || '%'
      WHERE imp_ref = :imp_ref
        AND coalesce(imp_err, FALSE) = FALSE
        AND value6 = '1'
        AND entr_id IS NULL) ss;


SELECT *
FROM plcsys
WHERE plcsys_ke IN (SELECT value4
                    FROM import.import_line
                    WHERE imp_ref = :imp_ref
                      AND replace(upper(trim(tp)), '_', '') = replace(upper(trim(value5)), '_', ''));
-- and name = 'P_STMT'
select count(*)
FROM import.import_line inner join plcsys on replace(upper(trim(name)), '_', '') = replace(upper(trim(value5)), '_', '')
 and upper(trim(plcsys_ke)) = upper(trim(value4))
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND value6 = '1'
  AND coalesce(value5, '') != ''


-- get sub-categories ids into and insert them into import-matching
INSERT INTO import.import_matching (imp_id, entr_table, entr_dst_id, entr_tp, entr_char1, entr_char2)
SELECT DISTINCT il.imp_id,
                'entr_plcsys_plcsys',
                plcsys_id,
                upper(trim(value5)) || '_GHS07' AS entr_tp,
                value3,
                value4
FROM import.import_line il
         INNER JOIN plcsys ON replace(upper(trim(name)), '_', '') = replace(upper(trim(value5)), '_', '')
    AND upper(trim(plcsys_ke)) = upper(trim(value4))
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND value6 = '1'
  AND coalesce(value5, '') != '';
