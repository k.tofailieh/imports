/* new statements to add. */
with new_statements(name, plcsys_ke, tp, path, sys_key, plcsys_co) as
         (values ('GHS_H_STMT', 'H220 + H230', 'H_STMT', 'H220 + H230', 'GHS', 'new statement.'),
                 ('GHS_H_STMT', 'H220 + H231', 'H_STMT', 'H220 + H231', 'GHS', 'new statement.'),
                 ('GHS_H_STMT', 'H220 + H232', 'H_STMT', 'H220 + H232', 'GHS', 'new statement.')),
     check_plcsys as (select coalesce(plcsys_id, -nextval('seq_plcsys_id')) as plcsys_id, new_statements.*
                      from new_statements
                           left join plcsys
                                     on upper(trim(plcsys.plcsys_ke)) = upper(trim(new_statements.plcsys_ke)) and
                                        plcsys.tp = new_statements.tp)
insert
into plcsys(plcsys_id, name, plcsys_ke, tp, path, sys_key, ns, plcsys_co)
select abs(plcsys_id),
       name,
       plcsys_ke,
       tp,
       path,
       sys_key,
       '*',
       plcsys_co
from check_plcsys
where plcsys_id < 0;


/* categories */
with classification(src_ke, dst_ke, tp, entr_tp) as
         (values ('Pyr. Gas', 'H232', array ['GHS06'], 'GHS_H_STMT'),
                 ('Chem. Unst. Gas A', 'H220 + H230', array ['GHS06', 'GHS07', 'GHS08'], 'GHS_H_STMT'),
                 ('Chem. Unst. Gas B', 'H220 + H231', array ['GHS06', 'GHS07', 'GHS08'], 'GHS_H_STMT'))
insert
into import.import_line(imp_ref, value1, value2, value3, value4)
select 'new-statements-import-test-2', src_ke, dst_ke, unnest(tp), entr_tp
from classification
;

/* get category id*/
update import.import_line
set value_id1 = plcsys_id
from plcsys
where imp_ref = :imp_ref
  and lower(trim(value1)) = lower(trim(plcsys_ke))
  and coalesce(value1, '') != ''
;

/* get statement id */
update import.import_line
set value_id2 = plcsys_id
from plcsys
where imp_ref = :imp_ref
  and lower(trim(value2)) = lower(trim(plcsys_ke))
  and coalesce(value2, '') != ''
;

/* get de_id */
update import.import_line
set value_id3 = de_id
from de
where lower(trim(value3)) = lower(trim(de_ke))
  and imp_ref = :imp_ref;


/* link category with de */
insert into der_plcsys(der_id, der_src_id, der_dst_id, der_tp, der_co)
select nextval('seq_der_plcsys_id'), value_id1, value_id3, 'VALID_FOR_REGULATORY', 'implement-test'
from import.import_line
where imp_ref = :imp_ref
  and coalesce(value_id1, 0) != 0
  and coalesce(value_id3, 0) != 0
group by value_id1, value_id3
on conflict (der_src_id, der_dst_id, der_tp) do nothing
;


/* link statement with de */
insert into der_plcsys(der_id, der_src_id, der_dst_id, der_tp, der_co)
select nextval('seq_der_plcsys_id'), value_id2, value_id3, 'VALID_FOR_REGULATORY', 'implement-test'
from import.import_line
where imp_ref = :imp_ref
  and coalesce(value_id2, 0) != 0
  and coalesce(value_id3, 0) != 0
group by value_id2, value_id3
on conflict (der_src_id, der_dst_id, der_tp) do nothing
;

/* get old relations to delete them. */
with allRelations as (select imp_id,
                             value1,
                             value_id1               as entr_src_id,
                             value_id2               as entr_dst_id,
                             value4 || '_' || value3 as entr_tp
                      from import.import_line
                      where imp_ref = :imp_ref),
     relationsToDelete as (select imp_id,
                                  entr_id,
                                  entr_src_id,
                                  entr_tp,
                                  entr_plcsys_plcsys.entr_dst_id,
                                  allRelations.value1,
                                  plcsys_ke
                           from entr_plcsys_plcsys
                                inner join allRelations using (entr_src_id, entr_tp)
                                inner join plcsys on entr_plcsys_plcsys.entr_dst_id = plcsys.plcsys_id)
insert
into import.import_matching(imp_id, entr_table, internal_id, entr_dst_id, entr_tp)
select imp_id, 'entr_plcsys_plcsys', entr_id, entr_dst_id, entr_tp
from relationsToDelete
where not exists(
        select 1 from import.import_matching where relationsToDelete.imp_id = import_matching.imp_id
    )
;

/* delete all selected relations. */
delete
from entr_plcsys_plcsys
where entr_id in (select import_matching.internal_id
                  from import.import_matching
                       inner join import.import_line using (imp_id)
                  where imp_ref = :imp_ref)
;

/* link category with statment */
insert into entr_plcsys_plcsys(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('seq_entr_plcsys_plcsys_id'),
       value_id1,
       value_id2,
       'GHS_H_STMT_' || upper(trim(value3)),
       'implement-test'
from import.import_line
where imp_ref = :imp_ref
  and coalesce(value_id1, 0) != 0
  and coalesce(value_id2, 0) != 0
group by value_id1, value_id2, value3
on conflict (entr_src_id, entr_dst_id, entr_tp) do nothing
;


with SelectedPlcsys as (select imp_id,
                               p1.plcsys_ke    p1_ke,
                               p1.plcsys_id as p1_id,
                               p2.plcsys_ke    p2_ke,
                               p2.plcsys_id as p2_id
                        from import.import_line
                             left join plcsys p1 on lower(trim(value1)) = lower(trim(p1.plcsys_ke))
                             left join plcsys p2 on lower(trim(value2)) = lower(trim(p2.plcsys_ke))
                        where imp_ref = :imp_ref
                        order by imp_id desc)
update import.import_line
set value_id1 = p1_id,
    value_id2 = p2_id
from SelectedPlcsys
where imp_ref = :imp_ref
  and SelectedPlcsys.imp_id = import_line.imp_id;