select
            reg_id as entity_id, name, reg_desc, reg_tp, active,
            decountry.de_ke as zone, deindustry.de_ke as industry, deVisible.de_ke as formulacheck
         from reg
            left outer join der_reg as derCountry on reg.reg_id = derCountry.der_src_id and derCountry.der_tp = 'REGULATION_COUNTRY'
            left outer join de as decountry on derCountry.der_dst_id = decountry.de_id
            left outer join der_reg as derIndustry on reg.reg_id = derIndustry.der_src_id and derIndustry.der_tp = 'REGULATION_MAIN_INDUSTRY'
            left outer join de as deindustry on derIndustry.der_dst_id = deindustry.de_id
            left outer join der_reg as derType on reg.reg_id = derType.der_src_id and derType.der_tp = 'REGULATION_TYPE'
            left outer join de as deType on derType.der_dst_id = deType.de_id
            /* Get formulacheck is visible value */
    		left join der_reg as derVisible on reg.reg_id = derVisible.der_src_id and derVisible.der_tp = 'VISIBLE_IN_FORMULACHECK'
    		left join de as deVisible on derVisible.der_dst_id = deVisible.de_id
			/* We get the associated project*/
			LEFT JOIN (select * from entr_project_reg
				INNER JOIN project ON entr_project_reg.entr_src_id = project.project_id and project.tp ='PROJECT_COSMETIC_BLACKLIST'
				WHERE entr_project_reg.entr_tp = 'CONCERNS') project on project.entr_dst_id = reg.reg_id AND project.entr_tp = 'CONCERNS'
			/* We get the associated master project*/
			LEFT JOIN entr_project_project
				ON entr_project_project.entr_dst_id = project.project_id AND entr_project_project.entr_tp = 'CONCERNS'
			LEFT JOIN public.project AS masterproject
				ON entr_project_project.entr_src_id = masterproject.project_id
         where reg.active = 1
			and reg.ns = 'EcoMundo'
			  group by reg.reg_id, decountry.de_ke, deindustry.de_ke, deVisible.de_ke order by name asc