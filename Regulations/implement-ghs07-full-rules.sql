/* upaload the import-file into import_line with these specifications:
   value1: path (not necessary)
   value2: tp  (not necessary)
   value3: entr_char1
   value4: entr_char2
   value5: entr_tp
   value6: GHS07

   in the queries we will use also:
        value_id1: de_id
        value_id2: sub-category_id
        value10, value11, value12: update message for entr_plcsys_plcsys and der_plcsys.
*/

/* Check. */
------------------------------------------------------------------------------------------------------------------------
/* Get plcsys_id. */
UPDATE import.import_line
SET internal_id = plcsys_id
FROM plcsys
WHERE upper(trim(value3)) = upper(trim(plcsys_ke)) -- do the matching by plcsys_ke.
  AND imp_ref = :imp_ref;

/* Error for not matched records. */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || E' | Cannot implement GH07 for this entity, it doesn\'t  matched with DB'
WHERE internal_id IS NULL
  AND imp_ref = :imp_ref;


/* Error for GH07 = null */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || E' | Must define GH07 to 0 or 1'
WHERE imp_ref = :imp_ref
  AND coalesce(value6, '') = '';
-- comment: need to provide a statistic if the cases like this exists

/* Error for GH07 = null */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || E' | No Need to Implement'
WHERE imp_ref = :imp_ref
  AND coalesce(value6, '') = '0';

-- get sub-categories ids into value_id2
UPDATE import.import_line
SET value_id2 = plcsys_id
FROM plcsys
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND coalesce(value5, '') != '' -- not a category
  -- do the matching by plcsys_ke and name
  AND replace(upper(trim(name)), '_', '') = replace(upper(trim(value5)), '_', '')
  AND upper(trim(plcsys_ke)) = upper(trim(value4));

-- check mismatched sub-categories:
UPDATE import.import_line
SET imp_warn = TRUE,
    value12  = coalesce(value12, '') || '| mismatched sub-category'
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND coalesce(value5, '') != ''
  AND value_id2 IS NULL;


/* get de_id for GHS07 into value_id1 */
UPDATE import.import_line
SET value_id1 = de_id
FROM de
WHERE coalesce(imp_err, FALSE) = FALSE -- for categories and sub-categories
  AND de_ke LIKE 'GHS07'
  AND imp_ref = :imp_ref;

/* Flag if der_plcsys is exists for some records (for categories). */
UPDATE import.import_line
SET imp_warn = TRUE,
    value10  = coalesce(value10, '') ||
               ' | already implement GHS07(der_plcsys for this record is already exists).'
FROM der_plcsys
WHERE (der_src_id = internal_id)
  AND der_dst_id = value_id1
  AND der_tp = 'VALID_FOR_REGULATORY'
  AND imp_ref = :imp_ref
  AND coalesce(value5, '') = ''
  AND coalesce(imp_err, FALSE) = FALSE;

/* Flag if der_plcsys is exists for some records (for sub-categories). */
UPDATE import.import_line
SET imp_warn = TRUE,
    value11  = coalesce(value11, '') ||
               ' | already implement GHS07(der_plcsys for this record is already exists).'
FROM der_plcsys
WHERE der_src_id = value_id2
  AND der_dst_id = value_id1
  AND der_tp = 'VALID_FOR_REGULATORY'
  AND coalesce(value5, '') != ''
  AND imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE;


/* Flag if entr_plcsys_plcsys is exists. */
UPDATE import.import_line
SET imp_warn = TRUE,
    value12  = coalesce(value12, '') ||
               ' | already implement GHS07(entr_plcsys_plcsys for this record is already exists).'
FROM entr_plcsys_plcsys
WHERE (entr_src_id = internal_id)
  AND entr_dst_id = value_id2
  AND entr_tp LIKE value5 || '_GHS07'
  AND imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE;


/* Commit. */
------------------------------------------------------------------------------------------------------------------------
BEGIN;
/* Link the categories with de(GHS07) */
INSERT INTO der_plcsys(der_id, der_src_id, der_dst_id, der_tp, der_co)
SELECT nextval('seq_der_plcsys_id'), internal_id, value_id1, 'VALID_FOR_REGULATORY', 'GHS07 implement test.'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND coalesce(value5, '') = '' -- it is a category.
  AND value10 IS NULL
GROUP BY internal_id, value_id1;


/* This update is to avoid creating the link many times. */
UPDATE import.import_line
SET value10 = ' | der_plcsys is implemented successfully.'
WHERE imp_ref = :imp_ref
  AND coalesce(import_line.imp_err, FALSE) = FALSE
  AND value10 IS NULL
  AND coalesce(value5, '') = '';


/* Link the sub-categories with de(GHS07) */
INSERT INTO der_plcsys(der_id, der_src_id, der_dst_id, der_tp, der_co)
SELECT nextval('seq_der_plcsys_id'), value_id2, value_id1, 'VALID_FOR_REGULATORY', 'GHS07 implement test.'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND coalesce(value5, '') != '' -- it is not a category.
  AND value11 IS NULL
GROUP BY value_id2, value_id1;

/* This update is to avoid creating the link many times. */
UPDATE import.import_line
SET value11 = coalesce(value11, '') || ' | der_plcsys is Implemented successfully.'
WHERE imp_ref = :imp_ref
  AND coalesce(import_line.imp_err, FALSE) = FALSE
  AND value11 IS NULL
  AND coalesce(value5, '') != '';

/* insert entr_plcsys_plcsys */
INSERT INTO entr_plcsys_plcsys(entr_id, entr_tp, entr_src_id, entr_dst_id, entr_char1, entr_char2, entr_co)
SELECT nextval('seq_entr_plcsys_plcsys_id'),
       upper(trim(value5)) || '_GHS07',
       import_line.internal_id,
       value_id2,
       trim(value3),
       trim(value4),
       'GH07 implement test.'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(import_line.imp_err, FALSE) = FALSE
  AND coalesce(value5, '') != ''
  AND value12 IS NULL -- if the link hadn't created.
GROUP BY internal_id, value_id2, trim(value3), trim(value4), upper(trim(value5));

/* This update is to avoid creating the link many times. */
UPDATE import.import_line
SET value12 = coalesce(value12, '') || '| entr_plcsys_plcsys is implemented successfully.'
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND value12 ISNULL
  AND coalesce(value5, '') != '';

COMMI

    T;
------------------------------------------------------------------------------------------------------------------------
/* Reporting */
-- Not Implemented Records.
SELECT value1      AS path,
       value2      AS tp,
       value3      AS entr_char1,
       value4      AS entr_char2,
       value5      AS entr_tp,
       value6      AS GHS07,
       imp_err_msg AS error_message
FROM import.import_line
WHERE imp_err = TRUE
  AND imp_ref = :imp_ref;

-- Implemented with Warnings
SELECT value1      AS path,
       value2      AS tp,
       value3      AS entr_char1,
       value4      AS entr_char2,
       value5      AS entr_tp,
       value6      AS GHS07,
       value10 related_to_category_der_plcsys,
       value11 related_to_sub_category_der_plcsys,
       value12 related_to_entr_plcsys_plcsys
FROM import.import_line
WHERE imp_warn = TRUE
  and coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;
