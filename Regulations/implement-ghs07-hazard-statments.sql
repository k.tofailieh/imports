/*
    value3: GHS Building Blocks DB.
    value4: HClassCat.
    value5: GHS 07.
*/

/* get statement id */
UPDATE import.import_line
SET value_id3 = plcsys_id
FROM plcsys
WHERE imp_ref = :imp_ref
  AND lower(trim(value3)) = lower(trim(plcsys_ke))
  AND coalesce(value3, '') != ''
;


/* get category id */
UPDATE import.import_line
SET value_id4 = plcsys_id
FROM plcsys
WHERE imp_ref = :imp_ref
  AND lower(trim(value4)) = lower(trim(plcsys_ke))
  AND coalesce(value4, '') != ''
;

/* Warning For Not Matched Cases. */
UPDATE import.import_line
SET imp_warn     = TRUE,
    imp_warn_msg = CASE
                       WHEN value_id3 IS NULL AND value_id4 IS NULL THEN coalesce(imp_err_msg,
                                                                                  'Statement and Category are not matched with DB')
                       WHEN value_id3 IS NULL THEN coalesce(imp_err_msg, 'Statement is not matched with DB')
                       ELSE coalesce(imp_err_msg, 'Category is not matched with DB')
        END
WHERE (value_id3 ISNULL OR value_id4 ISNULL)
  AND imp_ref = :imp_ref
;

UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || '| No Need to Import.'
WHERE imp_ref = :imp_ref
  AND coalesce(value5, '0') != '1'
;

/* Get de_id */
UPDATE import.import_line
SET value_id5 = de_id
FROM de
WHERE upper(trim(de_ke)) LIKE 'GHS07'
  AND imp_ref = :imp_ref
;

/* Link statement with de */
INSERT INTO der_plcsys(der_id, der_src_id, der_dst_id, der_tp, der_co)
SELECT nextval('seq_der_plcsys_id'), value_id3, value_id5, 'VALID_FOR_REGULATORY', 'implement-ghs07-test'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id3, 0) != 0
  AND NOT coalesce(imp_err, FALSE)
GROUP BY value_id3, value_id5
ON CONFLICT (der_src_id, der_dst_id, der_tp) DO NOTHING
;

/* Link category with de */
INSERT INTO der_plcsys(der_id, der_src_id, der_dst_id, der_tp, der_co);
SELECT nextval('seq_der_plcsys_id'), value_id4, value_id5, 'VALID_FOR_REGULATORY', 'implement-test'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id4, 0) != 0
  AND NOT coalesce(imp_err, FALSE)
GROUP BY value_id4, value_id5
--ON CONFLICT (der_src_id, der_dst_id, der_tp) DO NOTHING
;

/* Link statement with category */
INSERT INTO entr_plcsys_plcsys(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
SELECT nextval('seq_entr_plcsys_plcsys_id'),
       value_id3,
       value_id4,
       'GHS_H_STMT_GHS07',
       'implement-ghs07-test'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id3, 0) != 0
  AND coalesce(value_id4, 0) != 0
GROUP BY value_id3, value_id4
ON CONFLICT (entr_src_id, entr_dst_id, entr_tp) DO NOTHING
;

WITH SelectedPlcsys AS (SELECT imp_id,
                               p1.plcsys_ke    p1_ke,
                               p1.plcsys_id AS p1_id,
                               p2.plcsys_ke    p2_ke,
                               p2.plcsys_id AS p2_id
                        FROM import.import_line
                                 LEFT JOIN plcsys p1 ON lower(trim(value1)) = lower(trim(p1.plcsys_ke))
                                 LEFT JOIN plcsys p2 ON lower(trim(value2)) = lower(trim(p2.plcsys_ke))
                        WHERE imp_ref = :imp_ref
                        ORDER BY imp_id DESC)
UPDATE import.import_line
SET value_id1 = p1_id,
    value_id2 = p2_id
FROM SelectedPlcsys
WHERE imp_ref = :imp_ref
  AND SelectedPlcsys.imp_id = import_line.imp_id;

SELECT value3 as Statement, value4 as Category, value5 as ghs07, imp_warn_msg as warning_message
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND imp_warn = TRUE;
