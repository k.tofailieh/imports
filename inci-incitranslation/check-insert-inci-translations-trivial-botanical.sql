
/*
 * Translations Data is in import.import_line with this specifications:
 * imp_ref: contains the the subset name of translations : 'Botanical', 'Trivial'
 * value1: contains The Translation value.
 * value2: contains The Matching key (inci name).
 * value_id1: will be assigned the Id of new Translation when it inserted, or the Id of existing Translation in Database if it matched.
 * value_id2: will have the Id of the valid INCIs in Database.
 * imp_warn = True if inci has anther Translation (for the same country that we will insert).
 * imp_err  = True if the matching key(inci name) is not matched with any-valid inci.
 * */

/*
 * 1- Checking INCIs (put the matched inci_id in value_id2).
 * 2- Raise Error if the Translation is not matched to valid INCI.
 * 3- Raise Warning if INCI has another Translation.
 * 4- Insert Translations for the valid INCIs.
 * 5- Create a link.
 * */
/* testing condition: AND imp_id IN (27631,27632,27633,27634,28333,28335,28337,28340,28322,28324) */
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Matching with INCIs in DB. */
UPDATE "import".import_line SET value_id2 = inci_id FROM anx.inci 
WHERE imp_ref IN ('Trivial', 'Botanical') 
AND trim(upper(inciname)) = trim(upper(value2)) AND COALESCE(anx_st, 'VALID') = 'VALID';


/* Raise an error for Translations that is not matched. */
UPDATE "import".import_line SET imp_err = TRUE, imp_err_msg = COALESCE(imp_warn_msg || '| ','') || 'Not Matched to Valid INCIs.' 
WHERE imp_ref IN ('Trivial', 'Botanical') AND value_id2 IS NULL;


/* Raise a Warinig for Translations that have another Translation.*/
-- CA-US for Botanical.
UPDATE "import".import_line SET value_id1 = incitranslation_id, imp_warn = TRUE,
imp_warn_msg  = COALESCE(imp_warn_msg || '| ','') || 'This INCI has another ('|| country || ') Translation: ' || tuv_seg
FROM anx.entr_inci_incitranslation 
	INNER JOIN anx.incitranslation ON entr_dst_id = incitranslation_id AND country IN ('CA', 'US')
WHERE imp_ref = 'Botanical' AND value_id2 = entr_src_id;


-- CA for Trivial.
UPDATE "import".import_line SET value_id1 = incitranslation_id, imp_warn = TRUE,
imp_warn_msg  = COALESCE(imp_warn_msg || '| ','') || 'This INCI has another ('|| country || ') Translation: ' || tuv_seg
FROM anx.entr_inci_incitranslation 
	INNER JOIN anx.incitranslation ON entr_dst_id = incitranslation_id AND country = 'CA'
WHERE imp_ref = 'Trivial' AND value_id2 = entr_src_id;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Insert new valid translations and link them.*/

-- CA for both (Botanical and Trivial)
UPDATE "import".import_line SET value_id1 = -nextval('anx.seq_incitranslation_id')
 WHERE imp_ref IN ('Trivial', 'Botanical') 
 	AND  (value_id1 IS NULL OR value_id1 < 0) 
 	AND COALESCE(imp_err, FALSE) = FALSE
 	AND COALESCE(imp_warn, FALSE) = FALSE;
 	
INSERT INTO anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, country, co) 
	(SELECT ABS(value_id1), trim(value1), upper(trim(value1)), 'CA', 'Inserted Depends On INCI-Name Matching.'
	 FROM "import".import_line
	 WHERE imp_ref IN ('Trivial', 'Botanical') 
	 	AND  value_id1 < 0
	 	AND COALESCE(imp_err, FALSE) = FALSE
		AND COALESCE(imp_warn, FALSE) = FALSE);
		
		
INSERT INTO anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp) 
	(SELECT nextval('anx.seq_entr_inci_incitranslation_id'), value_id2, ABS(value_id1), 'TRANSLATION' AS tp
	 FROM "import".import_line il
	 WHERE imp_ref IN ('Trivial', 'Botanical') AND value_id1 < 0
  	  	AND COALESCE(imp_err, FALSE) = FALSE
		AND COALESCE(imp_warn, FALSE) = FALSE);
	  	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Insert new valid translations and link them.*/

-- US for Botanical.
UPDATE "import".import_line SET value_id1 = -nextval('anx.seq_incitranslation_id') 
 WHERE imp_ref = 'Botanical'
 	AND  (value_id1 IS NULL OR value_id1 < 0) 
 	AND COALESCE(imp_err, FALSE) = FALSE
 	AND COALESCE(imp_warn, FALSE) = FALSE;
 	
INSERT INTO anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, country, co) 
	(SELECT abs(value_id1), trim(value1), upper(trim(value1)), 'US', 'Inserted Depends On INCI-Name Matching.'
	 FROM "import".import_line
	 WHERE imp_ref = 'Botanical'
	 	AND  value_id1 < 0 
 	 	AND COALESCE(imp_err, FALSE) = FALSE
 		AND COALESCE(imp_warn, FALSE) = FALSE);
		
		
INSERT INTO anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp) 
	(SELECT nextval('anx.seq_entr_inci_incitranslation_id'), value_id2, abs(value_id1), 'TRANSLATION' AS tp
	 FROM "import".import_line
	 WHERE imp_ref = 'Botanical' 
	 AND value_id1 < 0
	 AND COALESCE(imp_err, FALSE) = FALSE
	 AND COALESCE(imp_warn, FALSE) = FALSE);	  	
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Report about Errors and Warnings.*/
SELECT imp_ref, value1,imp_err, imp_err_msg  FROM "import".import_line 
 WHERE imp_ref IN ('Trivial', 'Botanical') AND  imp_err = TRUE
ORDER BY imp_ref;
 
 
 SELECT imp_ref "subset name", value1 AS "translation", imp_warn_msg AS "warning message", value_id1 AS translation_id, value_id2 AS inci_id
 FROM "import".import_line 
 WHERE imp_ref IN ('Trivial', 'Botanical') AND imp_warn= TRUE
ORDER BY imp_ref;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------