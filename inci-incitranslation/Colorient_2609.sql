select inci_id, inciname, cas, tuv_seg, country
from anx.inci
     left join anx.entr_inci_incitranslation on inci.inci_id = entr_inci_incitranslation.entr_src_id
     left join anx.incitranslation on entr_inci_incitranslation.entr_dst_id = incitranslation.incitranslation_id
where upper(inciname) like any (values (upper('Henna%')))
;


select inci_id, inciname, cas --, tuv_seg, country, anx_st
from anx.inci
--      left join anx.entr_inci_incitranslation on inci.inci_id = entr_inci_incitranslation.entr_src_id
--      left join anx.incitranslation on entr_inci_incitranslation.entr_dst_id = incitranslation.incitranslation_id
where anx_st = 'VALID'
;

select inciname, cas, reg.*
from anx.inci
     inner join anx.entr_regentry_inci on inci.inci_id = entr_regentry_inci.entr_dst_id
     inner join entr_reg_regentry on entr_regentry_inci.entr_src_id = entr_reg_regentry.entr_dst_id
     inner join reg on entr_reg_regentry.entr_src_id = reg.reg_id
where cas = '4548-53-2'
;


select fiche.m_id, fiche.m_ref, entr_fiche_endpt.tpsfrom, endpt_name, endpt_tp, endpt_li
from entr_fiche_endpt
     inner join fiche on entr_fiche_endpt.entr_src_id = fiche.m_id
     inner join endpt on entr_fiche_endpt.entr_dst_id = endpt.endpt_id
where fiche.m_id in (585184, 125562, 126358)
order by entr_fiche_endpt.tpsfrom desc
;


select inci_id, inciname, cas, anx_st, tuv_seg
from anx.inci
     left join anx.entr_inci_incitranslation on inci.inci_id = entr_inci_incitranslation.entr_src_id
     left join anx.incitranslation on entr_inci_incitranslation.entr_dst_id = incitranslation.incitranslation_id and
                                      country in ('US', 'CA', 'EU')
where cas in ('2379-74-0', '6535-42-8', '2051-85-6', '6371-76-2', '6410-41-9', '2814-77-9', '6535-46-2', '1314-98-3',
              '1320-07-06', '33239-19-9', '33239-19-9')
;


select distinct coalesce(nullif(s.cas, ''), '-') as cas,
                coalesce(inc.inciname, '') as name,
                coalesce(conc, 0) as conc,
                product_name,
                product_code
from fiche aler
     left join  (select enf.entr_dst_id as m_id,
                        nom_aler.conc_real_ubound as conc,
                        product.m_ref as product_name,
                        product.marguage as product_code
                 from entr_nomclit_fiche enf
                      inner join nomclit nom_aler
                                     on enf.entr_src_id = nom_aler.nomclit_id
                      inner join entr_fiche_nomclit efn on nom_aler.nomclit_id = efn.entr_dst_id and
                                                           efn.entr_tp in ('REAL_COMPO', 'REAL_COMPO_FICHE_INGREDIENT')
                      inner join fiche product on product.m_id = efn.entr_src_id
                 where enf.entr_tp = 'HAS'
                   and product.tp in ('PRODUCT', 'COSMETIC_FOMULA', 'COSMETIC_FORMULA_TEST')
                   and split_part(product.ns, '/', 1) = :user_organisation
                   and product.m_id = cast(:MId as bigint)) compo using (m_id)
     inner join anx.entr_fiche_inci efi on aler.m_id = efi.entr_src_id
     inner join anx.inci inc on efi.entr_dst_id = inc.inci_id
     inner join anx.entr_substances_inci on inc.inci_id = entr_substances_inci.entr_dst_id
     inner join substances s on entr_substances_inci.entr_src_id = s.sub_id

where split_part(aler.ns, '/', 1) = :user_organisation and aler.tp = 'ALLERGEN'
order by conc desc
;



select distinct coalesce(nullif(coalesce(inc.cas, concernd_product.cas), ''), '-') as cas
        , coalesce(inc.inciname, concernd_product.name) as name
        , coalesce(concernd_product.conc, 0) as conc
        , concernd_product.product_name
        , concernd_product.product_code
from fiche
     left join
                (select product.m_ref as product_name,
                        product.marguage as product_code,
                        aler.m_id,
                        substances.cas as cas,
                        inc.inciname as name,
                        nom_aler.conc_real_ubound as conc
                 from fiche product
                      inner join entr_fiche_nomclit efn on product.m_id = efn.entr_src_id and
                                                           efn.entr_tp in ('REAL_COMPO', 'REAL_COMPO_FICHE_INGREDIENT')
                      inner join nomclit nom_aler on efn.entr_dst_id = nom_aler.nomclit_id
                      inner join entr_nomclit_fiche enf on nom_aler.nomclit_id = enf.entr_src_id and enf.entr_tp = 'HAS'
                      inner join fiche aler on aler.m_id = enf.entr_dst_id and aler.tp = 'ALLERGEN'
                      inner join entr_fiche_substances on aler.m_id = entr_fiche_substances.entr_src_id
                      inner join substances on substances.sub_id = entr_fiche_substances.entr_dst_id and
                                               entr_fiche_substances.entr_tp = 'ALLERGEN'

                      inner join anx.entr_fiche_inci efi on aler.m_id = efi.entr_src_id
                      inner join anx.inci inc on efi.entr_dst_id = inc.inci_id
                 where product.tp in ('PRODUCT', 'COSMETIC_FOMULA', 'COSMETIC_FORMULA_TEST')
                   and product.m_id = cast(:MId as bigint)
                   and split_part(product.ns, '/', 1) = :user_organisation) concernd_product
                    on fiche.m_id = concernd_product.m_id
     inner join anx.entr_fiche_inci efi on fiche.m_id = efi.entr_src_id
     inner join anx.inci inc on efi.entr_dst_id = inc.inci_id
where fiche.tp = 'ALLERGEN'
  and split_part(fiche.ns, '/', 1) = :user_organisation
order by conc desc;













