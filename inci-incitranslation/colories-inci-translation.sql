SELECT *
FROM import.import_line
WHERE imp_ref = 'inci-colories-import-1'
ORDER BY imp_dt DESC;

/*
Upload transformed version of inci-colories file into import line with these specifications:
    mixte: value1,
    inci_id: value_id1,
    cas: value24,
    currenttranslation_txt: value2,
    currenttranslation_id: value_id2,
    country: value25,
    newtranslation: value3,
    link_id: value_id3.
*/

/* check for the records that no need to update. */
UPDATE import.import_line
SET imp_warn     = TRUE,
    imp_warn_msg = coalesce(imp_warn_msg, '') || '| No Need to update this translation.'
WHERE value2 = value3
  AND imp_ref = :imp_ref;


/*
the splitting is done in two rounds:
   - the first round do the split by '/' and concatenate this part with the part that come from splitting by '_'
   - the second round do cleaning for the result and also split some strings that are not splitted by the first round.
   */
/* I used this function: (regexp_split_to_table) to split the string into many parts each of them will appear in a row. */

WITH get_siplitted_translations AS (SELECT *,
                                           coalesce((nullif(split_part(value3, '/', 1), '')), '') || '/' ||
                                           trim(regexp_split_to_table(split_part(value3, '/', 2), '_')) AS new_translation
                                    FROM import.import_line
                                    WHERE imp_ref = 'inci-colories-import-1'),
     get_second_siplitted_translations AS (SELECT *,
                                                  trim(regexp_split_to_table(coalesce(nullif(new_translation, '/'),
                                                                                      left(new_translation, length(new_translation) - 1)),
                                                                             '_')) AS new_translation_1
                                           FROM get_siplitted_translations)
        ,
     get_partitioned_translations AS (SELECT imp_id,
                                             imp_ref,
                                             value1,
                                             value_id1,
                                             value2,
                                             value_id2,
                                             value3,
                                             value25,
                                             imp_warn,
                                             CASE
                                                 WHEN right(new_translation_1, 1) = '/'
                                                     THEN left(new_translation_1, length(new_translation_1) - 1)
                                                 ELSE new_translation_1 END                      AS new_translation_1,
                                             row_number() OVER (PARTITION BY value_id1,value25 ) AS row_number
                                      FROM get_second_siplitted_translations)
/*
   this update statement is useful to update the existing translations in DB.
   by putting the new_translation in value4 and row_number in value5.*/
-- if you want to see the results of splitting please uncomment this:
-- SELECT * FROM get_partitioned_translations;
UPDATE import.import_line
SET value4 = new_translation_1,
    value5 = row_number
FROM get_partitioned_translations n_translation
WHERE import_line.imp_id = n_translation.imp_id
  AND row_number = 1
  AND coalesce(n_translation.value_id2, 0) != 0
  AND coalesce(n_translation.imp_warn, FALSE) = FALSE;


WITH get_siplitted_translations AS (SELECT *,
                                           coalesce((nullif(split_part(value3, '/', 1), '')), '') || '/' ||
                                           trim(regexp_split_to_table(split_part(value3, '/', 2), '_')) AS new_translation
                                    FROM import.import_line
                                    WHERE imp_ref = 'inci-colories-import-1'),
     get_second_siplitted_translations AS (SELECT *,
                                                  trim(regexp_split_to_table(coalesce(nullif(new_translation, '/'),
                                                                                      left(new_translation, length(new_translation) - 1)),
                                                                             '_')) AS new_translation_1
                                           FROM get_siplitted_translations)
        ,
     get_partitioned_translations AS (SELECT imp_id,
                                             imp_ref,
                                             value1,
                                             value_id1,
                                             value2,
                                             value_id2,
                                             value3,
                                             value25,
                                             imp_warn,
                                             CASE
                                                 WHEN right(new_translation_1, 1) = '/'
                                                     THEN left(new_translation_1, length(new_translation_1) - 1)
                                                 ELSE new_translation_1 END                      AS new_translation_1,
                                             row_number() OVER (PARTITION BY value_id1,value25 ) AS row_number
                                      FROM get_second_siplitted_translations)

/* insert the new split translations into value4 with their row_number into value5 and new translation_id into value_id4. */
INSERT
INTO import.import_line(imp_id, imp_ref, value1, value_id1, value2, value_id2, value3, value25, value4, value5,
                        value_id4)
    (SELECT nextval('import.import_line_1_imp_id_seq'),
            imp_ref,
            value1,
            value_id1,
            value2,
            value_id2,
            value3,
            value25,
            new_translation_1,
            row_number,
            nextval('anx.seq_incitranslation_id')
     FROM get_partitioned_translations n_translation
     WHERE (row_number > 1 OR coalesce(n_translation.value_id2, 0) = 0)
       AND coalesce(n_translation.imp_warn, FALSE) = FALSE)
;

/*
   for the records that are in import_line:
    1- update the translations for whose value5 = 1 (that exists in DB)
    2- insert new translation and link it with inci for whose value5 > 1.
    3- don't touch the records that have imp_warn = true.
*/

/* update parent translations. (whose value5 = 1) */
UPDATE anx.incitranslation
SET tuv_seg      = trim(value4),
    tuv_charonly = upper(trim(regexp_replace(value4, '[\d]+', '')))
FROM import.import_line
WHERE incitranslation_id = value_id2
  AND cast(value5 as integer) = 1
  AND value_id4 IS NULL
  AND coalesce(imp_warn, FALSE) = FALSE
  AND imp_ref = :imp_ref returning incitranslation_id;

/* insert new translations and link them */
INSERT
INTO anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, country)
SELECT value_id4, value4, upper(trim(regexp_replace(value4, '[\d]+', ''))), value25
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_warn, FALSE) = FALSE
  AND value_id4 IS NOT NULL;

/* link the new translations. */
INSERT INTO anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('anx.seq_entr_inci_incitranslation_id'), value_id1, value_id4, 'TRANSLATION'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_warn, FALSE) = FALSE
  AND value_id4 IS NOT NULL;

