/*
value1: Description(en	USA) --> ke
value2: Code --> short_li.

tp: OLFACTIVE_GROUP

*/

WITH exist_lov AS (SELECT imp_id, lov_id
                   FROM import.import_line
                            LEFT JOIN lov ON upper(trim(value1)) = upper(trim(ke)) AND
                                             upper(trim(ns)) LIKE
                                             upper(trim(:ns || '%')) AND upper(trim(tp)) = 'OLFACTIVE_GROUP'
                   WHERE imp_ref = :imp_ref)
UPDATE import.import_line
SET value_id1 = coalesce(lov_id, -nextval('seq_lov_id'))
FROM exist_lov
WHERE import_line.imp_id = exist_lov.imp_id;


INSERT
INTO lov(lov_id, ke, short_li, tp, st, ns, co)
SELECT abs(value_id1),
       value1,
       value2,
       'OLFACTIVE_GROUP',
       'VALID',
       :ns,
       'inserted from import file.(as testing for "OLFACTIVE_GROUP".'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value_id1 < 0;

select * from lov where tp = 'OLFACTIVE_GROUP' and ns is null;

SELECT * from entr_study_lov where entr_dst_id = 5546;

DELETE from entr_study_lov where entr_id = 1884

delete from lov where lov_id = 5546;


select * from lov where tp = 'OLFACTIVE_GROUP';

update lov set ns = 'PUIG'
where co = 'inserted from import file.(as testing for "OLFACTIVE_GROUP".';

INSERT
    INTO lov(lov_id, ke, short_li, tp, st, ns, co)
SELECT nextval('seq_lov_id'),
       ' new test kahled',
       '101',
       'OLFACTIVE_GROUP',
       'VALID',
       'EcoMundo',
       'inserted as edit testing.';

select * from anx.inci;