/*
    internal_id as sub_id
    value2      as cas
    value3      as ec
    value4      as idx
    value1      as naml
    value5      as mf
    value6      as smiles
    value7      as gr_per_mol
    value8      as Source
    value9      as Commentaire
    value10     as Note
    value11     as source_info
 */


/* Get correct sub_id: cas and ec concerned */
WITH Sub2Update AS (SELECT *
                    FROM import.import_line
                             INNER JOIN substances ON value2 = cas OR ec = value3
                    WHERE imp_ref = :imp_ref
                      AND coalesce(internal_id, -1) < 0)
UPDATE import.import_line
SET internal_id = sub_id
FROM Sub2Update
WHERE Sub2Update.imp_id = import_line.imp_id
  AND import_line.imp_ref = :imp_ref
  AND coalesce(import_line.internal_id, -1) < 0
RETURNING Sub2Update.sub_id
;

/* Get correct sub_id: naml concerned. */
WITH Sub2Update AS (SELECT *
                    FROM import.import_line
                             INNER JOIN substances ON upper(trim(value1)) = upper(trim(naml))
                    WHERE imp_ref = :imp_ref
                      AND coalesce(internal_id, -1) < 0)
UPDATE import.import_line
SET internal_id = sub_id
FROM Sub2Update
WHERE Sub2Update.imp_id = import_line.imp_id
  AND import_line.imp_ref = :imp_ref
  AND coalesce(import_line.internal_id, -1) < 0
RETURNING Sub2Update.sub_id
;

/* Update information for matched substances. */
UPDATE substances
SET naml        = value1,
    idx         = value4,
    mf          = value5,
    smiles      = value6,
    gr_per_mol  = cast(nullif(value7, '') AS double precision),
    source_info = value8,
    co          = value9,
    rqnaml      = value10,
    tech_info   = coalesce(tech_info, '') || 'info updated from import file.'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND internal_id = sub_id;


------------------------------------------------------------------------------------------------------------------------
/* Extract Mismatched Substances. */
SELECT internal_id AS sub_id,
       value2      AS cas,
       value3      AS ec,
       value4      AS idx,
       value1      AS naml,
       value5      AS mf,
       value6      AS smiles,
       value7      AS gr_per_mol,
       value8      AS "Source de l'",
       value9      AS "Commentaire",
       value10     AS "Note",
       value11     AS source_info
FROM import.import_line
         LEFT JOIN substances ON internal_id = sub_id
WHERE imp_ref = :imp_ref
  AND sub_id IS NULL;

WITH inci_count AS (SELECT entr_src_id,
                           entr_dst_id,
                           entr_tp,
                           ns,
                           m_ref,
                           fiche.tp                                                                         AS m_tp,
                           fiche.cas,
                           inciname,
                           inci.tp                                                                          AS incitp,
                           inci.cas,
                           count(entr_id) OVER (PARTITION BY entr_dst_id, ns, fiche.tp, entr_tp, fiche.cas) AS cnt
                    FROM anx.entr_fiche_inci
                             INNER JOIN fiche ON entr_src_id = m_id
                             INNER JOIN anx.inci ON inci_id = entr_fiche_inci.entr_dst_id)

SELECT *
FROM inci_count
WHERE cnt > 5
  AND m_tp = 'INGREDIENT';


SELECT active, ns, *
FROM entr_fiche_substances
         INNER JOIN fiche ON entr_fiche_substances.entr_src_id = fiche.m_id
WHERE entr_dst_id = 2600244

SELECT active, ns, *
FROM anx.entr_fiche_inci
         INNER JOIN fiche ON anx.entr_fiche_inci.entr_src_id = fiche.m_id
WHERE entr_dst_id = 55330

UPDATE fiche
SET active = 1
WHERE m_id IN (592272);

INSERT INTO anx.entr_fiche_inci(entr_id, entr_src_id, entr_dst_id, entr_tp)
VALUES (nextval('anx.seq_entr_fiche_inci_id'), 592272, 55330, 'INGREDIENT');


INSERT INTO entr_fiche_substances(entr_id, entr_src_id, entr_dst_id, entr_tp)
VALUES (nextval('seq_entr_fiche_substances_id'), 592272, 2600244, 'INGREDIENT');

SELECT *
FROM entr_fiche_substances
WHERE entr_src_id = 592273;

DELETE
FROM anx.entr_fiche_inci
WHERE entr_id IN (151185, 151186, 151187, 151188, 151189);

DELETE
FROM entr_fiche_substances
WHERE entr_id = 2848691;

WITH substances_count AS (SELECT inciname,
                                 naml,
                                 inci.cas                                       AS inci_cas,
                                 s.cas                                          AS sub_cas,
                                 count(entr_id) OVER (PARTITION BY entr_dst_id) AS cnt,
                                 row_number() OVER (PARTITION BY entr_dst_id)   AS rn,
                                 entr_dst_id

                          FROM anx.entr_substances_inci
                                   INNER JOIN substances s ON entr_substances_inci.entr_src_id = s.sub_id
                                   INNER JOIN anx.inci ON entr_substances_inci.entr_dst_id = inci.inci_id)
SELECT *, case when lag(cnt) over (PARTITION BY entr_dst_id) = cnt then '' else cast(cnt as varchar) end
FROM substances_count
WHERE cnt > 2 ;



































