/*
    internal_id as sub_id
    value1      as naml,
    value2      as cas,
    value3      as registration_number,

 */
------------------------------------------------------------------------------------------------------------------------

/* Flag mismatched substances by sub_id */
with mismatched_substances as (select imp_id, sub_id
                               from import.import_line
                                    left join substances on internal_id = sub_id
                               where imp_ref = :imp_ref and sub_id is null and internal_id is not null)
update import.import_line
set value4 = 'NOT_VALID_ID'
from mismatched_substances
where imp_ref = :imp_ref
  and mismatched_substances.imp_id = import_line.imp_id
;

/* Get right sub_id for records that are mismatched or have empty sub_id*/
with mismatched_substances as (select imp_id, sub_id
                               from import.import_line
                                    left join substances on upper(trim(value1)) = upper(trim(naml)) and
                                                            upper(trim(value2)) = upper(trim(cas))
                               where imp_ref = :imp_ref and internal_id is null or value4 = 'NOT_VALID_ID')
update import.import_line
set internal_id = coalesce(sub_id, -1)
from mismatched_substances
where imp_ref = :imp_ref
  and mismatched_substances.imp_id = import_line.imp_id
;

/* set error. */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg, '') || '| provided sub_id, (naml and cas) are not valid. '
where imp_ref = :imp_ref and internal_id < 0;

/* error for records with empty registration number or cas. */
update import.import_line
set imp_err     = true,
    imp_err_msg = '| registration number is empty.'
where imp_ref = :imp_ref
  and coalesce(value3, '') = '';
-- 144
------------------------------------------------------------------------------------------------------------------------

/* Update information for matched substances. */
update substances
set sub_xml   = format('[{"casNumber" : ["%s"],"registrationNumber" : ["%s"]}]', value2, value3),
    tech_info = coalesce(tech_info, '') || '| registration number for this substance is updated.'
from import.import_line
where imp_ref = :imp_ref
  and internal_id = sub_id
  and not coalesce(imp_err, false);
-- 150 updated.
------------------------------------------------------------------------------------------------------------------------

/* Extract Mismatched Substances. */
select internal_id as sub_id,
       value1 as naml,
       value2 as cas,
       value3 as registration_number,
       imp_err_msg as error_message
from import.import_line
     left join substances on internal_id = sub_id
where imp_ref = :imp_ref
  and imp_err = true;

