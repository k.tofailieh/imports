WITH get_m_id AS (SELECT m_id, value1, marguage, value2, m_ref, tp, count(*) OVER (PARTITION BY value1) AS cnt
                  FROM import.import_line
                           LEFT JOIN fiche f ON trim(marguage) = trim(value1)
                  WHERE imp_ref = :imp_ref
                    AND m_id IS NOT NULL)
SELECT *
FROM get_m_id
WHERE cnt > 1;

WITH set1 AS (SELECT * FROM import.import_line WHERE imp_ref = :imp_ref),
     set2 AS (SELECT * FROM import.import_line WHERE imp_ref = :imp_ref2)
SELECT set2.value4 AS entity_id, set2.value1 AS code, set2.value2 AS name, set2.value3 AS price
FROM set1
         RIGHT JOIN set2 ON set1.value1 = set2.value1;

DELETE
FROM import.import_line
WHERE imp_ref IN (:imp_ref, :imp_ref2)


INSERT INTO de (de_id, de_tp, de_ke, de_li)
VALUES (nextval('seq_de_id'), 'ENDPT_CAT', 'SyMo', 'SyMo'),
       (nextval('seq_de_id'), 'ENDPT_CAT', 'SyIng', 'SyIng'),
       (nextval('seq_de_id'), 'ENDPT_CAT', 'ORG_CPAI', 'ORG_CPAI'),
       (nextval('seq_de_id'), 'ENDPT_CAT', 'CPAI', 'CPAI'),
       (nextval('seq_de_id'), 'ENDPT_CAT', 'ORG_PPAI', 'ORG_PPAI'),
       (nextval('seq_de_id'), 'ENDPT_CAT', 'PPAI', 'PPAI')
RETURNING de_id;


/*
64287
64288
64289
64290
64291
64292
*/


-- exercise 1:
SELECT item_id, item.name AS item_name, item.category AS item_category, item.adjective AS item_adjective, event_time
FROM dsv1069.view_item_events view_event
         INNER JOIN dsv1069.items item ON item.id = view_event.item_id
ORDER BY event_time DESC;


SELECT user_id,
       user_.first_name,
       user_.last_name,
       user_.email_address,
       view_number,
       item.name,
       item.category,
       event_time AS view_at
FROM (SELECT user_id,
             item_id,
             ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY event_time DESC) AS view_number,
             event_time
      FROM dsv1069.view_item_events) recent_views
         INNER JOIN dsv1069.users user_ ON user_.id = recent_views.user_id
         INNER JOIN dsv1069.items item ON item.id = recent_views.item_id;



SELECT to_char(created_at, 'YYYY-MM-DD') AS day, count(*) AS orders_count
FROM dsv1069.orders
GROUP BY to_char(created_at, 'YYYY-MM-DD')
ORDER BY day DESC;



SELECT to_char(created_at, 'YYYY-MM-DD') AS day,
       d_rlp.date,
       d_rlp.d7_ago,
       d_rlp.d28_ago,
       count(*)                          AS orders_count
FROM dsv1069.orders
         INNER JOIN dsv1069.dates_rollup d_rlp ON to_char(created_at, 'YYYY-MM-DD') = to_char(d_rlp.date, 'YYYY-MM-DD')
GROUP BY to_char(created_at, 'YYYY-MM-DD'), d_rlp.date, d_rlp.d7_ago, d_rlp.d28_ago
ORDER BY day DESC;

SELECT count(DISTINCT parameter_value)
FROM dsv1069.events
WHERE event_name = 'test_assignment'
  AND parameter_name = 'test_id';

