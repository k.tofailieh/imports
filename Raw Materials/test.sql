select imp_ref, value10, value_txt10 from import.import_line order by imp_id desc


WITH allRMs AS (SELECT DISTINCT value1 AS m_ref, value2 AS code
                FROM import.import_line
                WHERE imp_ref IN :imp_parent_refs
                  AND coalesce(cust_id, '') = '')
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg || ' | ', '') || 'The RM does not exists in the tab RM or in database.'
WHERE imp_ref = :imp_ref
  AND coalesce(internal_id, 0) = 0
  AND (coalesce(import_line.value_txt10, '-'), import_line.value10) NOT IN
      (SELECT coalesce(code, '-'), m_ref FROM allRMs)
  AND coalesce(cust_id, '') = ''
;