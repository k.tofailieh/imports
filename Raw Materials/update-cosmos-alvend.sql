/*
entity_id: internal_id
Nom commercial: value10
Code de la matière première: value_txt10
    % PPAI: value1
    % PPAI BIO: value2
    % CPAI: value3
    % CPAI BIO: value4
    % Synthétique: value5
    % Greffon pétrochimique: value6
 */

/* copy Ids from value7 to internal_id */
UPDATE import.import_line
SET internal_id = cast(value7 AS bigint)
WHERE imp_ref = :imp_ref
  AND coalesce(value7, '') != '';


/* Flag matched records BY_ID. */
UPDATE import.import_line
SET value8 = 'Matched, BY_ID'
FROM fiche
WHERE internal_id = m_id
  -- AND upper(split_part(ns, '/', 1)) = upper(:user_ns)
  AND imp_ref = :imp_ref
;

/* Get m_id for not matched records BY_Id. */
UPDATE import.import_line
SET internal_id = m_id,
    value8      = 'Matched, BY_CODE'
FROM fiche
WHERE upper(trim(value_txt10)) = upper(trim(marguage))
  AND coalesce(upper(trim(value10)), '') = coalesce(upper(trim(m_ref)), '')

  AND tp IN ('RAW_MATERIAL', 'MASTER_RAW_MATERIAL')
  AND upper(split_part(ns, '/', 1)) = :user_ns
  AND active <> 0
  AND imp_ref = :imp_ref
  AND coalesce(value8, '') = ''
;

/*
UPDATE import.import_line
SET internal_id = -nextval('seq_fiche_id'),
    value8      = 'TO_ADD'
WHERE imp_ref = :imp_ref
  AND value8 IS NULL
  AND NOT (coalesce(value10, '') = '' AND coalesce(value_txt10, '') = '')
;

/* insert new RMs */
INSERT INTO fiche(m_id, m_ref, marguage, ns, tp)
SELECT abs(internal_id), coalesce(value10, ''), value_txt10, trim(:user_ns), 'RAW_MATERIAL'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value8 = 'TO_ADD'
  AND internal_id < 0
;
*/
/* Get endpt cosmos Ids.  */

WITH CosmosCatDescription AS (SELECT efe.entr_src_id,
                                     efe.entr_tp,
                                     max(CASE WHEN de_ke = 'PPAI' THEN e.endpt_id ELSE NULL END)     AS ppai_id,
                                     max(CASE WHEN de_ke = 'ORG_PPAI' THEN e.endpt_id ELSE NULL END) AS org_ppai_id,
                                     max(CASE WHEN de_ke = 'CPAI' THEN e.endpt_id ELSE NULL END)     AS cpai_id,
                                     max(CASE WHEN de_ke = 'ORG_CPAI' THEN e.endpt_id ELSE NULL END) AS org_cpai_id,
                                     max(CASE WHEN de_ke = 'SyIng' THEN e.endpt_id ELSE NULL END)    AS sying_id,
                                     max(CASE WHEN de_ke = 'SyMo' THEN e.endpt_id ELSE NULL END)     AS symo_id
                              FROM entr_fiche_endpt efe
                                       INNER JOIN endpt e
                                                  ON efe.entr_dst_id = e.endpt_id
                                       INNER JOIN der_endpt ON der_endpt.der_src_id = e.endpt_id AND der_tp = 'ENDPT_DESC'
                                       INNER JOIN de ON der_endpt.der_dst_id = de.de_id AND de_tp = 'ENDPT_CAT'
                                  AND de.de_ke IN
                                      ('SyMo', 'SyIng', 'ORG_CPAI', 'CPAI', 'ORG_PPAI', 'PPAI')
                                  AND coalesce(nullif(endpt_tp, ''), 'ENDPT') = 'ENDPT'
                                  AND coalesce(nullif(value_operator, ''), '=') = '='
                              GROUP BY efe.entr_src_id, efe.entr_tp),
     rm_regulatory_restrictions AS
         (SELECT DISTINCT internal_id                                                          AS rm_id,
                          COALESCE(CosmosCatDescription.ppai_id, -nextval('seq_endpt_id'))     AS ppai_id,
                          COALESCE(CosmosCatDescription.org_ppai_id, -nextval('seq_endpt_id')) AS org_ppai_id,
                          COALESCE(CosmosCatDescription.cpai_id, -nextval('seq_endpt_id'))     AS cpai_id,
                          COALESCE(CosmosCatDescription.org_cpai_id, -nextval('seq_endpt_id')) AS org_cpai_id,
                          COALESCE(CosmosCatDescription.sying_id, -nextval('seq_endpt_id'))    AS sying_id,
                          COALESCE(CosmosCatDescription.symo_id, -nextval('seq_endpt_id'))     AS symo_id
          FROM IMPORT.import_line
                   INNER JOIN fiche f ON f.m_id = abs(internal_id)
                   LEFT JOIN CosmosCatDescription ON CosmosCatDescription.entr_src_id = f.m_id AND
                                                     CosmosCatDescription.entr_tp = 'COSMOS_CONTENT'
          WHERE imp_ref = :imp_ref
            AND value1 IS NOT NULL
            AND internal_id IS NOT NULL)
/* update the import_line table with the rawmaterial disposal conditions data */

UPDATE import.import_line
SET value_id1 = ppai_id, /* endpt_id in value_id1 for PPAI */
    value_id2 = org_ppai_id, /* endpt_id in value_id2 for ORG_PPAI */
    value_id3 = cpai_id, /* endpt_id in value_id3 for CPAI */
    value_id4 = org_cpai_id, /* endpt_id in value_id4 for ORG_CPAI */
    value_id5 = sying_id, /* endpt_id in value_id5 for SYING */
    value_id6 = symo_id /* endpt_id in value_id6 for SYMO */
FROM rm_regulatory_restrictions
WHERE imp_ref = :imp_ref
  AND value1 IS NOT NULL
  AND abs(internal_id) = abs(rm_id);


/* add Cosmos-Descriptor-Id to import_line */
WITH CosmsosDercriptors AS (SELECT DISTINCT max(CASE WHEN de_ke = 'PPAI' THEN de_id ELSE NULL END)     AS ppai_id,
                                            max(CASE WHEN de_ke = 'ORG_PPAI' THEN de_id ELSE NULL END) AS org_ppai_id,
                                            max(CASE WHEN de_ke = 'CPAI' THEN de_id ELSE NULL END)     AS cpai_id,
                                            max(CASE WHEN de_ke = 'ORG_CPAI' THEN de_id ELSE NULL END) AS org_cpai_id,
                                            max(CASE WHEN de_ke = 'SyIng' THEN de_id ELSE NULL END)    AS sying_id,
                                            max(CASE WHEN de_ke = 'SyMo' THEN de_id ELSE NULL END)     AS symo_id
                            FROM de
                            WHERE de_ke IN ('SyMo', 'SyIng', 'ORG_CPAI', 'CPAI', 'ORG_PPAI', 'PPAI')
                              AND de_tp = 'ENDPT_CAT')
UPDATE import.import_line
SET value11 = cast(ppai_id AS varchar),
    value12 = cast(org_ppai_id AS varchar),
    value13 = cast(cpai_id AS varchar),
    value14 = cast(org_cpai_id AS varchar),
    value15 = cast(sying_id AS varchar),
    value16 = cast(symo_id AS varchar)
FROM CosmsosDercriptors
WHERE imp_ref = :imp_ref
  AND NOT coalesce(imp_err, FALSE)
  AND import_line.internal_id IS NOT NULL
;

------------------------------------------------------------------------------------------------------------------------
/* Insert new endpoint if it doesn't already exist (PPAI) */
INSERT
INTO endpt (endpt_id, val, value_unit, value_operator, endpt_tp, ns)
SELECT DISTINCT abs(value_id1), value1, '%', '=', 'ENDPT', :user_ns
FROM IMPORT.import_line
WHERE imp_ref = :imp_ref
  AND COALESCE(value_id1, 0) < 0
  AND value1 IS NOT NULL
;

/* Insert new endpoint if it doesn't already exist (ORG_PPAI) */
INSERT
INTO endpt (endpt_id, val, value_unit, value_operator, endpt_tp, ns)
SELECT DISTINCT abs(value_id2), value2, '%', '=', 'ENDPT', :user_ns
FROM IMPORT.import_line
WHERE imp_ref = :imp_ref
  AND COALESCE(value_id2, 0) < 0
  AND value2 IS NOT NULL
;

/* Insert new endpoint if it doesn't already exist (CPAI)*/
INSERT
INTO endpt (endpt_id, val, value_unit, value_operator, endpt_tp, ns)
SELECT DISTINCT abs(value_id3), value3, '%', '=', 'ENDPT', :user_ns
FROM IMPORT.import_line
WHERE imp_ref = :imp_ref
  AND COALESCE(value_id3, 0) < 0
  AND value3 IS NOT NULL
;

/* Insert new endpoint if it doesn't already exist (ORG_CPAI) */
INSERT
INTO endpt (endpt_id, val, value_unit, value_operator, endpt_tp, ns)
SELECT DISTINCT abs(value_id4), value4, '%', '=', 'ENDPT', :user_ns
FROM IMPORT.import_line
WHERE imp_ref = :imp_ref
  AND COALESCE(value_id4, 0) < 0
  AND value4 IS NOT NULL
;

/* Insert new endpoint if it doesn't already exist (SYING)*/
INSERT
INTO endpt (endpt_id, val, value_unit, value_operator, endpt_tp, ns)
SELECT DISTINCT abs(value_id5), value5, '%', '=', 'ENDPT', :user_ns
FROM IMPORT.import_line
WHERE imp_ref = :imp_ref
  AND COALESCE(value_id5, 0) < 0
  AND value5 IS NOT NULL
;

/* Insert new endpoint if it doesn't already exist (SYMO) */
INSERT
INTO endpt (endpt_id, val, value_unit, value_operator, endpt_tp, ns)
SELECT DISTINCT abs(value_id6), value6, '%', '=', 'ENDPT', :user_ns
FROM IMPORT.import_line
WHERE imp_ref = :imp_ref
  AND COALESCE(value_id6, 0) < 0
  AND value6 IS NOT NULL
;
------------------------------------------------------------------------------------------------------------------------
/* insert new relation between the de and the endpoint if it doesn't already exist (PPAI) */
INSERT INTO der_endpt (der_id, der_tp, der_src_id, der_dst_id)
SELECT nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id1), abs(cast(value11 AS bigint))
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id1, 0) < 0
  AND value11 IS NOT NULL
GROUP BY value_id1, value11;

/* insert new relation between the de and the endpoint if it doesn't already exist (ORG_PPAI) */
INSERT INTO der_endpt (der_id, der_tp, der_src_id, der_dst_id)
SELECT nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id2), abs(cast(value12 AS bigint))
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id2, 0) < 0
  AND value1 IS NOT NULL
GROUP BY value_id2, value12;

/* insert new relation between the de and the endpoint if it doesn't already exist (CPAI) */
INSERT INTO der_endpt (der_id, der_tp, der_src_id, der_dst_id)
SELECT nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id3), abs(cast(value13 AS bigint))
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id3, 0) < 0
  AND value13 IS NOT NULL
GROUP BY value_id3, value13;

/* insert new relation between the de and the endpoint if it doesn't already exist (ORG_CPAI) */
INSERT INTO der_endpt (der_id, der_tp, der_src_id, der_dst_id)
SELECT nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id4), abs(cast(value14 AS bigint))
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id4, 0) < 0
  AND value14 IS NOT NULL
GROUP BY value_id4, value14;

/* insert new relation between the de and the endpoint if it doesn't already exist (SYING) */
INSERT INTO der_endpt (der_id, der_tp, der_src_id, der_dst_id)
SELECT nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id5), abs(cast(value15 AS bigint))
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id5, 0) < 0
  AND value15 IS NOT NULL
GROUP BY value_id5, value15;

/* insert new relation between the de and the endpoint if it doesn't already exist (SYMO) */
INSERT INTO der_endpt (der_id, der_tp, der_src_id, der_dst_id)
SELECT nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id6), abs(cast(value16 AS bigint))
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id6, 0) < 0
  AND value16 IS NOT NULL
GROUP BY value_id6, value16;

------------------------------------------------------------------------------------------------------------------------
/* link the new inserted endpt with it's RM. (PPAI) */
INSERT INTO entr_fiche_endpt(entr_id, entr_tp, entr_src_id, entr_dst_id)
SELECT nextval('seq_entr_fiche_endpt_id'), 'COSMOS_CONTENT', abs(internal_id), abs(value_id1)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value1 IS NOT NULL
  AND NOT coalesce(imp_err, FALSE)
  AND coalesce(value_id1, 0) < 0
;

/* link the new inserted endpt with it's RM. (ORG_PPAI) */
INSERT INTO entr_fiche_endpt(entr_id, entr_tp, entr_src_id, entr_dst_id)
SELECT nextval('seq_entr_fiche_endpt_id'), 'COSMOS_CONTENT', abs(internal_id), abs(value_id2)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value2 IS NOT NULL
  AND NOT coalesce(imp_err, FALSE)
  AND coalesce(value_id2, 0) < 0
;

/* link the new inserted endpt with it's RM. (CPAI) */
INSERT INTO entr_fiche_endpt(entr_id, entr_tp, entr_src_id, entr_dst_id)
SELECT nextval('seq_entr_fiche_endpt_id'), 'COSMOS_CONTENT', abs(internal_id), abs(value_id3)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value3 IS NOT NULL
  AND NOT coalesce(imp_err, FALSE)
  AND coalesce(value_id3, 0) < 0
;

/* link the new inserted endpt with it's RM. (ORG_CPAI) */
INSERT INTO entr_fiche_endpt(entr_id, entr_tp, entr_src_id, entr_dst_id)
SELECT nextval('seq_entr_fiche_endpt_id'), 'COSMOS_CONTENT', abs(internal_id), abs(value_id4)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value4 IS NOT NULL
  AND NOT coalesce(imp_err, FALSE)
  AND coalesce(value_id4, 0) < 0
;

/* link the new inserted endpt with it's RM. (SYING) */
INSERT INTO entr_fiche_endpt(entr_id, entr_tp, entr_src_id, entr_dst_id)
SELECT nextval('seq_entr_fiche_endpt_id'), 'COSMOS_CONTENT', abs(internal_id), abs(value_id5)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value5 IS NOT NULL
  AND NOT coalesce(imp_err, FALSE)
  AND coalesce(value_id5, 0) < 0
;

/* link the new inserted endpt with it's RM. (SYNO) */
INSERT INTO entr_fiche_endpt(entr_id, entr_tp, entr_src_id, entr_dst_id)
SELECT nextval('seq_entr_fiche_endpt_id'), 'COSMOS_CONTENT', abs(internal_id), abs(value_id6)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value6 IS NOT NULL
  AND NOT coalesce(imp_err, FALSE)
  AND coalesce(value_id6, 0) < 0
;

------------------------------------------------------------------------------------------------------------------------
/* update the existing endpoint linked to the RM (PPAI) */
UPDATE endpt e
SET val = value1
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id1, 0) > 0
  AND coalesce(value1, '') != ''
  AND e.endpt_id = value_id1
;

/* update the existing endpoint linked to the RM (PPAI) */
UPDATE endpt e
SET val = value2
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id2, 0) > 0
  AND coalesce(value2, '') != ''
  AND e.endpt_id = value_id2
;

/* update the existing endpoint linked to the RM (PPAI) */
UPDATE endpt e
SET val = value3
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id3, 0) > 0
  AND coalesce(value3, '') != ''
  AND e.endpt_id = value_id3
;

/* update the existing endpoint linked to the RM (PPAI) */
UPDATE endpt e
SET val = value4
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id4, 0) > 0
  AND coalesce(value4, '') != ''
  AND e.endpt_id = value_id4
;

/* update the existing endpoint linked to the RM (PPAI) */
UPDATE endpt e
SET val = value5
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id5, 0) > 0
  AND coalesce(value5, '') != ''
  AND e.endpt_id = value_id5
;

/* update the existing endpoint linked to the RM (PPAI) */
UPDATE endpt e
SET val = value6
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id6, 0) > 0
  AND coalesce(value6, '') != ''
  AND e.endpt_id = value_id6
;


------------------------------------------------------------------------------------------------------------------------
SELECT m_id,
       fiche.ns,
       tp,
       m_ref,
       marguage,
       val,
       de_ke,
       endpt.endpt_id,
       entr_fiche_endpt.*
FROM fiche
         LEFT JOIN entr_fiche_endpt ON entr_src_id = m_id AND entr_tp = 'COSMOS_CONTENT'
         LEFT JOIN endpt ON entr_dst_id = endpt_id
         LEFT JOIN der_endpt ON der_src_id = endpt_id AND der_tp = 'ENDPT_DESC'
         LEFT JOIN de ON der_dst_id = de_id
WHERE endpt.endpt_id IS NULL
  AND tp = 'RAW_MATERIAL'
  AND split_part(fiche.ns, '/', 1) = 'EcoMundo';


SELECT endpt_tp
FROM entr_fiche_endpt
         INNER JOIN endpt ON entr_dst_id = endpt_id
WHERE entr_tp = 'COSMOS_CONTENT'
  AND entr_src_id = 542125;


INSERT INTO import.import_line(imp_ref, internal_id, value1, value2, value3, value4, value5, value6)
VALUES ('import-rm-cosmos-19', 574820, 60, 50, 40, 30, 20, 10);

SELECT m_id, tp, ns
FROM import.import_line
         INNER JOIN fiche f ON import_line.internal_id = f.m_id
WHERE imp_ref = :imp_ref;


SELECT imp_ref, * from import.import_line ORDER BY imp_id DESC;