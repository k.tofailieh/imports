/*
    value1:entity_id
    value2:code
    value3:name
    value4:price
*/

/* copy entity_id into value_id1 */
UPDATE import.import_line
SET value_id1 = cast(value1 AS bigint)
WHERE imp_ref = :imp_ref
  AND coalesce(value1, '') != '';

/* Get m_id for not matched records BY_ID. */
UPDATE import.import_line
SET value_id1 = m_id
FROM fiche
WHERE upper(trim(value2)) = upper(trim(marguage))
  AND coalesce(upper(trim(value3)), '') = coalesce(upper(trim(m_ref)), '')

  AND tp IN ('RAW_MATERIAL', 'MASTER_RAW_MATERIAL')
  AND upper(split_part(ns, '/', 1)) = :user_ns
  AND active <> 0
  AND imp_ref = :imp_ref
  AND value_id1 IS NULL
;

/* Create new raw material for not matched records. */
UPDATE import.import_line
SET value_id1 = -nextval('seq_fiche_id')
WHERE imp_ref = :imp_ref
  AND value_id1 IS NULL
;

/* find the endpt_id for existing price */
UPDATE import.import_line
SET value_id2 = entr_dst_id
FROM entr_fiche_endpt
WHERE imp_ref = :imp_ref
  AND value_id1 IS NOT NULL /* id */
  AND NOT (coalesce(import_line.imp_err, FALSE)) /*no err */
  AND entr_src_id = value_id1
  AND entr_tp = 'PRICE'
;
/* insert negative ids for the new prices/endpt */
WITH distinct_RM AS (SELECT DISTINCT value_id1 AS RM_id
                     FROM import.import_line
                     WHERE value_id1 IS NOT NULL
                       AND value_id2 IS NULL
                       AND imp_ref = :imp_ref
                       AND NOT (COALESCE(imp_err, FALSE)))
   , new_endpt_ids AS (SELECT -nextval('seq_endpt_id') AS newId, RM_id FROM distinct_RM)
UPDATE import.import_line
SET value_id2 = newId
FROM new_endpt_ids
WHERE imp_ref = :imp_ref
  AND value_id1 = RM_id
;

/* find de_id for price */
UPDATE import.import_line
SET value_id3 = de_id
FROM de
WHERE imp_ref = :imp_ref
  AND de_ke = 'PRICE'
  AND NOT (COALESCE(imp_err, FALSE))
;


------------------------------------------------------------------------------------------------------------------------
BEGIN;
/* update the existing price with new values */
UPDATE endpt
SET val = coalesce(nullif(value4, ''), val)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(value_id2, 0) > 0
  AND endpt_id = value_id2
;

/* insert the new RMs */
INSERT INTO fiche(m_id, m_ref, marguage, ns, tp)
SELECT abs(value_id1), coalesce(value10, ''), value_txt10, trim(:user_ns), 'RAW_MATERIAL'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value_id1 < 0
;

/* insert new endpt */
INSERT INTO endpt (endpt_id,
                   val,
                   ns)
SELECT abs(value_id2),
       value3,
       :user_ns
FROM import.import_line
WHERE imp_ref = :imp_ref /* Current upload */
  AND coalesce(imp_err, FALSE) = FALSE /* No error */
  AND coalesce(value_id2, 0) < 0 /* New endpt */
;

/*link RM to price/ insert entr_fiche_endpt*/
INSERT INTO entr_fiche_endpt(entr_id,
                             entr_src_id,
                             entr_dst_id,
                             entr_tp)
SELECT nextval('seq_entr_fiche_endpt_id'),
       abs(value_id1),
       abs(value_id2),
       'PRICE'
FROM import.import_line
WHERE imp_ref = :imp_ref /* Current upload */
  AND coalesce(imp_err, FALSE) = FALSE /* No error */
  AND NOT exists(SELECT 1
                 FROM entr_fiche_endpt
                 WHERE entr_src_id = value_id1
                   AND entr_dst_id = abs(value_id2)
                   AND entr_tp = 'PRICE'
    )
  AND value_id1 IS NOT NULL
  AND value_id2 IS NOT NULL;
;

/*link endpt to de price*/
INSERT INTO der_endpt(der_id,
                      der_src_id,
                      der_dst_id,
                      der_tp)
SELECT nextval('seq_der_endpt_id'),
       abs(value_id2),
       value_id3,
       'ENDPT_DESC'
FROM import.import_line
WHERE imp_ref = :imp_ref /* Current upload */
  AND coalesce(imp_err, FALSE) = FALSE /* No error */
  AND NOT exists(SELECT 1
                 FROM der_endpt
                 WHERE der_src_id = abs(value_id2)
                   AND der_dst_id = value_id3
                   AND der_tp = 'ENDPT_DESC'
    )
  AND value_id2 IS NOT NULL
  AND value_id3 IS NOT NULL
;
COMMIT;