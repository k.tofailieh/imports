/*
    * Common Fields:
          UN/ID no. : internal_id  - char1
          Proper Shipping Name/Description : value2 - char2
          Class or Div. (Sub Hazard) : value3 - char3
          Subsidiary hazards(s) -- Labels: value6 - char6
          Packing group : value5 - char5
          Special provisions: value7
          Limited and excepted quantity provisions Ltd Qty: value8
          Limited and excepted quantity provisions Excepted Qty: value9

    * For IMDG:
          Packing Instructions: value10
          Packing Provisions: value11
          IBC Instructions: value12
          IBC Provisions: value13
          Portable tanks and bulk containers Reserved: value14
          Portable tanks and bulk containers Provisions: value15
          Portable tanks and bulk containers Tank Instructions: value16
          EmS : value17
          Stowage and Handling : value18
          Segregation : value19

    * For ADR:
          Packaging Packing instructions: value10
          Packaging Special packing provisions: value11
          Packaging Mixed packing provisions: value12
          Portable tanks and bulk containers Instructions: value13
          Portable tanks and bulk containers Special provisions: value14
          ADR tank Tank code: value15
          ADR tank Special provisions: value16
          Vehicle for tank carriage: value17
          Transport category (Tunnel restriction code): value18
          Special provisions for carriage Packages: value19
          Special provisions for carriage Bulk: value20
          Special provisions for carriage Loading, unloading and handling: value21
          Special provisions for carriage Operation: value22
          Hazard identification No.: value23

    * For IATA:
           UN/ID no.: internal_id
           Proper Shipping Name/Description: value2 - char2
           Class or Div. (Sub Hazard): value3 - char3
           Hazard Labels: value6 - char6
           Packing group: value5 - char5
           Spec. Prov. see 4.4: value7
           Passenger and Cargo Aircraft EQ (see 2.6): value8
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst: value9
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge: value10
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst.1: value11
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge.1: value12
           Cargo Aircraft Only Pkg Inst: value13
           Cargo Aircraft Only Max Net Qty/Pkge: value14
           ERG Code: value16

    * Constant values:
        sys_ke: value24 = 'IMDG' or 'ADR'
        name: value25 = 'IMDG' or 'ADR'
        tp: value26 = 'IMDG' or 'ADR'
*/

/* Check Exists plcsys */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = imp_err_msg || ' | This record is already exists in DB.'
FROM plcsys
WHERE plcsys_char1 = lpad(CAST(internal_id AS VARCHAR), 4, '0')
  AND trim(upper(plcsys_char2)) = trim(upper(value2))
  AND imp_ref = :imp_ref;

/* Import new plcsys */
INSERT INTO plcsys(plcsys_id, plcsys_ke, path, sys_key, name, tp, plcsys_char1, plcsys_char2, plcsys_char3,
                   plcsys_char4, plcsys_char5,
                   plcsys_char6, plcsys_co, plcsys_info);
SELECT DISTINCT nextval('seq_plcsys_id'),
       lpad(CAST(internal_id AS VARCHAR), 4, '0') || '-' || 'plcsys_techcode' as plcsys_ke,
       trim(value26) || '/' || lpad(CAST(internal_id AS VARCHAR), 4, '0') || '-' || 'plcsys_techcode' as path  ,
       trim(value24) as sys_ke,
       trim(value25) as name,
       trim(value26) as tp,
       lpad(CAST(internal_id AS VARCHAR), 4, '0') as char1, -- UN NO.
       trim(value2) as char2,                               -- Name
       trim(value3) as char3,                               -- Class
       trim(value4) as char4,                               -- Classification Code
       trim(value5) as char5,                               -- Packaging Group
       trim(value6) as char6,                               -- Labels
       'import-imdg-adr-test' as co,
       (CASE
         WHEN value26 = 'IMDG' THEN
            E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
            E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >'
            || '<src>' || '' || '</src>' ||
            '<noOnu>' || lpad(CAST(internal_id AS VARCHAR), 4, '0') || '</noOnu>' ||
            E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
            '<class>' || trim(value3) || '</class>' ||
            '<classCode>' || trim(value3) || '</classCode>' ||
            '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
             '<labels>' || string_agg(label, '') OVER (PARTITION BY internal_id, value26, value2) ||
             '</labels>' ||
            '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
            '<imdg>' ||
                '<limitedQte>' || trim(value8) || '</limitedQte>' ||
                '<exceptedQte>' || trim(value9) || '</exceptedQte>' ||
                '<packagingInstructions>' || trim(value10) || '</packagingInstructions>' ||
                '<specialPackagingProvisions>' || trim(value11) || '</specialPackagingProvisions>' ||
                '<ibcInstructions>' || trim(value12) || '</ibcInstructions>' ||
                '<ibcProvisions>' || trim(value13) || '</ibcProvisions>' ||
                '<containersReversed>' || trim(value14) || '</containersReversed>' ||
                '<containersProvisions>' || trim(value15) || '</containersProvisions>' ||
                '<tankInstructions>' || trim(value16) || '</tankInstructions>' ||
                '<ems>' || trim(value17) || '</ems>' ||
                '<stowageAndHandling>' || value18 || '</stowageAndHandling>' ||
                '<segregation>' || value19 || '</segregation>' ||
            '</imdg>' ||
            '</adr>'
        WHEN value26 = 'ADR' THEN
        E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
             E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
             '<src>' || '' || '</src>' ||
             '<noOnu>' || lpad(CAST(internal_id AS VARCHAR), 4, '0') || '</noOnu>' ||
             E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
             '<class>' || trim(value3) || '</class>' ||
             '<classCode>' || trim(value4) || '</classCode>' ||
             '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
             '<labels>' ||
                 string_agg(label, '') OVER (PARTITION BY internal_id, value26, value2) ||
             '</labels>' ||
             '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
             '<adr>' ||
                 '<limitedQte>' || trim(value8) || '</limitedQte>' ||
                 '<exceptedQte>' || trim(value9) || '</exceptedQte>' ||
                 '<packagingInstructions>' || trim(value10) || '</packagingInstructions>' ||
                 '<specialPackagingProvisions>' || trim(value11) || '</specialPackagingProvisions>' ||
                 '<mixedPackagingProvisions>' || trim(value12) || '</mixedPackagingProvisions>' ||
                 '<tankInstructions>' || trim(value13) || '</tankInstructions>' ||
                 '<tankSpecialProvisions>' || trim(value14) || '</tankSpecialProvisions>' ||
                 '<adrTankCode>' || trim(value15) || '</adrTankCode>' ||
                 '<adrTankSpecialProvisions>' || trim(value16) || '</adrTankSpecialProvisions>' ||
                 '<vehiculeForTank>' || trim(value17) || '</vehiculeForTank>' ||
                 '<transportCat>' || trim(value18) || '</transportCat>' ||
                 '<specialProvisionsForPackages>' || trim(value19) || '</specialProvisionsForPackages>' ||
                 '<specialProvisionsForBulk>' || trim(value20) || '</specialProvisionsForBulk>' ||
                 '<specialProvisionsForHandling>' ||trim(value21) || '</specialProvisionsForHandling>' ||
                 '<specialProvisionsForOperation>' ||trim(value22) || '</specialProvisionsForOperation>' ||
                 '<hazardId>' || trim(value23) || '</hazardId>' ||
             '<adr>' ||
         '</adr>'
           when value26 = 'IATA' then
             E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
            E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >'
            || '<src>' || '' || '</src>' ||
            '<noOnu>' || lpad(CAST(internal_id AS VARCHAR), 4, '0') || '</noOnu>' ||
            E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
            '<class>' || trim(value3) || '</class>' ||
            '<classCode>' || trim(value3) || '</classCode>' ||
            '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
             '<labels>' || string_agg(label, '') OVER (PARTITION BY internal_id, value26, value2) ||
             '</labels>' ||
            '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
            '<iata>' ||
                '<passengerCargoEq>' || value8 || '</passengerCargoEq>' ||
                '<passengerCargoPackagingInstructions>' || value9 || '</passengerCargoPackagingInstructions>' ||
                '<passengerCargoMaxNetQty>' || value10 || '</passengerCargoMaxNetQty>' ||
                '<passengerCargoPackagingInstructions.1>' || value11 || '</passengerCargoPackagingInstructions.1>' ||
                '<passengerCargoMaxNetQty.1>' || value12 || '</passengerCargoMaxNetQty.1>' ||
                '<cargoPackagingInstructions>' || value13 || '</cargoPackagingInstructions>' ||
                '<cargoMaxNetQty>' || value14 || '</cargoMaxNetQty>' ||
                '<ergCode>'||  value15 || '</ergCode>' ||
            '</iata>' ||
            '</adr>'
           END) as info
FROM import.import_line
         INNER JOIN
     (SELECT imp_id, '<label>' || translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') || '</label>' AS label
      FROM import.import_line
      WHERE imp_ref = :imp_ref) label_agg USING (imp_id)
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

-------------------------------------------------------------

SELECT * from plcsys
where plcsys_char1 = '0012'





