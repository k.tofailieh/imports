/* see the duplications */
WITH get_counts AS (SELECT imp_id,
                           internal_id                                   AS un_no,
                           value2                                        AS description,
                           value3                                        AS class,
                           value4                                        AS class_code,
                           value5                                        AS packaging_group,
                           value6                                        AS labels,
                           value7                                        AS special_provisions,
                           row_number() OVER (PARTITION BY internal_id)  AS row_number,
                           count(imp_id) OVER (PARTITION BY internal_id, value2) AS cnt
                    FROM import.import_line
                    WHERE imp_ref = :imp_ref
                      AND value25 = :tp -- ADR or IMDG or IATA
)
SELECT *
FROM import.import_line
WHERE imp_id IN (SELECT imp_id FROM get_counts WHERE row_number > 1)

/* Duplicated ADR's over UN-NO */
WITH adr AS (SELECT *,
                    count(imp_id) OVER (PARTITION BY internal_id)                                AS cnt,
                    count(imp_id) OVER (PARTITION BY internal_id,value2, value5, value4, value6) AS cnt_name
             FROM import.import_line
             WHERE imp_ref = :imp_ref
               AND value25 = 'ADR'),
     imdg AS (SELECT *,
                     count(imp_id) OVER (PARTITION BY internal_id)                                AS cnt,
                     count(imp_id) OVER (PARTITION BY internal_id,value2, value5, value4, value6) AS cnt_name
              FROM import.import_line
              WHERE imp_ref = :imp_ref
                AND value25 = 'IMDG'),
     iata AS (SELECT *,
                     count(imp_id) OVER (PARTITION BY internal_id)                                AS cnt,
                     count(imp_id) OVER (PARTITION BY internal_id,value2, value5, value4, value6) AS cnt_name
              FROM import.import_line
              WHERE imp_ref = :imp_ref
                AND value25 = 'IATA')
SELECT DISTINCT cnt_name                                         AS times_appeared,
                count(*) OVER (PARTITION BY cnt_name) / cnt_name AS records_count,
                count(*) OVER (PARTITION BY cnt_name)            AS total
FROM iata;


/* Duplicated IMDG Except Description */
WITH adr AS (SELECT *,
--                     row_number()
--                     OVER (PARTITION BY internal_id,value2, value3, value4, value5,value6, value7, value8, value9,value10,value11,value12,value13,value14,value15,value16,value17, value18, value19, value20, value21, value22, value23) AS row_number,
                    count(imp_id)
                    OVER (PARTITION BY internal_id, value3, value4, value5,value6, value7, value8, value9,value10,value11,value12,value13,value14,value15,value16,value17, value18, value19, value20, value21, value22, value23) AS cnt_name
             FROM import.import_line
             WHERE imp_ref = :imp_ref
               AND value25 = 'ADR'),
     imdg AS (SELECT *,
                     count(imp_id) OVER (PARTITION BY internal_id)                                                                                                                            AS cnt,
                     count(imp_id)
                     OVER (PARTITION BY internal_id, value3, value4, value5,value6, value7, value8, value9,value10,value11,value12,value13,value14,value15,value16,value17, value18, value19) AS cnt_name
              FROM import.import_line
              WHERE imp_ref = :imp_ref
                AND value25 = 'IMDG'),
     iata AS (SELECT *,
--                      row_number()
--                      OVER (PARTITION BY internal_id, value25, value3, value5, value7,value8,value9, value10,value11,value12,value13,value14,value15) AS row_number,
                     count(imp_id)
                     OVER (PARTITION BY internal_id, value3, value5,value6, value7,value8,value9, value10,value11,value12,value13,value14,value15
                         ) AS cnt_name
              FROM import.import_line
              WHERE imp_ref = :imp_ref
                AND value25 = 'IATA'),
     count_name AS (SELECT *,
                           count(imp_id) OVER (PARTITION BY internal_id, value2) AS name_cnt,
                           row_number() OVER (PARTITION BY internal_id)          AS row_number
                    FROM adr
                    WHERE cnt_name > 1)
SELECT row_number,
       internal_id AS un_no,
       value2      AS description,
       value3      AS class,
       value4      AS class_code,
       value5      AS packaging_group,
       value6      AS labels,
       value7      AS special_provisions,
       value8,
       value8,
       value9,
       value10,
       value11,
       value12,
       value13,
       value14,
       value15,
       value16,
       value17,
       value18,
       value19,
       value20,
       value21,
       value22,
       coalesce(nbPlcsysDB, 0),
       nbPlcsysFile
FROM count_name
         LEFT JOIN (SELECT plcsys_char1, count(plcsys_id) AS nbPlcsysDB
                    FROM plcsys
                    WHERE tp = 'ADR'
                    GROUP BY plcsys_char1) plcsys_subquery
                   ON plcsys_subquery.plcsys_char1 = lpad(cast(count_name.internal_id AS text), 4, '0')

        LEFT JOIN (SELECT internal_id, count(imp_id) AS nbPlcsysFile
                   FROM import.import_line
             WHERE imp_ref = :imp_ref
               AND value25 = 'ADR'
                   GROUP BY internal_id) adr_file USING (internal_id)

WHERE name_cnt = 1;



LEFT JOIN plcsys
ON lpad(CAST (internal_id AS VARCHAR), 4, '0') = plcsys_char1
WHERE plcsys_id IS NULL;


SELECT *
FROM import.import_line
WHERE imp_ref = 'adr-import-test'

SELECT plcsys_ke, trim(value2)
FROM import.import_line
         INNER JOIN plcsys ON plcsys_ke = (lpad(cast(internal_id AS varchar), 4, '0') || '-' || trim(value1))
         INNER JOIN (SELECT (lpad(cast(internal_id AS varchar), 4, '0')) plcsys_char1, value2 AS plcsys_char2
                     FROM import.import_line
                     WHERE value25 = 'ADR') adr_import
                    ON adr_import.plcsys_char1 = plcsys.plcsys_char1 AND adr_import.plcsys_char2
WHERE imp_ref = 'adr-translations'


WITH adrs AS (SELECT (lpad(cast(internal_id AS varchar), 4, '0'))                                plcsys_char1,
                     upper(regexp_replace(value2, '[[:space:]]', '', 'g'))                    AS description,
                     count(*) OVER (PARTITION BY internal_id)                                 AS adr_cnt_un_no,
                     count(imp_id) OVER (PARTITION BY internal_id, trim(upper(value2)) )      AS adr_cnt_unno_name,
                     count(*) OVER (PARTITION BY internal_id, value3, value4, value5, value6) AS adr_cnt_all,
                     *
              FROM import.import_line
              WHERE imp_ref = 'adr-import-test'
                AND value25 = 'ADR'),

     duplicated_adr AS (SELECT *
                        FROM adrs
                        WHERE adr_cnt_un_no > 1
                          AND adr_cnt_all > 1),
     translations AS (SELECT (lpad(cast(internal_id AS varchar), 4, '0'))                           plcsys_char1,
                             value1,
                             upper(regexp_replace(value2, '[[:space:]]', '', 'g'))               AS description,
                             (lpad(cast(internal_id AS varchar), 4, '0') || '-' || trim(value1)) AS plcsys_ke
                      FROM import.import_line
                      WHERE imp_ref = 'adr-translations')

SELECT internal_id AS un_no,
       value2      AS original_description,
       description AS cleaned_description,
       value3      AS class,
       value4      AS class_code,
       value5      AS packaging_group,
       value6      AS labels,
       value7      AS special_provisions,
       value8,
       value8,
       value9,
       value10,
       value11,
       value12,
       value13,
       value14,
       value15,
       value16,
       value17,
       value18,
       value19,
       value20,
       value21,
       value22,
       nbPlcsys,
       nbPlcsys1

FROM duplicated_adr
         LEFT JOIN translations USING (plcsys_char1, description)
         LEFT JOIN (SELECT plcsys_char1, plcsys_char3, plcsys_char4, plcsys_char5, count(plcsys_id) AS nbPlcsys
                    FROM plcsys
                    WHERE tp = 'ADR'
                    GROUP BY plcsys_char1, plcsys_char3, plcsys_char4, plcsys_char5) plcsys_subquery
                   ON plcsys_subquery.plcsys_char1 = duplicated_adr.plcsys_char1 AND
                      coalesce(duplicated_adr.value3, '') = coalesce(plcsys_char3, '') AND
                      coalesce(plcsys_char4, '') = coalesce(duplicated_adr.value4, '') AND
                      coalesce(plcsys_char5, '') = coalesce(duplicated_adr.value5, '')

         LEFT JOIN (SELECT plcsys_char1, plcsys_char5, count(plcsys_id) AS nbPlcsys1
                    FROM plcsys
                    WHERE tp = 'ADR'
                    GROUP BY plcsys_char1, plcsys_char5) plcsys_subquery1
                   ON plcsys_subquery1.plcsys_char1 = duplicated_adr.plcsys_char1 AND
                      coalesce(plcsys_subquery1.plcsys_char5, '') = coalesce(duplicated_adr.value5, '')
WHERE translations.plcsys_char1 IS NULL
ORDER BY duplicated_adr.plcsys_char1;



SELECT internal_id, count(*)
FROM import.import_line
WHERE imp_ref = 'adr-import-test'
GROUP BY internal_id
HAVING count(*) > 1








