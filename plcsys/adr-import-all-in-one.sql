/*
    * Common Fields:
          UN/ID no. : internal_id  - char1
          Proper Shipping Name/Description : value2 - char2
          Class or Div. (Sub Hazard) : value3 - char3
          Subsidiary hazards(s) -- Labels: value6 - char6
          Packing group : value5 - char5
          Special provisions: value7
          Limited and excepted quantity provisions Ltd Qty: value8
          Limited and excepted quantity provisions Excepted Qty: value9

    * For IMDG:
          Packing Instructions: value10
          Packing Provisions: value11
          IBC Instructions: value12
          IBC Provisions: value13
          Portable tanks and bulk containers Reserved: value14
          Portable tanks and bulk containers Provisions: value15
          Portable tanks and bulk containers Tank Instructions: value16
          EmS : value17
          Stowage and Handling : value18
          Segregation : value19

    * For ADR:
          Packaging Packing instructions: value10
          Packaging Special packing provisions: value11
          Packaging Mixed packing provisions: value12
          Portable tanks and bulk containers Instructions: value13
          Portable tanks and bulk containers Special provisions: value14
          ADR tank Tank code: value15
          ADR tank Special provisions: value16
          Vehicle for tank carriage: value17
          Transport category (Tunnel restriction code): value18
          Special provisions for carriage Packages: value19
          Special provisions for carriage Bulk: value20
          Special provisions for carriage Loading, unloading and handling: value21
          Special provisions for carriage Operation: value22
          Hazard identification No.: value23

    * For IATA:
           UN/ID no.: internal_id
           Proper Shipping Name/Description: value2 - char2
           Class or Div. (Sub Hazard): value3 - char3
           Hazard Labels: value6 - char6
           Packing group: value5 - char5
           Spec. Prov. see 4.4: value7
           Passenger and Cargo Aircraft EQ (see 2.6): value8
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst: value9
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge: value10
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst.1: value11
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge.1: value12
           Cargo Aircraft Only Pkg Inst: value13
           Cargo Aircraft Only Max Net Qty/Pkge: value14
           ERG Code: value16

    * Constant values:
        tp: value25 = 'IMDG' or 'ADR' or IATA

    fro my test the imp_ref is adr-import-test
*/


/* get the xml format for IMDG records*/
with imdg_spec as (
    select  DISTINCT internal_id,
            value25 as tp,
            value2,  --description
            '<imdgSpec>' ||
                 E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
                '<class>' || trim(value3) || '</class>' ||
                '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
                '<limitedQte>' || trim(value8) || '</limitedQte>' ||
                '<labels>' ||
                 string_agg(label, '') OVER (PARTITION BY internal_id, value2) ||
                '</labels>' ||
                 '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
                '<exceptedQte>' || trim(value9) || '</exceptedQte>' ||
                '<packagingInstructions>' || trim(value10) || '</packagingInstructions>' ||
                '<specialPackagingProvisions>' || trim(value11) || '</specialPackagingProvisions>' ||
                '<ibcInstructions>' || trim(value12) || '</ibcInstructions>' ||
                '<ibcProvisions>' || trim(value13) || '</ibcProvisions>' ||
                '<containersReversed>' || trim(value14) || '</containersReversed>' ||
                '<containersProvisions>' || trim(value15) || '</containersProvisions>' ||
                '<tankInstructions>' || trim(value16) || '</tankInstructions>' ||
                '<ems>' || trim(value17) || '</ems>' ||
                '<stowageAndHandling>' || value18 || '</stowageAndHandling>' ||
                '<segregation>' || value19 || '</segregation>' ||
            '</imdgSpec>' as xml
            from import.import_line
                inner join  (SELECT imp_id,
             '<label>' || translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') || '</label>' AS label
      FROM import.import_line
      WHERE imp_ref = :imp_ref and value25 = 'IMDG') label_agg USING (imp_id)
            WHERE value25 = 'IMDG'

),
/* get the xml format for ADR records */
    adr_spec as (
        select  DISTINCT imp_id,
                internal_id,
                value2, -- description
                value25 as tp,
                '<adrSpec>' ||
                 E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
                 '<class>' || trim(value3) || '</class>' ||
                 '<classCode>' || trim(value4) || '</classCode>' ||
                 '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
                 '<labels>' ||
                     string_agg(label, '') OVER (PARTITION BY internal_id, value2) ||
                 '</labels>' ||
                 '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
                 '<limitedQte>' || trim(value8) || '</limitedQte>' ||
                 '<exceptedQte>' || trim(value9) || '</exceptedQte>' ||
                 '<packagingInstructions>' || trim(value10) || '</packagingInstructions>' ||
                 '<specialPackagingProvisions>' || trim(value11) || '</specialPackagingProvisions>' ||
                 '<mixedPackagingProvisions>' || trim(value12) || '</mixedPackagingProvisions>' ||
                 '<tankInstructions>' || trim(value13) || '</tankInstructions>' ||
                 '<tankSpecialProvisions>' || trim(value14) || '</tankSpecialProvisions>' ||
                 '<adrTankCode>' || trim(value15) || '</adrTankCode>' ||
                 '<adrTankSpecialProvisions>' || trim(value16) || '</adrTankSpecialProvisions>' ||
                 '<vehiculeForTank>' || trim(value17) || '</vehiculeForTank>' ||
                 '<transportCat>' || trim(value18) || '</transportCat>' ||
                 '<specialProvisionsForPackages>' || trim(value19) || '</specialProvisionsForPackages>' ||
                 '<specialProvisionsForBulk>' || trim(value20) || '</specialProvisionsForBulk>' ||
                 '<specialProvisionsForHandling>' ||trim(value21) || '</specialProvisionsForHandling>' ||
                 '<specialProvisionsForOperation>' ||trim(value22) || '</specialProvisionsForOperation>' ||
                 '<hazardId>' || trim(value23) || '</hazardId>' ||
             '</adrSpec>'  as xml
        from import.import_line
        inner join  (SELECT imp_id,
             '<label>' || translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') || '</label>' AS label
        FROM import.import_line
        WHERE imp_ref = :imp_ref and value25 = 'ADR') label_agg USING (imp_id)
        where value25 = 'ADR'
    ),
/* get the xml format for IATA records */
    iata_spec as (
        select DISTINCT  internal_id,
               value2, -- description
               value25 as tp,
            '<iataSpec>' ||
                 E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
                '<class>' || trim(value3) || '</class>' ||
                '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
                 '<labels>' || string_agg(label, '') OVER (PARTITION BY internal_id, value2) ||
                 '</labels>' ||
                '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
                '<passengerCargoEq>' || value8 || '</passengerCargoEq>' ||
                '<ltdPassengerCargoPackagingInstructions>' || value9 || '</ltdPassengerCargoPackagingInstructions>' ||
                '<ltdPassengerCargoMaxNetQty>' || value10 || '</ltdPassengerCargoMaxNetQty>' ||
                '<passengerCargoPackagingInstructions>' || value11 || '</passengerCargoPackagingInstructions>' ||
                '<passengerCargoMaxNetQty>' || value12 || '</passengerCargoMaxNetQty>' ||
                '<cargoPackagingInstructions>' || value13 || '</cargoPackagingInstructions>' ||
                '<cargoMaxNetQty>' || value14 || '</cargoMaxNetQty>' ||
                '<ergCode>'||  value15 || '</ergCode>' ||
            '</iataSpec>' as xml
            from import.import_line
            inner join  (SELECT imp_id,
             '<label>' || translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') || '</label>' AS label
          FROM import.import_line
          WHERE imp_ref = :imp_ref and value25 = 'IATA') label_agg USING (imp_id)
            where value25 = 'IATA'
    )
SELECT
     lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '-' || '1.7' as plcsys_ke,
       'ADR' || '/' || lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '-' || '1.7' as path  ,
       'ADR' as sys_ke,
       'ADR' as name,
       'ADR' as tp,
               E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
             E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
             '<src>' || '' || '</src>' ||
             '<noOnu>' || lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '</noOnu>' ||
             E'<name lg=\'EN\'>' || trim(import_line.value2) || '</name>' ||

            coalesce(adr_spec.xml, '<adr></adr>') ||
         --   coalesce(imdg_spec.xml, '<imdg></imdg>') ||
            coalesce(iata_spec.xml, '<iata></iata>') ||
         '</adr>'
         as info
FROM
    import.import_line
         left JOIN adr_spec USING (imp_id)
--         left JOIN imdg_spec on import_line.internal_id = imdg_spec.internal_id and trim(upper(import_line.value2)) = trim(upper(imdg_spec.value2))
         inner JOIN iata_spec on import_line.internal_id = iata_spec.internal_id and trim(upper(import_line.value2)) = trim(upper(iata_spec.value2))
WHERE coalesce(imp_err, FALSE) = FALSE AND value25 = 'ADR'
order by import_line.internal_id;




/* combine the records to import into plcsys. */
SELECT /*nextval('seq_plcsys_id')*/ 1,
        import_line.internal_id,
       lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '-' || '1.7' as plcsys_ke,
       'ADR' || '/' || lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '-' || '1.7' as path  ,
       'ADR' as sys_ke,
       'ADR' as name,
       'ADR' as tp,
       lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') as char1, -- UN NO.
       trim(import_line.value2) as char2,                               -- Name
       trim(value3) as char3,                               -- Class
       trim(value4) as char4,                               -- Classification Code
       trim(value5) as char5,                               -- Packaging Group
       trim(value6) as char6,                               -- Labels
       'import-adr-iata-imdg-test' as co,

        E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
             E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
             '<src>' || '' || '</src>' ||
             '<noOnu>' || lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '</noOnu>' ||
             E'<name lg=\'EN\'>' || trim(import_line.value2) || '</name>' ||

            coalesce(adr_spec.xml, '<adr></adr>') ||
            coalesce(imdg_spec.xml, '<imdg></imdg>') ||
            coalesce(iata_spec.xml, '<iata></iata>') ||
         '</adr>'
         as info
FROM import.import_line
         left JOIN adr_spec USING (imp_id)
         left JOIN imdg_spec on import_line.internal_id = imdg_spec.internal_id and trim(upper(import_line.value2)) = trim(upper(imdg_spec.value2))
         left JOIN iata_spec on import_line.internal_id = iata_spec.internal_id and trim(upper(import_line.value2)) = trim(upper(iata_spec.value2))
WHERE coalesce(imp_err, FALSE) = FALSE AND value25 = 'ADR'
order by internal_id
;



/* see the duplications */
WITH get_counts AS (SELECT internal_id as un_no, value2 as description, value3 as class, value4 as class_code, value5 as packaging_group, value6 as labels, value7 as special_provisions,
                    row_number() over (PARTITION BY internal_id) as row_number,
                    count(imp_id) over (PARTITION BY internal_id) as cnt
                   FROM import.import_line
                   WHERE imp_ref = :imp_ref
                     AND value25 = :tp -- ADR or IMDG or IATA
                  )
SELECT  * from get_counts where cnt > 1;


