/*
UN No.: internal_id , plcsys_char1
Name and description: value2, plcsys_char2
Class: value3,plcsys_char3
Classification code: value4, plcsys_char4
Packing group: value5, plcsys_char5
Labels: value6, plcsys_char6
Special provisions: value7
Limited and excepted quantities limitedQte : value8
Limited and excepted quantities exceptedQte: value9
Packaging Packing instructions: value10
Packaging Special packing provisions: value11
Packaging Mixed packing provisions: value12
Portable tanks and bulk containers Instructions: value13
Portable tanks and bulk containers Special provisions: value14
ADR tank Tank code: value15
ADR tank Special provisions: value16
Vehicle for tank carriage: value17
Transport category (Tunnel restriction code): value18
Special provisions for carriage Packages: value19
Special provisions for carriage Bulk: value20
Special provisions for carriage Loading, unloading and handling: value21
Special provisions for carriage Operation: value22
Hazard identification No.: value23

*/



/* check exists plcsys */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = imp_err_msg || ' | This record is already exists in DB.'
FROM plcsys
WHERE plcsys_char1 = lpad(CAST(internal_id AS VARCHAR), 4, '0')
  AND trim(upper(plcsys_char2)) = trim(upper(value2))
  AND imp_ref = :imp_ref;

/* Insert records*/
select
 E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
|| E'<adr
	xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
	 '<src>' || 171.12 || '</src>' ||
	 '<noOnu>' || lpad(CAST(internal_id AS VARCHAR), 4, '0') || '</noOnu>' ||
	 E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
	 '<class>' || trim(value3) || '</class>' ||
	 '<classCode>' || trim(value4) || '</classCode>' ||
	 '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
	 '<labels>' ||
		 string_agg(label, '') OVER (PARTITION BY internal_id) ||
	 '</labels>' ||
	 '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
	 '<limitedQte>' || trim(value8) || '</limitedQte>' ||
	 '<exceptedQte>' || trim(value9) || '</exceptedQte>' ||
	 '<packagingInstructions>' || trim(value10) || '</packagingInstructions>' ||
	 '<specialPackagingProvisions>' || trim(value11) || '</specialPackagingProvisions>' ||
	 '<mixedPackagingProvisions>' || trim(value12) || '</mixedPackagingProvisions>' ||
	 '<tankInstructions>' || trim(value13) || '</tankInstructions>' ||
	 '<tankSpecialProvisions>' || trim(value14) || '</tankSpecialProvisions>' ||
	 '<adrTankCode>' || trim(value15) || '</adrTankCode>' ||
	 '<adrTankSpecialProvisions>' || trim(value16) || '</adrTankSpecialProvisions>' ||
	 '<vehiculeForTank>' || trim(value17) || '</vehiculeForTank>' ||
	 '<transportCat>' || trim(value18) || '</transportCat>' ||
	 '<specialProvisionsForPackages>' || trim(value19) || '</specialProvisionsForPackages>' ||
	 '<specialProvisionsForBulk>' || trim(value20) || '</specialProvisionsForBulk>' ||
	 '<specialProvisionsForHandling>' ||trim(value21) || '</specialProvisionsForHandling>' ||
	 '<specialProvisionsForOperation>' ||trim(value22) || '</specialProvisionsForOperation>' ||
	 '<hazardId>' || trim(value23) || '</hazardId>' ||
 '</adr>'
FROM import.import_line
         INNER JOIN
     (SELECT imp_id, '<label>' || trim(unnest(regexp_split_to_array(value3, ','))) || '</label>' AS label
      FROM import.import_line
      WHERE imp_ref = :imp_ref) label_agg USING (imp_id)
WHERE imp_ref = :imp_ref;

