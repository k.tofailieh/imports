/* insert IMDG-IATA records */

/*
SELECT value_id1,
       import_line.cust_id || '-' || value37                       AS plcsys_ke,
       'ADR' || '/' || import_line.cust_id || '-' || trim(value37) AS path,
       coalesce(adr_spec.tp || ',', '') || coalesce(imdg_spec.tp || ',', '') ||
       coalesce(iata_spec.tp, '')                                  AS sys_ke,
       'ADR'                                                       AS name,
       'ADR'                                                       AS tp,
       E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
       E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
       '<src>' || trim(import_line.value37) || '</src>' ||
       '<noOnu>' || lpad(CAST(import_line.cust_id AS VARCHAR), 4, '0') || '</noOnu>' ||
       E'<name lg=\'EN\'>' || trim(import_line.value2) || '</name>' ||
       coalesce(adr_spec.xml, '<adr></adr>') ||
       coalesce(imdg_spec.xml, '<imdg></imdg>') ||
       coalesce(iata_spec.xml, '<iata></iata>') ||
       '</adr>'
                                                                   AS info
FROM import.import_line
         INNER JOIN adr_spec USING (imp_id)
         LEFT JOIN imdg_spec ON import_line.cust_id = imdg_spec.cust_id AND
                                trim(upper(import_line.value2)) = trim(upper(imdg_spec.value2))
         LEFT JOIN iata_spec ON import_line.cust_id = iata_spec.cust_id AND
                                trim(upper(translate(import_line.value2, E'\n', '_'))) LIKE
                                trim(upper(translate(iata_spec.value2, E'\n', '_')))
WHERE coalesce(imp_err, FALSE) = FALSE
  AND value25 = 'ADR'
  AND NOT coalesce(import_line.imp_err, FALSE)
;
*/


--ORDER BY import_line.cust_id;


-- matched with id and description 1821

SELECT value2, translate(value2, E'\n', '_')
FROM import.import_line
WHERE imp_ref = :imp_ref;


-- SELECT nextval('seq_de_id'), currval('seq_de_id')

SELECT substring(cast(plcsys_id AS varchar), 1, 2) || '.' || substring(cast(plcsys_id AS varchar), 3, 4),
       plcsys_id,
       *
FROM plcsys
WHERE tp = 'ADR'
  AND plcsys_id IN (1975, 2278, 2271);


SELECT regexp_replace(E'kdfjbnkd+_-\nldknvs \\', '[[:space:]]', '_', 'g'), E'hhh\nmm';

UPDATE import.import_line
SET imp_err     = NULL,
    imp_err_msg = NULL
WHERE imp_ref = :imp_ref;

SELECT currval('seq_plcsys_id')


SELECT substr('asbscs', length('asbscs') - 3);


SELECT unnest(regexp_match(plcsys_ke, '\d+-(.+)')), plcsys_ke
FROM plcsys


SELECT trim('khaled, ', ', ');

SELECT count(*)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value25 = 'ADR'
  and value_id2 is not null and value_id3 is not null;



SELECT value37, imp_err, value_id1
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value25 = 'ADR'
  AND value_id1 < 1;



SELECT count(*)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value25 = 'ADR'
  AND value_id1 > 1;


UPDATE import.import_line
SET value_id1   = NULL,
    value_id2   = NULL,
    value_id3   = NULL,
    value37     = NULL,
    imp_err     = NULL,
    imp_err_msg = NULL
WHERE TRUE;

UPDATE import.import_line
SET value39 = null,
    value38 = NULL
WHERE TRUE;

with duplicate_plcsys as (select plcsys_char1,
                                 plcsys_ke,
                                 plcsys_char2,
                                 plcsys_char3,
                                 plcsys_char4,
                                 plcsys_char5,
                                 plcsys_char6,
                                 count(*) over (partition by plcsys_char1) as cnt
                          from plcsys
                          where tp = 'ADR')
select *
from duplicate_plcsys
where cnt > 2
;


-- 2,824 updated.
SELECT count(*)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND imp_warn is null
  AND value25 = 'IATA';
and value1 = '1'
;
-- 18 IMDG
-- 13 IATA


select * ,marguage from fiche where tp='PRODUCT' limit 10
