/* see the duplications */
WITH get_counts AS (SELECT imp_id,
                           internal_id                                                               AS un_no,
                           value2                                                                    AS description,
                           value3                                                                    AS class,
                           value4                                                                    AS class_code,
                           value5                                                                    AS packaging_group,
                           value6                                                                    AS labels,
                           value7                                                                    AS special_provisions,
                           count(imp_id) OVER (PARTITION BY cust_id)                                 AS cnt,
                           count(imp_id) OVER (PARTITION BY cust_id, value2)                         AS cnt_name,
                           count(imp_id) OVER (PARTITION BY cust_id, value3, value5, value6)         AS cnt_other,
                           count(imp_id) OVER (PARTITION BY cust_id, value2, value3, value5, value6) AS cnt_all
                    FROM import.import_line
                    WHERE imp_ref = :imp_ref
                      AND value25 = :tp -- ADR or IMDG or IATA
)
SELECT count(*)
FROM get_counts
        -- inner join plcsys on plcsys_char1 =
WHEREtrue;--cnt = 1;

-- 2930
-- ADR: (cnt>1: 963), (cnt_name>1: 594), (cnt_other>1: 112), (cnt_all>1: 0)
-- ADR: (cnt=1: 1967)

-- 2857
-- IMDG: (cnt>1: 905), (cnt_name>1: 865), (cnt_other>1: 47), (cnt_all>1: 26)
-- IMDG (cnt=1: 1952)

-- 3442
-- IATA: (cnt>1: 1720), (cnt_name>1: 1229), (cnt_other>1: 953), (cnt_all>1: 344)
-- IATA: (cnt=1: 1722)

select distinct imp_ref
from import.import_line;


select count(efn.entr_id), efn.entr_tp, RM.tp, enf.entr_tp
from fiche f
         inner join entr_fiche_nomclit efn on f.m_id = efn.entr_src_id
         inner join entr_nomclit_fiche enf on efn.entr_dst_id = enf.entr_src_id
         inner join fiche RM on enf.entr_dst_id = RM.m_id

where f.tp = 'DETERGENT' and RM.tp = 'RAW_MATERIAL'
group by efn.entr_tp, RM.tp, enf.entr_tp;

update import.import_line set imp_ref = 'adr-import-test' where imp_ref like 'adr-import-test_'
