/*
    * Common Fields:
          UN/ID no. : cust_id  - char1
          Proper Shipping Name/Description : value2 - char2
          Class or Div. (Sub Hazard) : value3 - char3
          Subsidiary hazards(s) -- Labels: value6 - char6
          Packing group : value5 - char5
          Special provisions: value7
          Limited and excepted quantity provisions Ltd Qty: value8
          Limited and excepted quantity provisions Excepted Qty: value9

    * For IMDG:
          Packing Instructions: value10
          Packing Provisions: value11
          IBC Instructions: value12
          IBC Provisions: value13
          Portable tanks and bulk containers Reserved: value14
          Portable tanks and bulk containers Provisions: value15
          Portable tanks and bulk containers Tank Instructions: value16
          EmS : value17
          Stowage and Handling : value18
          Segregation : value19

    * For ADR:
          Packaging Packing instructions: value10
          Packaging Special packing provisions: value11
          Packaging Mixed packing provisions: value12
          Portable tanks and bulk containers Instructions: value13
          Portable tanks and bulk containers Special provisions: value14
          ADR tank Tank code: value15
          ADR tank Special provisions: value16
          Vehicle for tank carriage: value17
          Transport category (Tunnel restriction code): value18
          Special provisions for carriage Packages: value19
          Special provisions for carriage Bulk: value20
          Special provisions for carriage Loading, unloading and handling: value21
          Special provisions for carriage Operation: value22
          Hazard identification No.: value23

    * For IATA:
           UN/ID no.: cust_id
           Proper Shipping Name/Description: value2 - char2
           Class or Div. (Sub Hazard): value3 - char3
           Hazard Labels: value6 - char6
           Packing group: value5 - char5
           Spec. Prov. see 4.4: value7
           Passenger and Cargo Aircraft EQ (see 2.6): value8
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst: value9
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge: value10
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst.1: value11
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge.1: value12
           Cargo Aircraft Only Pkg Inst: value13
           Cargo Aircraft Only Max Net Qty/Pkge: value14
           ERG Code: value16

    * Constant values:
        tp: value25 = 'IMDG' or 'ADR' or IATA

    fro my test the imp_ref is adr-import-test
*/


/*
Normalize
*/
UPDATE import.import_line
SET cust_id = lpad(CAST(import_line.cust_id AS VARCHAR), 4, '0'), -- cust_id to be 4 digits for example: (1 ---> 0001).
    value32 = translate(value2, E'\n', '_'),                      -- remove the "\n" (replace by '_') from description.
    value36 = regexp_replace(value6, '[[:space:]]', '_', 'g')     -- remove all space chars from Labels.

WHERE imp_ref = :imp_ref
;


/* Normalize  Class or Div. (Sub Hazard) to take an integer part if it has float part */
UPDATE import.import_line
SET value33 = regexp_replace(value3, '^(\d+)(\.?0*)$', '\1') -- remove zeros after the point, (1.000 --> 1).
WHERE imp_ref = :imp_ref
  AND value3 SIMILAR TO '\d+\.?0*'
;

/* put records count for each category based on UN-NO */
WITH un_no_counts AS (SELECT imp_id, cust_id, value25, count(*) OVER (PARTITION BY value25, cust_id) as cnt
                      FROM import.import_line
                      WHERE imp_ref = :imp_ref
                        AND coalesce(value25, '') != '')
UPDATE import.import_line
set value1 = cnt
from un_no_counts
where import_line.imp_id = un_no_counts.imp_id
  and imp_ref = :imp_ref
  and not coalesce(imp_err, false)
;

-- 1- Flag ADR, IATA and IMDG records that are duplicated
WITH duplicated_records AS (SELECT *,
                                   count(*)
                                   OVER (PARTITION BY cust_id, value2, value3/**/, value5/**/, value6/**/, value25 /*tp*/) AS cnt
                            FROM import.import_line
                            WHERE imp_ref = :imp_ref
                              AND coalesce(value25, '') != 'ADR')
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || '(duplicated) need to revise before import.'
WHERE imp_id IN (SELECT imp_id FROM duplicated_records WHERE cnt > 1);
-- 370 adr records, 25 IMDGs and 345 IATAs.


WITH duplicated_adrs AS (SELECT *,
                                count(*)
                                OVER (PARTITION BY cust_id, value3/**/, value4/**/, value5/**/, value6/**/) AS cnt
                         FROM import.import_line
                         WHERE imp_ref = :imp_ref
                           AND coalesce(value25, '') = 'ADR')
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || '(duplicated) need to revise before import.'
WHERE imp_id IN (SELECT imp_id FROM duplicated_adrs WHERE cnt > 1);
-- 106


/* Determine which ADR is exists and which is a new one. */
WITH plcsys_counts as (select *, count(*) over (partition by plcsys_char1) as cnt from plcsys where tp = 'ADR'),
     distinct_plcsys as (select * from plcsys_counts where cnt = 1),
     exist_adrs AS (SELECT imp_id, plcsys_id, plcsys_ke, plcsys_char2, value2, split_part(plcsys_ke, '-', 2) AS src
                    FROM import.import_line
                             LEFT JOIN distinct_plcsys ON plcsys_char1 = cust_id
                         --and value2 != plcsys.plcsys_char2
--                         AND coalesce(plcsys_char3, '') = coalesce(value33, plcsys_char3, '')
--                         AND coalesce(plcsys_char4, '') = coalesce(value4, plcsys_char4, '')
--                         AND coalesce(plcsys_char5, '') = coalesce(value5, plcsys_char5, '') -- Packing group.
--                         AND coalesce(plcsys_char6, '') = coalesce(value33, plcsys_char6, '')

                    WHERE imp_ref = :imp_ref
                      AND NOT coalesce(imp_err, FALSE)
                      and cast(value1 as int) = 1
                      AND value25 = 'ADR'),
     selected_adrs AS (SELECT imp_id,
                              coalesce(plcsys_id, -nextval('seq_plcsys_id')) AS plcsys_id,
                              coalesce(src, substr(cast(currval('seq_plcsys_id') AS varchar),
                                                   length(cast(currval('seq_plcsys_id') AS varchar)) - 3, 2) ||
                                            '.' ||
                                            substr(cast(currval('seq_plcsys_id') AS varchar),
                                                   length(cast(currval('seq_plcsys_id') AS varchar)) - 1,
                                                   2))                       AS src
--                               cast(abs(coalesce(plcsys_id, currval('seq_plcsys_id'))) AS varchar) AS src
                       FROM exist_adrs)
UPDATE import.import_line
SET value_id1 = plcsys_id,
    value37   = src
FROM selected_adrs
WHERE imp_ref = :imp_ref
  AND selected_adrs.imp_id = import_line.imp_id
  AND value25 = 'ADR'
  and cast(value1 as int) = 1
  AND NOT coalesce(imp_err, FALSE)
;
-- 1967 ADR records are unique and matched plcsys records (55 one of them are new ones).

-- total = 2824, will updated = 2070, new adr records = 754

/* get the xml format for IMDG records*/
WITH imdg_spec AS (SELECT DISTINCT imp_id,
                                   cust_id,
                                   value25       AS tp,
                                   value3,
                                   value5,
                                   value33,
                                   value2, --description
                                   '<imdgSpec>' ||
                                   E'<name lg=\'EN\'>' || TRIM(value2) || '</name>' ||
                                   '<class>' || TRIM(value3) || '</class>' ||
                                   '<packagingGpe>' || TRIM(value5) || '</packagingGpe>' ||
                                   '<limitedQte>' || TRIM(value8) || '</limitedQte>' ||
                                   '<labels>' ||
                                   string_agg(LABEL, '') OVER (PARTITION BY cust_id, value2) ||
                                   '</labels>' ||
                                   '<specialProvisions>' || TRIM(value7) || '</specialProvisions>' ||
                                   '<exceptedQte>' || TRIM(value9) || '</exceptedQte>' ||
                                   '<packagingInstructions>' || TRIM(value10) || '</packagingInstructions>' ||
                                   '<specialPackagingProvisions>' || TRIM(value11) || '</specialPackagingProvisions>' ||
                                   '<ibcInstructions>' || TRIM(value12) || '</ibcInstructions>' ||
                                   '<ibcProvisions>' || TRIM(value13) || '</ibcProvisions>' ||
                                   '<containersReversed>' || TRIM(value14) || '</containersReversed>' ||
                                   '<containersProvisions>' || TRIM(value15) || '</containersProvisions>' ||
                                   '<tankInstructions>' || TRIM(value16) || '</tankInstructions>' ||
                                   '<ems>' || TRIM(value17) || '</ems>' ||
                                   '<stowageAndHandling>' || value18 || '</stowageAndHandling>' ||
                                   '<segregation>' || value19 || '</segregation>' ||
                                   '</imdgSpec>' AS XML
                   FROM IMPORT.import_line
                            INNER JOIN (SELECT imp_id,
                                               '<label>' ||
                                               translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') ||
                                               '</label>' AS LABEL
                                        FROM IMPORT.import_line
                                        WHERE imp_ref = :imp_ref
                                          AND value25 = 'IMDG') label_agg USING (imp_id)
                   WHERE value25 = 'IMDG'
                     AND NOT coalesce(imp_err, FALSE)
                     and cast(value1 as int) = 1),
/* get the xml format for ADR records */
     adr_spec AS (SELECT DISTINCT imp_id,
                                  cust_id,
                                  value2, -- description
                                  value3,
                                  value33,
                                  value5,
                                  value25      AS tp,
                                  '<adrSpec>' ||
                                  E'<name lg=\'EN\'>' || TRIM(value2) || '</name>' ||
                                  '<class>' || TRIM(value3) || '</class>' ||
                                  '<classCode>' || TRIM(value4) || '</classCode>' ||
                                  '<packagingGpe>' || TRIM(value5) || '</packagingGpe>' ||
                                  '<labels>' ||
                                  string_agg(LABEL, '') OVER (PARTITION BY cust_id, value2) ||
                                  '</labels>' ||
                                  '<specialProvisions>' || TRIM(value7) || '</specialProvisions>' ||
                                  '<limitedQte>' || TRIM(value8) || '</limitedQte>' ||
                                  '<exceptedQte>' || TRIM(value9) || '</exceptedQte>' ||
                                  '<packagingInstructions>' || TRIM(value10) || '</packagingInstructions>' ||
                                  '<specialPackagingProvisions>' || TRIM(value11) || '</specialPackagingProvisions>' ||
                                  '<mixedPackagingProvisions>' || TRIM(value12) || '</mixedPackagingProvisions>' ||
                                  '<tankInstructions>' || TRIM(value13) || '</tankInstructions>' ||
                                  '<tankSpecialProvisions>' || TRIM(value14) || '</tankSpecialProvisions>' ||
                                  '<adrTankCode>' || TRIM(value15) || '</adrTankCode>' ||
                                  '<adrTankSpecialProvisions>' || TRIM(value16) || '</adrTankSpecialProvisions>' ||
                                  '<vehiculeForTank>' || TRIM(value17) || '</vehiculeForTank>' ||
                                  '<transportCat>' || TRIM(value18) || '</transportCat>' ||
                                  '<specialProvisionsForPackages>' || TRIM(value19) ||
                                  '</specialProvisionsForPackages>' ||
                                  '<specialProvisionsForBulk>' || TRIM(value20) || '</specialProvisionsForBulk>' ||
                                  '<specialProvisionsForHandling>' || TRIM(value21) ||
                                  '</specialProvisionsForHandling>' ||
                                  '<specialProvisionsForOperation>' || TRIM(value22) ||
                                  '</specialProvisionsForOperation>' ||
                                  '<hazardId>' || TRIM(value23) || '</hazardId>' ||
                                  '</adrSpec>' AS XML
                  FROM IMPORT.import_line
                           INNER JOIN (SELECT imp_id,
                                              '<label>' ||
                                              translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') ||
                                              '</label>' AS LABEL
                                       FROM IMPORT.import_line
                                       WHERE imp_ref = :imp_ref
                                         AND value25 = 'ADR') label_agg USING (imp_id)
                  WHERE value25 = 'ADR'
                    AND NOT coalesce(imp_err, FALSE)
                    and cast(value1 as int) = 1),
/* get the xml format for IATA records */
     iata_spec AS (SELECT DISTINCT imp_id,
                                   cust_id,
                                   value2, -- description
                                   value3,
                                   value33,
                                   value5,
                                   value25       AS tp,
                                   '<iataSpec>' ||
                                   E'<name lg=\'EN\'>' || TRIM(value2) || '</name>' ||
                                   '<class>' || TRIM(value3) || '</class>' ||
                                   '<packagingGpe>' || TRIM(value5) || '</packagingGpe>' ||
                                   '<labels>' || string_agg(LABEL, '') OVER (PARTITION BY cust_id, value2) ||
                                   '</labels>' ||
                                   '<specialProvisions>' || TRIM(value7) || '</specialProvisions>' ||
                                   '<passengerCargoEq>' || value8 || '</passengerCargoEq>' ||
                                   '<ltdPassengerCargoPackagingInstructions>' || value9 ||
                                   '</ltdPassengerCargoPackagingInstructions>' ||
                                   '<ltdPassengerCargoMaxNetQty>' || value10 || '</ltdPassengerCargoMaxNetQty>' ||
                                   '<passengerCargoPackagingInstructions>' || value11 ||
                                   '</passengerCargoPackagingInstructions>' ||
                                   '<passengerCargoMaxNetQty>' || value12 || '</passengerCargoMaxNetQty>' ||
                                   '<cargoPackagingInstructions>' || value13 || '</cargoPackagingInstructions>' ||
                                   '<cargoMaxNetQty>' || value14 || '</cargoMaxNetQty>' ||
                                   '<ergCode>' || value15 || '</ergCode>' ||
                                   '</iataSpec>' AS XML
                   FROM IMPORT.import_line
                            INNER JOIN (SELECT imp_id,
                                               '<label>' ||
                                               translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') ||
                                               '</label>' AS LABEL
                                        FROM IMPORT.import_line
                                        WHERE imp_ref = :imp_ref
                                          AND value25 = 'IATA') label_agg USING (imp_id)
                   WHERE value25 = 'IATA'
                     AND NOT coalesce(imp_err, FALSE)
                     and cast(value1 as int) = 1)
UPDATE import.import_line
SET value38      = trim(coalesce(adr_spec.tp || ',', '') || coalesce(imdg_spec.tp || ',', '') ||
                        coalesce(iata_spec.tp, ''), ', '),                      -- sys_ke
    value39      = 'ADR' || '/' || import_line.cust_id || '-' || trim(value37), -- path
    value40      = E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
                   E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
                   '<src>' || trim(import_line.value37) || '</src>' ||
                   '<noOnu>' || lpad(CAST(import_line.cust_id AS VARCHAR), 4, '0') || '</noOnu>' ||
                   E'<name lg=\'EN\'>' || trim(import_line.value2) || '</name>' ||
                   coalesce(adr_spec.xml, '<adrSpec></adrSpec>') ||
                   coalesce(imdg_spec.xml, '<imdgSpec></imdgSpec>') ||
                   coalesce(iata_spec.xml, '<iataSpec></iataSpec>') ||
                   '</adr>',

    value_id2    = imdg_spec.imp_id,                                            -- IMDG Record
    value_id3    = iata_spec.imp_id,                                            -- IATA Record
    imp_warn     = TRUE,
    imp_warn_msg = coalesce(imp_warn_msg, '') || 'ADR+IMDG(optional)+IATA(optional).'
FROM adr_spec
         LEFT JOIN imdg_spec ON adr_spec.cust_id = imdg_spec.cust_id /*AND
                                trim(upper(adr_spec.value2)) = trim(upper(imdg_spec.value2)) AND
                                coalesce(adr_spec.value33, '') = coalesce(imdg_spec.value3, adr_spec.value3, '') AND
                                coalesce(adr_spec.value5, '') = coalesce(imdg_spec.value5, adr_spec.value5, '')*/

         LEFT JOIN iata_spec ON adr_spec.cust_id = iata_spec.cust_id /*AND
                                trim(upper(translate(adr_spec.value2, E'\n', '_'))) LIKE
                                trim(upper(translate(iata_spec.value2, E'\n', '_'))) AND
                                coalesce(adr_spec.value3, '') = coalesce(iata_spec.value3, adr_spec.value3, '') AND
                                coalesce(adr_spec.value5, '') = coalesce(iata_spec.value5, adr_spec.value5, '')*/
WHERE import_line.imp_id = adr_spec.imp_id
  AND coalesce(imp_err, FALSE) = FALSE
  AND value25 = 'ADR'
  AND NOT coalesce(import_line.imp_err, FALSE)
  and cast(value1 as int) = 1
;
-- 1,967 updated.


-- 2,824 updated.
SELECT count(*)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value_id1 < 1;

/* Update the existing plcsys values. */
UPDATE plcsys
SET sys_key     = value38,
    plcsys_info = value40,
    plcsys_co   = coalesce(plcsys_co, '') || ', updated from import file.'
FROM import.import_line
WHERE value_id1 = plcsys_id
  AND imp_ref = :imp_ref
  AND value25 = 'ADR'
  AND coalesce(value39, value40, '') != ''
  AND value_id1 > 0
--   AND value_id1 IN (1975, 2278, 2271)
;


/* Insert new plcsys records based on ADR and have IMDG(optional) and IATA(optional) */
INSERT INTO plcsys(plcsys_id, name, path, plcsys_ke, tp, sys_key, plcsys_char1, plcsys_char2, plcsys_char3,
                   plcsys_char4, plcsys_char5, plcsys_char6, plcsys_info, plcsys_co)
SELECT abs(value_id1),
       'ADR',
       value39,
       'ADR',
       'ADR',
       value38,
       cust_id,
       value2,
       value3,
       value4,
       value5,
       value6,
       value40,
       ''
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value25 = 'ADR'
  AND NOT coalesce(imp_err, FALSE)
  AND value_id1 < 0
;

/* Exclude IMDG, IATA records that are inserted inside ADR. */
UPDATE import.import_line iata_imdg
SET imp_warn     = TRUE,
    imp_warn_msg = coalesce(iata_imdg.imp_warn_msg, '') ||
                   format('| This %s record is inserted inside ADR with plcsys_id = %s', il.value25,
                          cast(abs(il.value_id1) AS varchar))
FROM import.import_line il
WHERE iata_imdg.imp_ref = :imp_ref
  AND iata_imdg.value25 IN ('IATA', 'IMDG')
  AND NOT coalesce(iata_imdg.imp_err, FALSE)
  AND (iata_imdg.imp_id = il.value_id2
    OR iata_imdg.value_id2 = il.value_id3)
;


/* insert IMDG-IATA records */

/*
SELECT value_id1,
       import_line.cust_id || '-' || value37                       AS plcsys_ke,
       'ADR' || '/' || import_line.cust_id || '-' || trim(value37) AS path,
       coalesce(adr_spec.tp || ',', '') || coalesce(imdg_spec.tp || ',', '') ||
       coalesce(iata_spec.tp, '')                                  AS sys_ke,
       'ADR'                                                       AS name,
       'ADR'                                                       AS tp,
       E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
       E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
       '<src>' || trim(import_line.value37) || '</src>' ||
       '<noOnu>' || lpad(CAST(import_line.cust_id AS VARCHAR), 4, '0') || '</noOnu>' ||
       E'<name lg=\'EN\'>' || trim(import_line.value2) || '</name>' ||
       coalesce(adr_spec.xml, '<adr></adr>') ||
       coalesce(imdg_spec.xml, '<imdg></imdg>') ||
       coalesce(iata_spec.xml, '<iata></iata>') ||
       '</adr>'
                                                                   AS info
FROM import.import_line
         INNER JOIN adr_spec USING (imp_id)
         LEFT JOIN imdg_spec ON import_line.cust_id = imdg_spec.cust_id AND
                                trim(upper(import_line.value2)) = trim(upper(imdg_spec.value2))
         LEFT JOIN iata_spec ON import_line.cust_id = iata_spec.cust_id AND
                                trim(upper(translate(import_line.value2, E'\n', '_'))) LIKE
                                trim(upper(translate(iata_spec.value2, E'\n', '_')))
WHERE coalesce(imp_err, FALSE) = FALSE
  AND value25 = 'ADR'
  AND NOT coalesce(import_line.imp_err, FALSE)
;
*/


--ORDER BY import_line.cust_id;


-- matched with id and description 1821

SELECT value2, translate(value2, E'\n', '_')
FROM import.import_line
WHERE imp_ref = :imp_ref;


-- SELECT nextval('seq_de_id'), currval('seq_de_id')

SELECT substring(cast(plcsys_id AS varchar), 1, 2) || '.' || substring(cast(plcsys_id AS varchar), 3, 4),
       plcsys_id,
       *
FROM plcsys
WHERE tp = 'ADR'
  AND plcsys_id IN (1975, 2278, 2271);


SELECT regexp_replace(E'kdfjbnkd+_-\nldknvs \\', '[[:space:]]', '_', 'g'), E'hhh\nmm';

UPDATE import.import_line
SET imp_err     = NULL,
    imp_err_msg = NULL
WHERE imp_ref = :imp_ref;

SELECT currval('seq_plcsys_id')


SELECT substr('asbscs', length('asbscs') - 3);


SELECT unnest(regexp_match(plcsys_ke, '\d+-(.+)')), plcsys_ke
FROM plcsys


SELECT trim('khaled, ', ', ');

SELECT count(DISTINCT cust_id)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value25 = 'IMDG'



SELECT value37, imp_err, value_id1
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value25 = 'ADR'
  AND value_id1 < 1;



SELECT count(*)
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value25 = 'ADR'
  AND value_id1 > 1;


UPDATE import.import_line
SET value_id1   = NULL,
    value_id2   = NULL,
    value_id3   = NULL,
    value37     = NULL,
    imp_err     = NULL,
    imp_err_msg = NULL
WHERE TRUE;

UPDATE import.import_line
SET value39 = null,
    value38 = NULL
WHERE TRUE;

with duplicate_plcsys as (select plcsys_char1,
                                 plcsys_ke,
                                 plcsys_char2,
                                 plcsys_char3,
                                 plcsys_char4,
                                 plcsys_char5,
                                 plcsys_char6,
                                 count(*) over (partition by plcsys_char1) as cnt
                          from plcsys
                          where tp = 'ADR')
select *
from duplicate_plcsys
where cnt > 2
;