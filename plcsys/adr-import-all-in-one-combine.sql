/*
    * Common Fields:
          UN/ID no. : internal_id  - char1
          Proper Shipping Name/Description : value2 - char2
          Class or Div. (Sub Hazard) : value3 - char3
          Subsidiary hazards(s) -- Labels: value6 - char6
          Packing group : value5 - char5
          Special provisions: value7
          Limited and excepted quantity provisions Ltd Qty: value8
          Limited and excepted quantity provisions Excepted Qty: value9

    * For IMDG:
          Packing Instructions: value10
          Packing Provisions: value11
          IBC Instructions: value12
          IBC Provisions: value13
          Portable tanks and bulk containers Reserved: value14
          Portable tanks and bulk containers Provisions: value15
          Portable tanks and bulk containers Tank Instructions: value16
          EmS : value17
          Stowage and Handling : value18
          Segregation : value19

    * For ADR:
          Packaging Packing instructions: value10
          Packaging Special packing provisions: value11
          Packaging Mixed packing provisions: value12
          Portable tanks and bulk containers Instructions: value13
          Portable tanks and bulk containers Special provisions: value14
          ADR tank Tank code: value15
          ADR tank Special provisions: value16
          Vehicle for tank carriage: value17
          Transport category (Tunnel restriction code): value18
          Special provisions for carriage Packages: value19
          Special provisions for carriage Bulk: value20
          Special provisions for carriage Loading, unloading and handling: value21
          Special provisions for carriage Operation: value22
          Hazard identification No.: value23

    * For IATA:
           UN/ID no.: internal_id
           Proper Shipping Name/Description: value2 - char2
           Class or Div. (Sub Hazard): value3 - char3
           Hazard Labels: value6 - char6
           Packing group: value5 - char5
           Spec. Prov. see 4.4: value7
           Passenger and Cargo Aircraft EQ (see 2.6): value8
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst: value9
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge: value10
           Passenger and Cargo Aircraft Ltd Qty Pkg Inst.1: value11
           Passenger and Cargo Aircraft Ltd Qty Max Net Qty/Pkge.1: value12
           Cargo Aircraft Only Pkg Inst: value13
           Cargo Aircraft Only Max Net Qty/Pkge: value14
           ERG Code: value16

    * Constant values:
        tp: value25 = 'IMDG' or 'ADR' or IATA

    fro my test the imp_ref is adr-import-test
*/

/* check ADR duplications */

WITH get_counts AS (SELECT imp_id,
                           internal_id                                             AS un_no,
                           row_number() OVER (PARTITION BY internal_id)   AS row_number,
                           count(imp_id) OVER (PARTITION BY internal_id) AS cnt
                    FROM import.import_line
                    WHERE imp_ref = :imp_ref
                      AND value25 = 'ADR' -- ADR or IMDG or IATA
)
update import.import_line
    set imp_err = True,
        imp_err_msg = coalesce(imp_err_msg, '') || 'This record is duplicated and will not import this time.'
from get_counts where get_counts.imp_id = import_line.imp_id and cnt > 1;

/* check exists records */
UPDATE import.import_line
SET imp_warn     = TRUE,
    imp_warn_msg = coalesce(imp_err_msg, '') || ' | This record matched one in the DB and will updated.'
FROM plcsys
WHERE plcsys_char1 = lpad(CAST(internal_id AS VARCHAR), 4, '0')
  AND value25 = 'ADR'
  AND coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

/* get the xml format for IMDG */
with imdg_spec as (
    select  internal_id,
            value25 as tp,
            --value2,  --description
            '<imdgSpec>' ||
                 E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
                '<class>' || trim(value3) || '</class>' ||
                '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
                '<limitedQte>' || trim(value8) || '</limitedQte>' ||
                '<labels>' ||
                 string_agg(label, '')||
                '</labels>' ||
                 '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
                '<exceptedQte>' || trim(value9) || '</exceptedQte>' ||
                '<packagingInstructions>' || trim(value10) || '</packagingInstructions>' ||
                '<specialPackagingProvisions>' || trim(value11) || '</specialPackagingProvisions>' ||
                '<ibcInstructions>' || trim(value12) || '</ibcInstructions>' ||
                '<ibcProvisions>' || trim(value13) || '</ibcProvisions>' ||
                '<containersReversed>' || trim(value14) || '</containersReversed>' ||
                '<containersProvisions>' || trim(value15) || '</containersProvisions>' ||
                '<tankInstructions>' || trim(value16) || '</tankInstructions>' ||
                '<ems>' || trim(value17) || '</ems>' ||
                '<stowageAndHandling>' || value18 || '</stowageAndHandling>' ||
                '<segregation>' || value19 || '</segregation>' ||
            '</imdgSpec>' as xml
            from import.import_line
                inner join  (SELECT imp_id,
             '<label>' || translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') || '</label>' AS label
      FROM import.import_line
      WHERE imp_ref = :imp_ref and value25 = 'IMDG') label_agg USING (imp_id)
            WHERE value25 = 'IMDG'
    GROUP BY internal_id, value25, value2, value3, value5, value7, value8, value9,value10,value11,value12,value13,value14,value15,value16,value17, value18, value19
),
/* get the list of IMDG objects. */
     imdg_list AS (SELECT internal_id,tp, string_agg(xml, '') as xml
                   FROM imdg_spec
                   GROUP BY internal_id, tp
                   ORDER BY internal_id
),
/* get the xml format for IATA */
    iata_spec as (
        select internal_id,
               value2, -- description
               value25 as tp,
            '<iataSpec>' ||
                 E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
                '<class>' || trim(value3) || '</class>' ||
                '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
                 '<labels>' || string_agg(label, '') ||
                 '</labels>' ||
                '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
                '<passengerCargoEq>' || value8 || '</passengerCargoEq>' ||
                '<ltdPassengerCargoPackagingInstructions>' || value9 || '</ltdPassengerCargoPackagingInstructions>' ||
                '<ltdPassengerCargoMaxNetQty>' || value10 || '</ltdPassengerCargoMaxNetQty>' ||
                '<passengerCargoPackagingInstructions>' || value11 || '</passengerCargoPackagingInstructions>' ||
                '<passengerCargoMaxNetQty>' || value12 || '</passengerCargoMaxNetQty>' ||
                '<cargoPackagingInstructions>' || value13 || '</cargoPackagingInstructions>' ||
                '<cargoMaxNetQty>' || value14 || '</cargoMaxNetQty>' ||
                '<ergCode>'||  value15 || '</ergCode>' ||
            '</iataSpec>' as xml
            from import.import_line
            inner join  (SELECT imp_id,
             '<label>' || translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') || '</label>' AS label
          FROM import.import_line
          WHERE imp_ref = :imp_ref and value25 = 'IATA') label_agg USING (imp_id)
            where value25 = 'IATA'
        GROUP BY internal_id, value25, value2, value3, value5, value7,value8,value9, value10,value11,value12,value13,value14,value15
        order by internal_id
    ),
/* get the list of IATA objects */
     iata_list AS (SELECT internal_id,tp, string_agg(xml, '') as xml
                   FROM iata_spec
                   GROUP BY internal_id, tp
                   ORDER BY internal_id
),
/* get the xml format for ADR records */
    adr_spec as (
        select  DISTINCT imp_id,
                value2, -- description
                value25 as tp,
                '<adrSpec>' ||
                 E'<name lg=\'EN\'>' || trim(value2) || '</name>' ||
                 '<class>' || trim(value3) || '</class>' ||
                 '<classCode>' || trim(value4) || '</classCode>' ||
                 '<packagingGpe>' || trim(value5) || '</packagingGpe>' ||
                 '<labels>' ||
                     string_agg(label, '') OVER (PARTITION BY internal_id, value2) ||
                 '</labels>' ||
                 '<specialProvisions>' || trim(value7) || '</specialProvisions>' ||
                 '<limitedQte>' || trim(value8) || '</limitedQte>' ||
                 '<exceptedQte>' || trim(value9) || '</exceptedQte>' ||
                 '<packagingInstructions>' || trim(value10) || '</packagingInstructions>' ||
                 '<specialPackagingProvisions>' || trim(value11) || '</specialPackagingProvisions>' ||
                 '<mixedPackagingProvisions>' || trim(value12) || '</mixedPackagingProvisions>' ||
                 '<tankInstructions>' || trim(value13) || '</tankInstructions>' ||
                 '<tankSpecialProvisions>' || trim(value14) || '</tankSpecialProvisions>' ||
                 '<adrTankCode>' || trim(value15) || '</adrTankCode>' ||
                 '<adrTankSpecialProvisions>' || trim(value16) || '</adrTankSpecialProvisions>' ||
                 '<vehiculeForTank>' || trim(value17) || '</vehiculeForTank>' ||
                 '<transportCat>' || trim(value18) || '</transportCat>' ||
                 '<specialProvisionsForPackages>' || trim(value19) || '</specialProvisionsForPackages>' ||
                 '<specialProvisionsForBulk>' || trim(value20) || '</specialProvisionsForBulk>' ||
                 '<specialProvisionsForHandling>' ||trim(value21) || '</specialProvisionsForHandling>' ||
                 '<specialProvisionsForOperation>' ||trim(value22) || '</specialProvisionsForOperation>' ||
                 '<hazardId>' || trim(value23) || '</hazardId>' ||
             '</adrSpec>'  as xml
        from import.import_line
        inner join  (SELECT imp_id,
             '<label>' || translate(unnest(regexp_split_to_array(value6, '[,+]')), E'\n ', '') || '</label>' AS label
        FROM import.import_line
        WHERE imp_ref = :imp_ref and value25 = 'ADR') label_agg USING (imp_id)
        where value25 = 'ADR'
    ),
     combined_result AS
         (SELECT 1, /*nextval('seq_plcsys_id')*/
                 import_line.internal_id,
                 lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '-' || '1.7' AS plcsys_ke,
                 'ADR' || '/' || lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '-' || '1.7' AS path,
                 'ADR'                                                                  AS sys_ke,
                 'ADR'                                                                  AS name,
                 'ADR'                                                                  AS tp,
                 lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0')                 AS char1, -- UN NO.
                 trim(import_line.value2)                                               AS char2, -- Name
                 trim(value3)                                                           AS char3, -- Class
                 trim(value4)                                                           AS char4, -- Classification Code
                 trim(value5)                                                           AS char5, -- Packaging Group
                 trim(value6)                                                           AS char6, -- Labels
                 'import-adr-iata-imdg-test'                                            AS co,

                E'<?xml version=\'1.0\' encoding=\'UTF-8\'?>' ||
                     E'<adr xmlns=\'http://www.ecomundo.eu/adr\' version=\'1.0\' >' ||
                     '<src>' || '3.23' || '</src>' ||
                     '<noOnu>' || lpad(CAST(import_line.internal_id AS VARCHAR), 4, '0') || '</noOnu>' ||
                     E'<name lg=\'EN\'>' || trim(import_line.value2) || '</name>' ||

                    coalesce(adr_spec.xml, '<adrSpec></adrSpec>') ||
                    coalesce(imdg_list.xml, '<imdgSpec></imdgSpec>') ||
                    coalesce(iata_list.xml, '<iataSpec></iataSpec>') ||
                 '</adr>' AS info,
                 imp_id
          FROM import.import_line
                   LEFT JOIN adr_spec USING (imp_id)
                   LEFT JOIN imdg_list USING (internal_id)
                   LEFT JOIN iata_list USING (internal_id)
          WHERE coalesce(imp_err, FALSE) = FALSE
            AND value25 = 'ADR'
            AND coalesce(imp_err, FALSE) = FALSE
          ORDER BY internal_id
    )
SELECT * from combined_result

;
    update plcsys set
           plcsys_char2 = coalesce(nullif(char2, ''), plcsys_char2),
           plcsys_char3 = coalesce(nullif(char3, ''), plcsys_char3),
           plcsys_char4 = coalesce(nullif(char4, ''), plcsys_char4),
           plcsys_char5 = coalesce(nullif(char5, ''), plcsys_char5),
           plcsys_char6 = coalesce(nullif(char6, ''), plcsys_char6),
           plcsys_info =  info,
           plcsys_co = co
    from combined_result where combined_result.char1 = plcsys_char1;
/* combine the records to import into plcsys. */

/* see the duplications */
WITH get_counts AS (SELECT imp_id, internal_id as un_no, value2 as description, value3 as class, value4 as class_code, value5 as packaging_group, value6 as labels, value7 as special_provisions,
                    row_number() over (PARTITION BY internal_id, value2, value3, value4, value5, value7,value8,value9, value10,value11,value12,value13,value14,value15 )as row_number,
                    count(imp_id) over (PARTITION BY internal_id, value2, value3, value4, value5, value7,value8,value9, value10,value11,value12,value13,value14,value15 ) as cnt
                   FROM import.import_line
                   WHERE imp_ref = :imp_ref
                     AND value25 = :tp -- ADR or IMDG or IATA
                  )
delete from import.import_line where imp_id in (select imp_id from get_counts where row_number > 1)

 update  import.import_line set imp_ref = 'adr-import-test' where coalesce(imp_ref, '') in( 'adr-import-test-iata', 'adr-import-test-imdg')

select * from plcsys where plcsys_char1 = '0065'


select * from anx.inci where inciname like '%[%]%'