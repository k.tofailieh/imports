
/*
 * -- Import PIF Translations.
 * upload PIF Translations file into import.import_line 
 * columns: 
 * 	-value1: phractxtke.
 * 	-value2: phractxt.
 * 	-value3: lg (23 European languges).
 * 	-value_id1: phractxtke_id.
 * 	-value_id2: phractxt_id.
 * 
 * imp_ref = 'PIF Translations'.
 * test condition: AND imp_id IN (94793,94794,94797,94798,94397,94398,94399,94400)
 * 
 * */

-- insert new translations:

UPDATE "import".import_line SET value_id2 = -nextval('public.seq_phractxt_id2') WHERE 
imp_ref = 'PIF Translations18-5-2022' AND COALESCE(value_id2,0) = 0;

INSERT INTO phractxt(phractxt_id, txt, lg, author, tech_info)
SELECT abs(value_id2), trim(value2), trim(value3), 'DEEPL','PIF translation with DEEPL'
FROM "import".import_line 
WHERE imp_ref = 'PIF Translations18-5-2022' AND value_id2 < 0;

-- create link: 
INSERT INTO entr_phractxtke_phractxt(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('public.seq_entr_phractxtke_phractxt_id2'), value_id1, abs(value_id2),'TRANSLATION'
FROM "import".import_line 
WHERE imp_ref = 'PIF Translations18-5-2022' AND value_id2 < 0;
------------------------------------------------------------------------------------------------------------------------------

-- update existing translations:
UPDATE phractxt SET txt = value2, tech_info = 'PIF translation with DEEPL'
FROM "import".import_line 
WHERE imp_ref = 'PIF Translations18-5-2022' 
AND value_id2 = phractxt_id   /* corresponded Translations */
AND trim(value2) != trim(txt) /* changed. */
AND value_id2 > 0 /* is not new Translation.*/;
-------------------------------------------------------------------------------------------------------------------------------