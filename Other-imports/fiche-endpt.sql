SELECT *
FROM fiche
WHERE m_id = 44370

INSERT INTO public.entr_fiche_endpt(entr_id, entr_src_id, entr_tp, entr_dst_id, entr_co)
SELECT nextval('seq_entr_fiche_endpt_id'), 44379, 'COSMOS_CONTENT', endpt_id, 'mohammed al taweel import'
FROM endpt
where endpt_id BETWEEN 913421 and 913426
RETURNING entr_id, entr_dst_id;


/*
entr_id, entr_dst_id
---------------------
957688, 913417
957689, 913416
957690, 913415
957691, 913414
957692, 913413
957693, 913412
*/


/*
new entr_fiche_endpt
entr_ID, dst_id
957694,913421
957695,913422
957696,913423
957697,913424
957698,913425
957699,913426
*/

INSERT INTO der_endpt(der_id, der_src_id, der_dst_id, der_tp)
VALUES (nextval('seq_der_endpt_id'), 913421, 3105, 'ENDPT_DESC'),
       (nextval('seq_der_endpt_id'), 913422, 3104, 'ENDPT_DESC'),
       (nextval('seq_der_endpt_id'), 913423, 3103, 'ENDPT_DESC'),
       (nextval('seq_der_endpt_id'), 913424, 3102, 'ENDPT_DESC'),
       (nextval('seq_der_endpt_id'), 913425, 3101, 'ENDPT_DESC'),
       (nextval('seq_der_endpt_id'), 913426, 3100, 'ENDPT_DESC')
RETURNING der_id;


insert into endpt(endpt_id, endpt_tp, val) values
                                               (nextval('seq_endpt_id'), 'COSMETIC_FORMULA_GENERAL_INFORMATION_ENDPT', '1'),
                                               (nextval('seq_endpt_id'),NULL, '2'),
                                               (nextval('seq_endpt_id'), 'COSMETIC_FORMULA_GENERAL_INFORMATION_ENDPT', '3'),
                                               (nextval('seq_endpt_id'), NULL, '4'),
                                               (nextval('seq_endpt_id'), 'COSMETIC_FORMULA_GENERAL_INFORMATION_ENDPT', '5'),
                                               (nextval('seq_endpt_id'), NULL, '6')
RETURNING endpt_id;

delete from der_endpt where der_src_id in (
    913417
 ,913416
 ,913415
 ,913414
 ,913413
 ,913412
    )
/*
new endpt_ids
913421
913422
913423
913424
913425
913426

*/
