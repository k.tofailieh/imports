
update import.import_line set value_id1 = cast(value3 as bigint) where imp_ref = :imp_ref and coalesce(value3, '') != '';

update import.import_line set value_id2 = cast(value4 as bigint) where imp_ref = :imp_ref and coalesce(value4, '') != '';



update import.import_line set imp_err = null, imp_err_msg = null  where imp_ref = :imp_ref;
update import.import_line set imp_warn = null, imp_warn_msg = null  where imp_ref = :imp_ref;


update import.import_line set value5 = null  where imp_ref = :imp_ref;

select count(*) from (select count(*), value_id1, value_id2
                      from import.import_line
                      where imp_ref = :imp_ref
                        and value_id2 < 0 and not coalesce(imp_err, false)
                      group by value_id1, value_id2) ss

