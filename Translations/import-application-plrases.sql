/*
    value_id1: txtke_id,
    value_id2: korean_id,
    value1: korean_txt
    value2: application.
*/

-- Check not exist keys:
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg, '') || 'This Phrase does not exist.'
where imp_ref = :imp_ref
  and not exists(select 1 from txtke where txtke_id = value_id1)
;


-- Check not exist keys:
update import.import_line
set imp_warn     = true,
    imp_warn_msg = coalesce(imp_warn_msg, '') || 'This Translation does not exist.'
where imp_ref = :imp_ref
  and not exists(select 1 from txt where txt_id = value_id2)
  and value_id2 is not null
;


-- Flag records that need to be imported:
with
    selectedPhrases as (select imp_id, value_id2, row_number() over (partition by imp_ref, value_id2) rn
                        from import.import_line
                             inner join txt on txt_id = value_id2 and
                                               not (coalesce(value2, '') = '' /*or trim(value2) = trim(txt.txt)*/) -- txt is changed.
                        where imp_ref = :imp_ref)
update import.import_line
set value5 = 'UPDATE'
from selectedPhrases
where import_line.imp_id = selectedPhrases.imp_id -- matched.
  and not coalesce(imp_warn, false)               -- exist in DB.
  and rn = 1                                      -- master txt.
  and imp_ref = :imp_ref
;


-- Update exists phrases:
update txt
set txt = trim(value1),
    co  = coalesce(co, '') || 'updated after revision.'
from import.import_line
where value_id2 = txt_id
  and value5 = 'UPDATE'             -- txt is different
  and not coalesce(imp_warn, false) -- exist in DB
  and imp_ref = :imp_ref
;


-- Update Id for records that need to be inserted:
with
    selectedTranslations as (select distinct value_id2, trim(value1) as txt, value5
                             from import.import_line
                             where imp_ref = :imp_ref
                               and not coalesce(imp_err, false)                    -- has key
                               and coalesce(value5, '') != 'UPDATE'
                               and (value_id2 is null or coalesce(imp_warn, true)) -- new translation
                               and coalesce(value1, '') != '' -- not a blank
    ),
    newTranslations as (select -nextval('seq_txt_id') as newId, txt from selectedTranslations)
update import.import_line
set value_id2 = newId
from newTranslations
where imp_ref = :imp_ref
  and trim(value1) = trim(txt)
  and not coalesce(imp_err, false)
;

-- Insert new Translation:
insert into txt(txt_id, txt, lg, ns, author, quality, reviewer, co)
select abs(value_id2), value1, 'ko', '*', 'EcoMundo-Korea', '4-EXCELLENT', '*', 'Inserted after revision.'
from import.import_line
where imp_ref = :imp_ref
  and value_id2 < 0
group by value_id2, value1
;

-- link translation with it's key:
insert into entr_txtke_txt(entr_id, entr_tp, entr_src_id, entr_dst_id)
select nextval('seq_entr_txtke_txt_id'), 'TRANSLATION', value_id1, abs(value_id2)
from import.import_line
where imp_ref = :imp_ref
  and not coalesce(imp_err, false)
  and value_id2 < 0
group by value_id1, value_id2
;

------------------------------------------------------------------------------------------------------------------------
-- extract error cases:
select value_id1 as txtke_id, value_id2 koriean_id, value1 koriean_txt, value2 application, imp_err_msg as error_message
from import.import_line
where imp_err = true;

-- extract invalid translation-Id cases:
select imp_id, value_id1 as txtke_id, value_id2 koriean_id, value1 koriean_txt, value2 application, imp_warn_msg as error_message
from import.import_line
where imp_warn = true;

-- extraxt updated cases:
select value_id1 as txtke_id, value_id2 koriean_id, value1 koriean_txt, value2 application
from import.import_line
where value5 = 'UPDATE';

