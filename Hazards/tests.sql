SELECT DISTINCT tp
FROM plc;

SELECT DISTINCT tp
FROM plcsys;

SELECT DISTINCT entr_tp
FROM entr_fiche_plc;

SELECT DISTINCT entr_tp
FROM entr_plc_plcsys;


SELECT *
FROM plcsys
WHERE plcsys.plcsys_char3 ILIKE 'Pyrophoric gas%'


SELECT *
FROM substances;

SELECT cn, email, count(*)
FROM dir
GROUP BY cn, email
HAVING count(*) > 1
;

SELECT DISTINCT st
FROM doc;


SELECT *
FROM dir;


CREATE OR REPLACE VIEW ns_star.doc AS
SELECT *
FROM public.doc
WHERE ((doc.tptp) = 'MOL' OR doc.ns ~~ ANY (SELECT ns_ok.ns_like FROM ns_ok) OR doc.ns IS NULL OR
       doc.doc_id IN (SELECT docr_study.docr_dst_id
                      FROM (public.docr_study JOIN public.study ON (((docr_study.docr_src_id = study.study_id) AND
                                                                     ((study.ns)::text ~~ ANY (SELECT ns_ok.ns_like FROM ns_ok)))))
                      UNION
                      SELECT docr_profile.docr_dst_id
                      FROM (public.docr_profile JOIN public.profile
                            ON (((docr_profile.docr_src_id = profile.profile_id) AND
                                 ((profile.ns)::text ~~ ANY (SELECT ns_ok.ns_like FROM ns_ok)))))))
  AND (tpsfrom, tpstill) OVERLAPS
      (ns_star.getDbTransfertAsOfOverlapsLBound(), ns_star.getDbTransfertAsOfOverlapsUBound());



select value26,ordr, dense_rank()  over(ORDER BY value26) from import.import_line
where imp_ref=:imp_ref
order by ordr asc;