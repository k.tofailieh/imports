SELECT product.m_ref                      AS name,
       product.marguage                   AS code,
       IFRA_COMPO.conc_real_ubound          AS rel_conc,
       coalesce(nullif(IFRA_COMPO.cas, ''), '-') AS cas,
       IFRA_COMPO.naml
FROM fiche product
         LEFT JOIN (SELECT efn.entr_src_id, efn.entr_tp, nom_IFRA.nomclit_id, nom_IFRA.conc_real_ubound, sub.cas, sub.naml
                    FROM entr_fiche_nomclit efn
                             INNER JOIN nomclit nom_RM ON efn.entr_dst_id = nom_RM.nomclit_id
                             INNER JOIN entr_nomclit_fiche enf
                                        ON nom_RM.nomclit_id = enf.entr_src_id AND enf.entr_tp = 'HAS'
                             INNER JOIN fiche rm ON rm.m_id = enf.entr_dst_id AND rm.tp = 'RAW_MATERIAL'
                             INNER JOIN entr_fiche_nomclit efn1 ON efn1.entr_src_id = rm.m_id
                             INNER JOIN nomclit nom_IFRA ON nom_IFRA.nomclit_id = efn1.entr_dst_id
                             INNER JOIN entr_nomclit_fiche enf1
                                        ON enf1.entr_src_id = nom_IFRA.nomclit_id AND enf1.entr_tp = 'HAS'
                             INNER JOIN fiche ifra ON ifra.m_id = enf1.entr_dst_id AND ifra.tp = 'PERFUME_COMPONENT'
                             INNER JOIN entr_fiche_substances efs
                                        ON ifra.m_id = efs.entr_src_id AND efs.entr_tp = 'PERFUME_COMPONENT'
                             INNER JOIN substances sub ON sub.sub_id = efs.entr_dst_id) IFRA_COMPO
                   ON product.m_id = IFRA_COMPO.entr_src_id AND
                      IFRA_COMPO.entr_tp IN ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
WHERE split_part(product.ns, '/', 1) = :user_organisation
  AND product.m_id = CAST(:MId AS bigint);


select * from fiche where m_id = 572804;