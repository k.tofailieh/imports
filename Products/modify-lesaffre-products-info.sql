/*
This script is to modify some products info for "".
* Upload the file into import line with this specifications:
  Fields:
      -Nom du produit: value1
      -Existing Code: value2
      -New Code (from SAP): value3
      -Existing External Id: value4(not mandatory )
      -New External Id (LIP): value5(not mandatory )
      -Existing Product type: value6
      -New Product type: value7
*/

/* change ns from Lesaffre to Lessafrelis */
SELECT rename_ns('Lesaffre', 'LesaffreLis');

UPDATE import.import_line
SET value3 = regexp_replace(value3, '^-+$', ''),
    value2 = regexp_replace(value2, '^-+$', '')
WHERE imp_ref = :imp_ref;


UPDATE import.import_line
SET internal_id = m_id
FROM fiche
WHERE upper(trim(value1)) = upper(trim(m_ref))
  --AND coalesce(trim(upper(value2)), '') = coalesce(trim(upper(marguage)), '')
  --AND trim(upper(value6)) = trim(upper(tp))
  AND split_part(ns, '/', 1) = 'LesaffreLis'
  AND active = 1
  AND imp_ref = :imp_ref;

/* Warning for the records that are not matched*/
UPDATE import.import_line
SET imp_warn     = TRUE,
    imp_warn_msg = coalesce(imp_warn_msg, '') || '| This product is not matched any product in DB'
WHERE internal_id IS NULL
  AND imp_ref = :imp_ref;

/* Flag the records that will be updated.(that have a new code or a new external_id) */
UPDATE import.import_line
SET value10 = 'INFO_UPDATED'
FROM fiche
WHERE m_id = internal_id
  AND (trim(upper(marguage)) != trim(upper(value3)) OR trim(value5) != trim(fiche.cust_id))
  AND imp_ref = :imp_ref
  AND coalesce(imp_warn, FALSE) = FALSE;

/* Modify products information. */
UPDATE fiche
SET marguage = coalesce(nullif(trim(value2), ''), marguage),
    cust_id  = coalesce(nullif(trim(value3), ''), fiche.cust_id)
FROM import.import_line
WHERE m_id = internal_id
  AND value10 = 'INFO_UPDATED'
  AND imp_ref = :imp_ref
;

/* Upadte tp. */
/* products that we can change the tp for them */
WITH mat_to_com_substance AS (SELECT imp_id,
                                     internal_id
                              FROM import.import_line
                              WHERE internal_id IS NOT NULL
                                AND coalesce(imp_warn, FALSE) = FALSE
                                AND trim(upper(value6)) = 'MAT'
                                AND trim(upper(translate(value7, ' ', '_'))) = upper(trim('Commercial_Substance'))
                                AND imp_ref = :imp_ref),

     able_to_update_products AS (SELECT imp_id,
                                        internal_id
                                 FROM import.import_line
                                 WHERE internal_id IS NOT NULL
                                   AND coalesce(imp_warn, FALSE) = FALSE
                                   AND (trim(upper(value6)) = 'COMMERCIAL_SUBSTANCE' AND
                                        trim(upper(value7)) = 'COMMERCIAL SUBSTANCE')
                                   AND imp_ref = :imp_ref),
    /* get count of RMs and substances that related to each product. */
     filtered AS (SELECT imp_id,
                         internal_id,
                         nbRMs,
                         nbSUBs
                  FROM mat_to_com_substance
                           LEFT JOIN (SELECT efn.entr_src_id    AS internal_id,
                                             count(enf.entr_id) AS nbRMs
                                      FROM entr_fiche_nomclit efn
                                               INNER JOIN entr_nomclit_fiche enf ON efn.entr_dst_id = enf.entr_src_id
                                      GROUP BY efn.entr_src_id) RMs
                                     USING (internal_id)
                           LEFT JOIN (SELECT efn.entr_src_id    AS internal_id,
                                             count(ens.entr_id) AS nbSUBs
                                      FROM entr_fiche_nomclit efn
                                               INNER JOIN entr_nomclit_substances ens ON efn.entr_dst_id = ens.entr_src_id
                                      GROUP BY efn.entr_src_id) SUBs USING (internal_id)),
    /* get the products that has only substances in their composition(MAT --> COMMERCIAL SUBSTANCE) */
     selected AS (SELECT imp_id,
                         internal_id         AS product_id,
                         coalesce(nbRMs, 0)  AS nbRMs,
                         coalesce(nbSUBs, 0) AS nbSUBs
                  FROM filtered
                  WHERE coalesce(nbRMs, 0) = 0
                  UNION
                  SELECT imp_id,
                         internal_id AS product_id,
                         0           AS nbRMs,
                         0           AS nbSubs
                  FROM able_to_update_products)
/* Flag the products that able to update tp. */
UPDATE import.import_line
SET value11 = 'TP_UPDATED'
FROM selected
WHERE selected.imp_id = import_line.imp_id
;

/* update tp in DB */
UPDATE fiche
SET tp = trim(upper(value7))
FROM import.import_line
WHERE coalesce(imp_warn, FALSE) = FALSE
  AND value11 = 'TP_UPDATED'
  AND internal_id = m_id
  AND imp_ref = :imp_ref;

/* reindex one of two client accounts and delete it. */
SELECT reindex_delete_entity('dir', 2112357, 2115575);


/* Reporting */
------------------------------------------------------------------------------------------------------------------------
/* extract matched and modified products */
-- SELECT value2, value3
-- FROM import.import_line
--          INNER JOIN fiche ON internal_id = m_id
-- WHERE imp_ref = :imp_ref
--   AND coalesce(value2, '') != ''
--   AND coalesce(value3, '') != ''
--   AND coalesce(value2, '') != coalesce(value3, '');

/* extract modified products with existing type and new type. */
SELECT internal_id AS porduct_id,
       m_ref       AS product_name_db,
       value1      AS product_name_file,
       value3      AS new_code_file,
       marguage    AS new_code_db,
       value5      AS new_external_id_file,
       f.cust_id   AS new_external_id_db,
       value6      AS existing_tp,
       value7      AS new_tp
FROM import.import_line
         INNER JOIN fiche f ON import_line.internal_id = f.m_id
WHERE internal_id IS NOT NULL
  AND coalesce(imp_warn, FALSE) = FALSE
  AND imp_ref = :imp_ref;


/* extract not matched products */
SELECT value1       AS product_name,
       value2       AS product_code,
       value4       AS external_code,
       value6       AS product_tp,
       imp_warn_msg AS warning_message
FROM import.import_line
WHERE imp_warn = TRUE
  AND imp_ref = :imp_ref;


/* products that we cannot change the tp for them */
WITH not_able_to_change_products AS (SELECT *
                                     FROM import.import_line
                                     WHERE internal_id IS NOT NULL
                                       AND (trim(upper(value6)) IN ('MAT', 'COMMERCIAL_SUBSTANCE') AND
                                            trim(upper(value7)) = 'RAW MATERIAL')
                                       AND imp_ref = :imp_ref)
SELECT internal_id AS product_id,
       value1      AS product_name,
       value2      AS product_code,
       value3      AS new_product_code,
       value6      AS current_tp,
       value7      AS new_tp
FROM not_able_to_change_products;

/* False Product Tp */
WITH false_prod_tp AS
         (SELECT *
          FROM import.import_line
          WHERE internal_id IS NOT NULL
            AND trim(upper(value6)) = 'SUBSTANCE'
            AND imp_ref = :imp_ref)
SELECT internal_id AS product_id,
       value1      AS product_name,
       value2      AS product_code,
       value3      AS new_product_code,
       value6      AS current_tp,
       value7      AS new_tp
FROM false_prod_tp;

SELECT *
FROM fiche
         INNER JOIN entr_fiche_nomclit ON fiche.m_id = entr_fiche_nomclit.entr_src_id
         INNER JOIN entr_nomclit_substances ens ON entr_fiche_nomclit.entr_dst_id = ens.entr_src_id
WHERE split_part(ns, '/', 1) = 'LesaffreLis'
  AND tp = 'MAT';


/* ignore updating tp */
SELECT internal_id AS product_id,
       value1      AS product_name,
       value2      AS product_code,
       value3      AS new_product_code,
       value6      AS current_tp,
       value7      AS new_tp
FROM import.import_line
WHERE trim(upper(value6)) = 'MAT'
  AND trim(upper(value7)) = 'PRODUCT'
  AND internal_id IS NOT NULL;


SELECT *
FROM import.import_line
WHERE value1 LIKE 'Water';


WITH dupliacates AS (SELECT value1,
                            internal_id,
                            value2                                                       AS existing_code,
                            value3                                                       AS new_code,
                            value4                                                       AS existing_cust_id,
                            value5                                                       AS new_cust_id,
                            value6                                                       AS existing_tp,
                            value7                                                       AS new_tp,
                            count(imp_id) OVER (PARTITION BY trim(value1), trim(value6)) AS cnt
                     FROM import.import_line
                     WHERE imp_ref = :imp_ref)
SELECT *
FROM dupliacates
WHERE cnt > 1;


WITH not_matched AS (SELECT value1,
                            internal_id,
                            value2                                                       AS existing_code,
                            value3                                                       AS new_code,
                            value4                                                       AS existing_cust_id,
                            value5                                                       AS new_cust_id,
                            value6                                                       AS existing_tp,
                            value7                                                       AS new_tp,
                            count(imp_id) OVER (PARTITION BY trim(value1), trim(value6)) AS cnt
                     FROM import.import_line
                     WHERE imp_ref = :imp_ref)
SELECT *
FROM dupliacates
WHERE cnt > 1;



SELECT m_id,
       coalesce(nullif(trim(value3), ''), marguage),
       coalesce(nullif(trim(value5), ''), fiche.cust_id),
       marguage,
       value5,
       fiche.cust_id
FROM import.import_line
         INNER JOIN fiche ON m_id = internal_id
    AND value10 = 'INFO_UPDATED'
    AND imp_ref = :imp_ref
    AND value1 LIKE 'Water'
;

SELECT *
FROM fiche
WHERE split_part(ns, '/', 1) = 'LesaffreLis'

SELECT value2, regexp_replace(value2, '^-+$', ''), *
FROM import.import_line
WHERE imp_ref = :imp_ref --and imp_id = 1238670;

SELECT *
FROM import.import_line
ORDER BY imp_id DESC;

SELECT *
FROM dir
WHERE dir_id = 2136243;

SELECT *
FROM society
WHERE society_id = 74159, 74092;

SELECT *
FROM entr_society_dir
WHERE entr_src_id = 74159;


SELECT *
FROM fiche
WHERE m_ref = 'LIF SUP 505 L'

SELECT *
FROM entr_fiche_dir
WHERE entr_src_id = 591276

SELECT m_ref, marguage, cust_id
FROM fiche;

UPDATE import.import_line
SET value40 = 'override'
WHERE imp_ref = :imp_ref;

SELECT *
FROM entr_fiche_dir
WHERE entr_id = 1207984

SELECT *
FROM import.import_line
WHERE imp_ref = :imp_ref;


SELECT *
FROM fiche
WHERE marguage IN
      ('BP 001', 'COL 001', 'COL 002', 'COL 003', 'COL 004', 'COL 005', 'COL 007', 'COL 008', 'COL 009', 'COL 011',
       'COL 012', 'COL 014', 'COL 015', 'COL 016', 'COL 0017', 'COL 019', 'COL 0021', 'COL 022', 'COL 023', 'COL 024',
       'COL 0024', 'COL 0030', 'COL 030', 'COL 030', 'COL 030', 'COL 030', 'COL 030', 'COL 031')

SELECT *
FROM fiche
WHERE marguage = 'COL 005'

SELECT ns, *
FROM fiche
WHERE cust_id IN ('LIP-M-0354', 'LIP-M-0730', 'LIP-M-00355', 'LIP-M-00135', 'LIP-M-00356', 'LIP-M-00133', 'LIP-M-00357',
                  'LIP-M-00358', 'LIP-M-00731', 'LIP-M-00359', 'LIP-M-00360', 'LIP-M-00732', 'LIP-M-00361',
                  'LIP-M-00136', 'LIP-M-00733', 'LIP-M-00362', 'LIP-M-00363', 'LIP-M-00364', 'LIP-M-00734',
                  'LIP-M-00365', 'LIP-M-00365', 'LIP-M-00366', 'LIP-M-00366', 'LIP-M-00366', 'LIP-M-00366',
                  'LIP-M-00366', 'LIP-M-00366', 'LIP-M-00367', 'LIP-M-00368', 'LIP-M-00369', 'LIP-M-00370',
                  'LIP-M-00371', 'LIP-M-00735', 'LIP-M-00736', 'LIP-M-00737', 'LIP-M-00738', 'LIP-M-00739',
                  'LIP-M-00740', 'LIP-M-00041', 'LIP-M-00741', 'LIP-M-00137', 'LIP-M-00372', 'LIP-M-00742',
                  'LIP-M-00044', 'LIP-M-00373', 'LIP-M-00374', 'LIP-M-00743');


/*
 Normalization: to keep the numeric term of cust_id.
 EX: (1.0000 ---> 1).
*/
UPDATE import.import_line
SET cust_id = regexp_replace(cust_id, '^(\d+)(\.?0*)$', '\1')
WHERE imp_ref = :imp_ref
  AND cust_id SIMILAR TO '\d+\.?0*'
;

INSERT INTO import.import_line(imp_ref, cust_id)
VALUES ('normalization-test', '1.001'),
       ('normalization-test', '1.000'),
       ('normalization-test', '1.101'),
       ('normalization-test', '1.001'),
       ('normalization-test', '1.0');


select cust_id from import.import_line where imp_ref = :imp_ref;