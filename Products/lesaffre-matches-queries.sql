WITH matches AS (SELECT imp_id,
                        regexp_matches(value3, '\d+', '')    AS numbers,
                        regexp_matches(value3, '\w+\s*', '') AS chars
                 FROM import.import_line
                 WHERE imp_ref = :imp_ref),
     code_concat AS (SELECT imp_id,
                            chars[1] || (CASE
                                             WHEN substr(numbers[1], 1, 1) = '0' THEN substr(numbers[1], 2)
                                             ELSE numbers[1] END) value3_new
                     FROM matches)
UPDATE import.import_line
SET value4 = trim(value3_new)
FROM code_concat
WHERE import_line.imp_ref = :imp_ref
  AND import_line.imp_id = code_concat.imp_id
;