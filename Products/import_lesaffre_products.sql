/*
This script is to import new products for "".

Fields:
    Nom du produit: value1
    Existing Code: value2
    New Code (from SAP): value3
    Existing External Id: value4
    New External Id (LIP): value5
    Existing Product type: value6
    New Product type: value7


*/

/* Check Exists products */
UPDATE import.import_line
SET internal_id = m_id,
    new_id      = - nextval('seq_fiche_id')
FROM fiche
WHERE trim(upper(value1)) = trim(upper(m_ref))
--   AND coalesce(trim(upper(value2)), '') = coalesce(trim(upper(marguage)), '')
 -- AND trim(upper(value6)) = trim(upper(tp))
--   AND split_part(ns, '/', 1) = 'Lesaffre'
  AND imp_ref = :imp_ref;

/* Raise an error for the records that are not matched*/
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = imp_err_msg || '| This product is not matched any product in DB'
WHERE coalesce(internal_id, 0) = 0
  AND imp_ref = :imp_ref;

/* Copy the matched products with new_ids and apply the updates for them. */
INSERT INTO fiche(m_id, reference, marguage, pci, postproduct, postconsumer, active, fiche_nt, xml, cust_id,
                  typical_use, m_ref, m_attribute, densite, sds_code, reach_st, sds_up_dt, sds_req, adr_numero_onu,
                  adr_designation, adr_classe, adr_code_classification, adr_groupe_emballage, adr_etiquette,
                  adr_danger_num, sds_usecase_cmt, typicaluse_eq_sdsuse, manufacturer, manufacturer_ref, co1, co2, co3,
                  first_vers_m_id, ns, ec, cas, idx, registration_num, tps_md_ns, tps_md_app, acl_select, acl_options, ean_code, tp)

SELECT abs(new_id),
       f.reference,
       value3 /* update product code "marguage"*/,
       f.pci,
       f.postproduct,
       f.postconsumer,
       f.active,
       f.fiche_nt,
       f.xml,
       value5 /* update cust_id */,
       f.typical_use,
       f.m_ref,
       f.m_attribute,
       f.densite,
       f.sds_code,
       f.reach_st,
       f.sds_up_dt,
       f.sds_req,
       f.adr_numero_onu,
       f.adr_designation,
       f.adr_classe,
       f.adr_code_classification,
       f.adr_groupe_emballage,
       f.adr_etiquette,
       f.adr_danger_num,
       f.sds_usecase_cmt,
       f.typicaluse_eq_sdsuse,
       f.manufacturer,
       f.manufacturer_ref,
       f.co1,
       f.co2,
       f.co3,
       f.first_vers_m_id,
       trim(:new_ns) /* update ns */,
       f.ec,
       f.cas,
       f.idx,
       f.registration_num,
       f.tps_md_ns,
       f.tps_md_app,
       f.acl_select,
       f.acl_options,
       f.ean_code,
       trim(upper(value7)) /* update tp */
FROM import.import_line
         INNER JOIN fiche f ON internal_id = m_id
    AND coalesce(imp_err, FALSE) = FALSE
    AND imp_ref = :imp_ref;


/* Copy the Relations of Products */

/* entr_fiche_dir */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_dir', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_dir efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_dir(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_dir_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_dir'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_dir efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_substances */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_substances', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_substances efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_substances(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_substances_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_substances'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_substances efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_study */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_study', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_study efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_study(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_study_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_study'
    AND NOT exists(SELECT 1
                 FROM entr_fiche_study efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_profile */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_profile', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_profile efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_profile(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_profile_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_profile'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_profile efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_lov */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_lov', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_lov efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_lov(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_lov_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_lov'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_lov efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));


/* entr_fiche_plc */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_plc', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_plc efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_plc(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_plc_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_plc'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_plc efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));


/* entr_fiche_process */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_process', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_process efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_process(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_process_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_process'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_process efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));


/* entr_fiche_fiche (src) */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_fiche_src', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_fiche efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_fiche_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_fiche_src'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_app */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_app', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_app efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_app(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_app_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_app'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_app efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_endpt */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_endpt', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_endpt efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_endpt(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_endpt_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_endpt'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_endpt efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_reg */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_reg', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_reg efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_reg(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_reg_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_reg'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_reg efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_sdsdoc */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_sdsdoc', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_sdsdoc efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_sdsdoc(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_sdsdoc_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_sdsdoc'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_sdsdoc efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_wfprocess */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_wfprocess', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_wfprocess efd ON efd.entr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_wfprocess(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_wfprocess_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_wfprocess'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_wfprocess efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id));

/* entr_fiche_nomclit */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_nomclit', efd.entr_tp, abs(il.new_id), efd.entr_dst_id
FROM import.import_line il
         INNER JOIN entr_fiche_nomclit efd ON efd.entr_src_id = internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_nomclit(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_nomclit_id'), internal_id, entr_dst_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_nomclit'
  AND NOT exists(SELECT 1
                 FROM entr_fiche_nomclit efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (entr_src_id, efd.entr_tp, efd.entr_dst_id))
;


/* entr_fiche_fiche (dst)*/
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_fiche_fiche_dst', efd.entr_tp, il.internal_id, efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_fiche_fiche efd ON efd.entr_dst_id = internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_fiche_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_fiche_dir_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_fiche_fiche_dst'
AND NOT exists(SELECT 1
                 FROM entr_fiche_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;

/* entr_part_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_part_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_part_fiche efd ON efd.entr_dst_id = internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO entr_part_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_part_fiche_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_part_fiche'
AND NOT exists(SELECT 1
                 FROM entr_fiche_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;

/* entr_presence_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_presence_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_presence_fiche efd ON efd.entr_dst_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref
;

INSERT INTO entr_presence_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_presence_fiche_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_presence_fiche'
AND NOT exists(SELECT 1
                 FROM entr_presence_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;


/* entr_profile_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_profile_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_profile_fiche efd ON efd.entr_dst_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref
;

INSERT INTO entr_profile_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_profile_fiche_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_profile_fiche'
AND NOT exists(SELECT 1
                 FROM entr_profile_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;


/* entr_ns_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_ns_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_ns_fiche efd ON efd.entr_dst_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref
;

INSERT INTO entr_ns_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_ns_fiche_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_ns_fiche'
AND NOT exists(SELECT 1
                 FROM entr_ns_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;


/* entr_project_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_project_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_project_fiche efd ON efd.entr_dst_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref
;

INSERT INTO entr_project_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_project_fiche_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_project_fiche'
AND NOT exists(SELECT 1
                 FROM entr_ns_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;

/* entr_feedback_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_feedback_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_feedback_fiche efd ON efd.entr_dst_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref
;

INSERT INTO entr_feedback_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_feedback_fiche_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_feedback_fiche'
AND NOT exists(SELECT 1
                 FROM entr_feedback_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;

/* entr_subscription_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_subscription_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_subscription_fiche efd ON efd.entr_dst_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref
;

INSERT INTO entr_subscription_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_subscription_fiche_id'), entr_dst_id, internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_feedback_fiche'
AND NOT exists(SELECT 1
                 FROM entr_subscription_fiche efd
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;

/* der_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id, entr_char1, entr_char2)
SELECT imp_id, 'der_fiche', df.der_tp, abs(il.new_id), df.der_dst_id, der_char1, der_char2
FROM import.import_line il
         INNER JOIN der_fiche df ON df.der_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO der_fiche(der_id, der_src_id, der_dst_id, der_tp, der_char1, der_char2)
SELECT nextval('seq_entr_fiche_dir_id'), internal_id, entr_dst_id, entr_tp, entr_char1, entr_char2
FROM import.import_matching im
WHERE entr_table = 'der_fiche'
AND NOT exists(SELECT 1
                 FROM der_fiche df
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id, im.entr_char1, im.entr_char2) = (df.der_src_id, df.der_tp, df.der_dst_id, df.der_char1, df.der_char2))
;


/* extr_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id, entr_char1, entr_char2)
SELECT imp_id, 'extr_fiche', ef.extr_tp, abs(il.new_id), ef.extr_dst_id, extr_char1, extr_char2
FROM import.import_line il
         INNER JOIN extr_fiche ef ON ef.extr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO extr_fiche(extr_id, extr_src_id, extr_dst_id, extr_tp, extr_char1, extr_char2)
SELECT nextval('seq_extr_fiche_id'), internal_id, entr_dst_id, entr_tp, entr_char1, entr_char2
FROM import.import_matching im
WHERE entr_table = 'extr_fiche'
AND NOT exists(SELECT 1
                 FROM extr_fiche ef
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id, im.entr_char1, im.entr_char2) = (ef.extr_src_id, ef.extr_tp, ef.extr_dst_id, ef.extr_char1, ef.extr_char2))
;

/* docr_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id, entr_char1, entr_char2)
SELECT imp_id, 'docr_fiche', dof.docr_tp, abs(il.new_id), dof.docr_dst_id, dof.docr_char1, dof.docr_char2
FROM import.import_line il
         INNER JOIN docr_fiche dof ON dof.docr_src_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

INSERT INTO docr_fiche(docr_id, docr_src_id, docr_dst_id, docr_tp, docr_char1, docr_char2)
SELECT nextval('seq_docr_fiche_id'), internal_id, entr_dst_id, entr_tp, entr_char1, entr_char2
FROM import.import_matching im
WHERE entr_table = 'docr_fiche'
AND NOT exists(SELECT 1
                 FROM docr_fiche dof
                 WHERE (internal_id, im.entr_tp, im.entr_dst_id, im.entr_char1, im.entr_char2) = (dof.docr_src_id, dof.docr_tp, dof.docr_dst_id, dof.docr_char1, dof.docr_char2))
;
