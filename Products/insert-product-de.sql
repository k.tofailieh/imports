INSERT INTO de (de_id, de_tp, de_li, de_ke, de_st)
VALUES (nextval('seq_de_id'), 'COSMETIC_PRODUCT_DOC_TYPE', 'MANUFACTURING_PROCESS', 'MANUFACTURING_PROCESS', 'VALID'),
       (nextval('seq_de_id'), 'COSMETIC_PRODUCT_DOC_TYPE', 'FORMULA', 'FORMULA', 'VALID'),
       (nextval('seq_de_id'), 'COSMETIC_PRODUCT_DOC_TYPE', 'BATCH_NUMBERING_PROCEDURE', 'BATCH_NUMBERING_PROCEDURE',
        'VALID'),
       (nextval('seq_de_id'), 'COSMETIC_PRODUCT_DOC_TYPE', 'FSE', 'FSE', 'VALID'),
       (nextval('seq_de_id'), 'COSMETIC_PRODUCT_DOC_TYPE', 'IFRA_CERTIFICATE', 'IFRA_CERTIFICATE', 'VALID');


SELECT *
FROM fiche pc
         INNER JOIN entr_fiche_nomclit efn
                    ON pc.m_id = efn.entr_src_id -- and efn.entr_tp = 'DIRECT_COMPO_FICHE_PERFUME_CONCENTRATE'
         INNER JOIN entr_nomclit_fiche enf ON enf.entr_src_id = efn.entr_dst_id AND
                                              enf.entr_tp = 'NOMENCLATURE_ITEM_FOR_FICHE_PERFUME_CONCENTRATE'
         INNER JOIN fiche RM ON RM.m_id = enf.entr_dst_id AND RM.tp = 'RAW_MATERIAL'

WHERE pc.tp = 'PERFUME_CONCENTRATE';


SELECT fi.*, se.*
FROM entr_fiche_fiche
         INNER JOIN fiche fi ON entr_fiche_fiche.entr_dst_id = fi.m_id AND fi.tp = 'PERFUME_CONCENTRATE'
         INNER JOIN fiche se ON se.m_id = entr_fiche_fiche.entr_src_id AND se.tp = 'RAW_MATERIAL';

