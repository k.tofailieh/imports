/*
    put this columns in import line:
    value1 = A
    value2 = B
    value3 = C

    -- internal_id will updated to be product_id.
    -- value4 will updated to be normalized product code.
 */

------------------------------------------------------------------------------------------------------------------------
/*
  Change ns from Lesaffre to Lessafrelis.
  -Uncomment if needed.
*/
--SELECT rename_ns('Lesaffre', 'LesaffreLis');
--SELECT reindex_delete_entity('dir', 2112357, 2115575);


/* Normalization: set value1,value2,value3 as '' when it's a '-' */
UPDATE import.import_line
SET value1 = regexp_replace(value1, '^-+$', ''),
    value2 = regexp_replace(value2, '^-+$', ''),
    value3 = regexp_replace(value3, '^-+$', '')
WHERE imp_ref = :imp_ref
;

/*
 Normalization: to keep the numeric term of cust_id.
 EX: (1.0000 ---> 1).
*/
UPDATE import.import_line
SET cust_id = regexp_replace(cust_id, '^(\d+)(\.?\d*)$', '\1')
WHERE imp_ref = :imp_ref
  AND cust_id SIMILAR TO '\d+\.?\d*'
;

/*
 * Normalize the code to be like current code in DB
 * EX: (COL 0001 --> COL 001)
 * Normalized code will be used as matching-key.
  It's not used now because the matching criteria is changed to cell 'B' and cust_id.
*/
/*
WITH matches AS (SELECT imp_id,
                        regexp_matches(value3, '\d+', '')    AS numbers,
                        regexp_matches(value3, '\w+\s*', '') AS chars
                 FROM import.import_line
                 WHERE imp_ref = :imp_ref),
     code_concat AS (SELECT imp_id,
                            chars[1] || (CASE
                                             WHEN substr(numbers[1], 1, 1) = '0' THEN substr(numbers[1], 2)
                                             ELSE numbers[1] END) value3_new
                     FROM matches)
UPDATE import.import_line
SET value4 = trim(value3_new)
FROM code_concat
WHERE import_line.imp_ref = :imp_ref
  AND import_line.imp_id = code_concat.imp_id
;
*/

/* Get product_ids, Based on cust_id . */
UPDATE import.import_line
SET internal_id = m_id
FROM fiche
WHERE
   trim(upper(value2)) = trim(upper(fiche.cust_id))
  AND split_part(ns, '/', 1) = 'LesaffreLis'
  AND active = 1
  AND imp_ref = :imp_ref
;

/* Error for mismatched products.  */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') ||
                  format('The product with cust_id "%s" and code %s is mismatched.', value2, value3)
WHERE internal_id IS NULL
  AND imp_ref = :imp_ref
;

------------------------------------------------------------------------------------------------------------------------
/* Upadte matched products give them the new_values. */

UPDATE fiche
SET marguage = coalesce(nullif(value3, ''), marguage),
    cust_id  = coalesce(nullif(value1, ''), fiche.cust_id),
    co3      = coalesce(co3, '') || '| update marguage, cust_id from import file.'
FROM import.import_line
WHERE m_id = internal_id
  AND NOT coalesce(imp_err, FALSE)
  AND imp_ref = :imp_ref
;
------------------------------------------------------------------------------------------------------------------------
/* Extract mismatched products */
SELECT value1 AS new_external_id, value2 exist_external_id, value3 new_code
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND imp_err = TRUE;

/* Extract updated Products. (with right Action.) */
SELECT value1, value2, value3, case when internal_id is not null then 'Modify' else 'Create' end as action
FROM import.import_line
WHERE --internal_id IS NOT NULL
  imp_ref = :imp_ref
ORDER BY value1;