/*
In this script we will copy the nomclits that related with original products and link them with the new (copy) products
for import_matching:
    internal_id contains the new(copy) product ids
    entr_dst_id contains the ids of the nomclits that related with the old(original) products.
    entr_num1 will contains the new(copy) nomclit ids.

*/

/* entr_nomclit_fiche */
INSERT INTO import.import_matching(imp_id, entr_table, entr_tp, internal_id, entr_dst_id)
SELECT imp_id, 'entr_nomclit_fiche', efd.entr_tp, abs(il.new_id), efd.entr_src_id
FROM import.import_line il
         INNER JOIN entr_nomclit_fiche efd ON efd.entr_dst_id = il.internal_id
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

UPDATE import.import_line
SET value_id1 = -nextval('seq_nomclit_id')
WHERE entr_table = 'entr_nomclit_fiche'
  AND coalesce(imp_err, FALSE) = FALSE;

INSERT INTO nomclit(nomclit_id, alt_set, alt_name, alt_st, weight_cal, weight_cal_unit, weight_mes, weight_mes_unit,
                    qte, qte_unit, qte_nt, nb, item_tp, co, cmt, nt, ke, info, tech_info, tech_code, st, cr_dt, md_dt,
                    ns, entity_tp, json_str, conc_direct_lbound, conc_direct_ubound, conc_direct_lbound_inc,
                    conc_direct_ubound_inc, conc_real_lbound, conc_real_ubound, conc_real_lbound_inc,
                    conc_real_ubound_inc, conc_unit, tpsuid, tpsfrom, tpstill, tps_md_ns, tps_md_app, batch_number)
SELECT abs(entr_num1),
       alt_set,
       alt_name,
       alt_st,
       weight_cal,
       weight_cal_unit,
       weight_mes,
       weight_mes_unit,
       qte,
       qte_unit,
       qte_nt,
       nb,
       item_tp,
       co,
       cmt,
       nt,
       ke,
       info,
       tech_info,
       tech_code,
       st,
       cr_dt,
       md_dt,
       ns,
       entity_tp,
       json_str,
       conc_direct_lbound,
       conc_direct_ubound,
       conc_direct_lbound_inc,
       conc_direct_ubound_inc,
       conc_real_lbound,
       conc_real_ubound,
       conc_real_lbound_inc,
       conc_real_ubound_inc,
       conc_unit,
       tpsuid,
       tpsfrom,
       tpstill,
       tps_md_ns,
       tps_md_app,
       batch_number
FROM import.import_matching
    inner join nomclit on entr_dst_id = nomclit_id
where entr_table = 'entr_nomclit_fiche';

/* link the new(copy) nomclits with the new(copy) products */
INSERT INTO entr_nomclit_fiche(entr_id, entr_src_id, entr_dst_id, entr_tp)
SELECT nextval('seq_entr_nomclit_fiche_id'), abs(entr_num1), internal_id, entr_tp
FROM import.import_matching im
WHERE entr_table = 'entr_nomclit_fiche'
  AND NOT exists(SELECT 1
                 FROM entr_nomclit_fiche efd
                 WHERE (internal_id, im.entr_tp, abs(im.entr_num1)) = (efd.entr_dst_id, efd.entr_tp, efd.entr_src_id))
;


delete from nomclit where nomclit_id in (select entr_num1 from import.import_matching where entr_table = 'entr_nomclit_fiche')

select * from import.import_matching where entr_table = 'entr_nomclit_fiche'



select * from fiche where m_id = 514699 or m_id = 515720 or m_id = 515721

select * from fiche where m_id in (515721, 515720, 514699 )



