/* CHECK */

------ Check if at least one of the product idenfiers is filled ------
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg || ' | ', '') || 'None of the product identifiers is filled'
WHERE imp_ref = :imp_ref
  AND coalesce(value_id1, 0) = 0
  AND coalesce(value_txt1, value1, value2, '') = '';

--- show error if more than one product correspond at one combination customId and marguage ---
WITH duplicate_cust_id AS (SELECT f.cust_id
                           FROM fiche f
                           WHERE (f.cust_id, marguage) IN
                                 (SELECT DISTINCT value_txt1, value1 FROM import.import_line WHERE imp_ref = :imp_ref)
                           GROUP BY f.cust_id
                           HAVING count((f.cust_id, f.marguage)) > 1)
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg || ' | ', '') || 'The custom_id ''' || value_txt1 ||
                  ''' corresponds to more than one product'
WHERE imp_ref = :imp_ref
  AND coalesce(value_txt1, '') != ''
  AND value_txt1 IN (SELECT cust_id FROM duplicate_cust_id);

------ Update the value_id1 (Material Id) if it is not yet set, from the product cust_id and marguage------
UPDATE import.import_line
SET value_id1 = f.m_id
FROM fiche f
WHERE imp_ref = :imp_ref
  AND coalesce(value_txt1, '') != ''
  AND f.cust_id = value_txt1
  AND marguage = value1
  AND (ns = :user_organisation OR ns LIKE :user_organisation || '/%')
  AND active IN (1, 2)
  AND m_attribute = '0'
  AND value_id1 IS NULL;

--- check if the product with combination of name and code exist. Put error if more than one product correspond at these combination ---
WITH material_from_name_and_code AS (SELECT count(*) AS material_count, value1, value2
                                     FROM fiche
                                              INNER JOIN import.import_line ON import.import_line.value1 = marguage
                                         AND import.import_line.value2 = m_ref
                                     WHERE coalesce(value1, '') != ''
                                       AND coalesce(value2, '') != ''
                                       AND imp_ref = :imp_ref
                                       AND (ns = :user_organisation OR ns LIKE :user_organisation || '/%')
                                       AND active IN (1, 2)
                                       AND m_attribute = '0'
                                     GROUP BY value1, value2)

UPDATE import.import_line il
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg || ' | ', '') || 'The combination of the name ''' || il.value2 ||
                  ''' and the code ''' || il.value1 || ''' correspond to more than one product'
FROM material_from_name_and_code m
WHERE imp_ref = :imp_ref
  AND il.value1 = m.value1
  AND il.value2 = m.value2
  AND m.material_count > 1
  AND il.value_id1 IS NULL;

------ Update the value_id1 (Material Id) if it is not yet set, from the product codification ------
WITH material_from_codification AS (SELECT count(*) AS material_count, value1
                                    FROM fiche
                                             INNER JOIN import.import_line ON import.import_line.value1 = marguage
                                    WHERE coalesce(value1, '') != ''
                                      AND marguage = value1
                                      AND imp_ref = :imp_ref
                                      AND (ns = :user_organisation OR ns LIKE :user_organisation || '/%')
                                      AND active IN (1, 2)
                                      AND m_attribute = '0'
                                    GROUP BY value1)

UPDATE import.import_line il
SET value_id1 = f.m_id
FROM fiche f
         INNER JOIN material_from_codification m ON m.value1 = f.marguage
WHERE coalesce(il.value1, '') != ''
  AND marguage = il.value1
  AND imp_ref = :imp_ref
  AND (ns = :user_organisation OR ns LIKE :user_organisation || '/%')
  AND active IN (1, 2)
  AND m_attribute = '0'
  AND value_id1 IS NULL
  AND m.material_count = 1;

------ Update the value_id1 (Material Id) if it is not yet set, from the product name ------
WITH material_from_codification AS (SELECT count(*) AS material_count, value1
                                    FROM fiche
                                             INNER JOIN import.import_line ON import.import_line.value1 = marguage
                                    WHERE coalesce(value1, '') != ''
                                      AND marguage = value1
                                      AND imp_ref = :imp_ref
                                      AND (ns = :user_organisation OR ns LIKE :user_organisation || '/%')
                                      AND active IN (1, 2)
                                      AND m_attribute = '0'
                                    GROUP BY value1)

UPDATE import.import_line
SET value_id1 = f.m_id
FROM fiche f
         INNER JOIN material_from_codification m ON m.value1 = f.marguage
WHERE imp_ref = :imp_ref
  AND coalesce(value2, '') != ''
  AND m_ref = value2
  AND (ns = :user_organisation OR ns LIKE :user_organisation || '/%')
  AND active IN (1, 2)
  AND m_attribute = '0'
  AND value_id1 IS NULL
  AND m.material_count <> 1;


----- Update the value_id3 in import table to be the ns_id of the users group ----
UPDATE import.import_line
SET value_id3 = ns_id
FROM ns
WHERE imp_ref = :imp_ref
  AND ns_tp = 'UsersGroup'
  AND split_part(ns, '/', 1) = :user_organisation
  AND split_part(ns_li, '/', 3) = value3;


------ Check if the users group exist in the database ------
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg || ' | ', '') || 'The group does not exist'
WHERE imp_ref = :imp_ref
  AND (coalesce(value3, '') = ''
    OR value3 NOT IN (SELECT DISTINCT split_part(ns_li, '/', 3)
                      FROM ns
                      WHERE ns_tp = 'UsersGroup'
                        AND split_part(ns, '/', 1) = :user_organisation));

SELECT *
FROM ns
WHERE split_part(ns_li, '/', 3) = 'Panification'

------------------------------------------------------------------------------------------------------------------------
/* COMMIT */

---- Insert in import.import_line the distinct material ids ----
WITH distinct_material AS (SELECT DISTINCT value_id1 AS distinct_m_id
                           FROM import.import_line
                           WHERE value_id1 IS NOT NULL
                             AND imp_ref = :imp_ref)

INSERT
INTO import.import_line (imp_id, value_id1, imp_ref, value7, value8)
SELECT nextval('import.import_line_imp_id'), distinct_m_id, 'NEW_REF' || :imp_ref, NULL, NULL
FROM distinct_material;

---- Update the import.import_line to take in account the permissions value regarding of the value inserted in import data ----
WITH material_to_update AS (SELECT imp_id,
                                   value_id1 AS material_id,
                                   value_id3 AS group_id,
                                   value3,
                                   value1,
                                   value2,
                                   value_txt1,
                                   CASE
                                       WHEN coalesce(TRIM(value4), '') != '' THEN '"RFGI-' || value_id3 || '"'
                                       ELSE NULL
                                       END      can_view,
                                   CASE
                                       WHEN coalesce(TRIM(value5), '') = '' THEN '"RFGI-' || value_id3 || '"'
                                       ELSE NULL
                                       END      cannot_modify
                            FROM import.import_line
                            WHERE imp_ref = :imp_ref),
     acl_columns AS (SELECT material_id,
                            value3,
                            string_agg(can_view, ',')      AS can_view,
                            string_agg(cannot_modify, ',') AS cannot_modify
                     FROM material_to_update
                     GROUP BY material_id, value3)
UPDATE import.import_line
SET value7 = acl_columns.can_view,
    value8 = acl_columns.cannot_modify
FROM acl_columns
WHERE imp_ref = 'NEW_REF' || :imp_ref
  AND value_id1 IS NOT NULL
  AND value_id1 = material_id
;

------ Update the view and modification permissions (acl_select and acl_options) of products regarding of the values in value4, value5 ------
WITH material_to_update AS (SELECT imp_id,
                                   value_id1,

                                   CASE
                                       WHEN coalesce(value7, '') != ''
                                           THEN cast('{"select": [' || value7 || ']}' AS JSONb)
                                       ELSE NULL
                                       END groups_can_view,

                                   CASE
                                       WHEN coalesce(value8, '') != '' THEN cast(
                                                       '{"customOptionOverrides": [{"ns": [' || value8 ||
                                                       '], "txt": "false", "txtKe": "SDSFactory.Product.Permissions.canUpdate"}]}' AS JSONb)
                                       ELSE NULL
                                       END groups_cannot_modify

                            FROM import.import_line
                            WHERE imp_ref = 'NEW_REF' || :imp_ref
                              AND value_id1 IS NOT NULL)

UPDATE fiche f
SET acl_select  = groups_can_view,
    acl_options = groups_cannot_modify,
    co3         = coalesce(co3, '') || '| Access-Overload Updated.'
FROM material_to_update
WHERE f.m_id = value_id1;


SELECT value_id1  AS Material_EcoUID,
       value_txt1 AS Material_CustomId,
       value1     AS Codification,
       value2     AS Name,
       value3     AS "Group",
       value4     AS Can_view,
       value5     AS Can_modify,
       value6
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND value_id1 IS NULL
  AND coalesce(imp_err, FALSE) = TRUE;


SELECT count(*)
FROM import.import_line
WHERE imp_ref LIKE :imp_ref;

WITH material_to_set_action AS (SELECT *
                                FROM import.import_line
                                         LEFT JOIN fiche ON import_line.value1 = fiche.marguage AND
                                                            split_part(ns, '/', 1) = 'LesaffreLis'
                                WHERE imp_ref LIKE :imp_ref)
UPDATE import.import_line
SET value30 = CASE WHEN m_id ISNULL THEN 'Create' ELSE 'Modify' END
FROM material_to_set_action
WHERE material_to_set_action.imp_id = import_line.imp_id
;

SELECT fiche.cust_id as fich_cust,
    import_line.cust_id AS "Material CustomId",
       value1              AS "Codification",
       value2              AS "Name",
       value25             AS "Material tpye",
       value3              AS "N° EC (for pure substances)",
       value4              AS "N° CAS (for pure substances)",
       value5              AS "Supplier commercial refrence",
       value6              AS "Supplier ERP code",
       value7              AS "Supplier",
       value8              AS "Manufacturer reference",
       value9              AS "Manufacturer",
       value10             AS "Category",
       value11             AS "Comment 1",
       value12             AS "Comment 2",
       value13             AS "Comment 3",
       value14             AS "ADR Hazard Code",
       value15             AS "ADR Hazard Class",
       value16             AS "ADR Group packaging",
       value17             AS "Conditionning",
       value18             AS "Physical state",
       value19             AS "Physical form if solid",
       value28             AS "Boiling point operator",
       value20             AS "Boiling point (°C)",
       value29             AS "Flash point operator",
       value21             AS "Flash point (°C)",
       value27             AS "Cinematic Viscosity (mm2/s, at 40°C)",
       value22             AS "pH",
       value30             AS "Action"
FROM import.import_line
         LEFT JOIN fiche ON marguage = value1
WHERE imp_ref LIKE :imp_ref
  AND split_part(ns, '/', 1) = 'LesaffreLis'
  AND m_id IS NOT NULL
ORDER BY value30
;
;

SELECT *
FROM information_schema.columns
WHERE table_name = 'fiche'
  AND column_name = 'version_tree'



SELECT count(*)
FROM import.import_line
WHERE imp_ref LIKE 'ImportSdsMaterial_00d63248-0895-4d68-922c-7a08b191b9c3_Material' --order by imp_id desc;


SELECT *, imp_err, imp_err_msg
FROM import.import_line
ORDER BY imp_id DESC

SELECT cust_id
FROM fiche
WHERE marguage = 'ENZ 0461'







































